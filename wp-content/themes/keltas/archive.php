<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header();

	global $wp;
	$url = home_url( $wp->request );
	$url_array = explode("/", $url);
?>

<div class="header-image" style="background-image: url(<?php echo get_the_post_thumbnail_url(  get_option( 'page_for_posts' ) ) ?>)"></div>

<div class="container" style="margin: auto; max-width: 1420px; padding: 50px 0;">
	<div class="row m-0">

		<div class="col-lg-3">
			<div class="sidebar-container">

				<h4 class="mid-sidebar-title"><?php echo get_the_title( get_option( 'page_for_posts' ) ) ?></h4>

				<?php if ( have_posts() ) :

					$years = array();
					$args = array('post_type' => 'post', 'posts_per_page' => -1,);
					$all_posts = new WP_Query( $args );

					while ( $all_posts->have_posts() ) : $all_posts->the_post();

						if ( !in_array(get_the_date('Y'), $years) )
							array_push($years, get_the_date('Y'));

					endwhile;
					wp_reset_query();

					echo '<ul class="archive-filter-block">';
						foreach ($years as $year) {

							$active_class = "";
							if ( $url_array[3] == $year ) $active_class = "filter-link-active";

							echo '<li><a class="archive-filter-link '. $active_class  .'" href="'. get_site_url() . "/". $year .'">'. $year .'</a></li>';
						}
					echo '</ul>';

				endif; ?>

			</div>
		</div>

		<div class="col-lg">

			<h1 class="big-title"><?php echo str_replace( "Metai:", "", get_the_archive_title()); ?> Metų naujienos</h1>

			<div class="row m-0 news-articles">
				<?php if ( have_posts() ) :

					$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
					$args = array(
						'post_type' 		=> 'post',
						'posts_per_page'	=> 9,
						'paged' 			=> $paged,
					);

					$args['date_query'] = array(
					        array('year'  => $url_array[3]),
                    );

					$posts = new WP_Query( $args );
					while ( $posts->have_posts() ) : $posts->the_post();

						get_template_part( 'template-parts/post/news' );

					endwhile;
				endif; ?>
			</div>

			<div class="row justify-content-center">
				<?php
				global $wp_query;
				if ( '' != $posts ) {
					$total_pages = $posts->max_num_pages;
				} else {
					$total_pages = $wp_query->max_num_pages;
				}

				if ( $total_pages > 1 ) {

					$current_page = get_query_var('paged');
					if (!$current_page) $current_page = 1;

					echo paginate_links(
						array(
							'base' => str_replace(999999, '%#%', esc_url(get_pagenum_link(999999))),
							'current' => max(1, get_query_var('paged')),
							'total' => $total_pages,
							'mid_size' => 4,
							'type' => 'list',
							'prev_next' => false,
						)
					);

				}
				?>
			</div>

		</div>
	</div>
</div>

<?php get_footer(); ?>
