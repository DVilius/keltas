<?php
/**
 * The template for displaying the header
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<title><?php bloginfo( 'name' ); ?></title>

<link rel="shortcut icon"  href="<?php echo get_template_directory_uri() .'/assets/images/favicon260x260.png' ?>"/>
<link rel="icon" type="image/vnd.microsoft.icon" href="<?php echo get_template_directory_uri() .'/assets/images/favicon260x260.png' ?>"/>
<link rel="icon" type="image/x-icon"  href="<?php echo get_template_directory_uri() .'/assets/images/favicon260x260.png' ?>"/>
<link rel="icon" href="<?php echo get_template_directory_uri() .'/assets/images/favicon260x260.png' ?>"/>

<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="profile" href="http://gmpg.org/xfn/11">

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<link rel='stylesheet' href='/wp-content/themes/keltas/assets/fonts/fonts.css' type='text/css' media='all' />

<?php wp_head(); ?>

	<?php $message = esc_html__("Mūsų svetainėje naudojami slapukai, kurie užtikrina sklandų svetainės darbą ir, kad suprastume, kaip jūs naudojatės mūsų svetaine. Daugiau informacijos apie naudojamus slapukus galite rasti svetainės", "keltas-theme" ) ?>
	<?php $link = esc_html__("privatumo politikoje.", "keltas-theme") ?>
	<?php $dismiss = esc_html__("Sutinku", "keltas-theme") ?>

	<!-- Begin Cookie Consent plugin by Silktide - http://silktide.com/cookieconsent -->
	<script async type="text/javascript">
		window.cookieconsent_options = {
			message: '<?php echo $message ?>',
			dismiss: '<?php echo $dismiss ?>',
			learnMore: '<?php echo $link ?>',
			theme: 'light-bottom',
			link: '<?php echo get_permalink( get_option( 'wp_page_for_privacy_policy' )) ?>',
			markup: [
				'<div class="cc_banner-wrapper {{containerClasses}}">',
				'<div class="cc_banner cc_container cc_container--open">',
				'<a href="#" data-cc-event="click:dismiss" target="_blank" class="cc_btn cc_btn_accept_all">{{options.dismiss}}</a>',
				'<p class="cc_message">{{options.message}} <a target="_blank" data-cc-if="options.link" class="cc_more_info" href="{{options.link || "#null"}}">{{options.learnMore}}</a></p>',
				'</div>',
				'</div>'
			]
		};
	</script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.10/cookieconsent.min.js"></script>
	<!-- End Cookie Consent plugin -->

	<?php $google_key = get_field('google_maps_api', 'option');?>
	<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo $google_key ?>&language=lt&region=LT&libraries=geometry"></script>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script src="https://www.googletagmanager.com/gtag/js?id=UA-128054119-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-128054119-1');
	</script>

    <script>
        var wtpQualitySign_projectId = 166290;
        var wtpQualitySign_language = "lt";
    </script>

    <script src="https://bank.paysera.com/new/js/project/wtpQualitySigns.js" type="text/javascript" charset="utf-8"></script>
    <script src="https://www.paysera.com/new/en/projects/get_quality_sign_code/166290"></script>


</head>

<body <?php body_class(); ?>>

<?php

$slug = explode("/", get_page_template_slug());
$slugExp = isset($slug[1]) ? $slug[1] : false;

if ($slugExp != "simple-schedule.php" && $slugExp != "new-part.php" && $slugExp != "old-part.php") :?>

    <?php if (is_user_logged_in()) : ?>
        <div style="position: absolute; height: 32px"></div>
    <?php endif; ?>

<header class="header-container-fixed">
	<div class="shadow-header"></div>

        <?php
        $custom_style = "";
        if (get_field("important-message-color", get_option('page_on_front')) != null) {
            $custom_style = "background: " . get_field("important-message-color", get_option('page_on_front'));
        }

        $apiContents = Api::getInfo(ICL_LANGUAGE_CODE);
        $info = json_decode($apiContents, true);

        ?>

        <?php if (!empty($info)): ?>

        <div class="warn-info-block" style="<?php echo $custom_color ?>">
            <div class="warn-info-container">
                <p style="text-align: center; font-size: 16px; float: left;"> <?php get_template_part('assets/svg/warning'); ?> <strong>Dėmesio!</strong>
                    <?php echo $info; ?>
                </p>
                <div class="close-button-container">
                    <a id="close-button" style=""><?php get_template_part('assets/svg/close'); ?></a>
                </div>
            </div>
        </div>

    <?php endif; ?>

	<script>
		$(document).ready(function(){
			$('#close-button').on('click', function (e) {
				$('.warn-info-block').css( "display", "none" );
			});
		});
	</script>

</header>

<?php endif; ?>