<?php
/**
 * The template for displaying 404 pages (not found)
 */

get_header(); ?>

	<div class="header-image" style="background-image: url(<?php echo get_the_post_thumbnail_url(  get_option( 'page_for_posts' ) ) ?>)"></div>

	<div class="container" style="margin: auto; max-width: 1420px; padding: 50px 0;">
		<div class="row m-0">
			<div class="col-lg">
				<h1 class="big-title"><?php echo esc_html__( "Puslapis nerastas", "keltas-theme" ) ?></h1>

				<?php $unique_id = esc_attr( uniqid( 'search-form-' ) ); ?>
				<form role="search" method="get" class="search-form search-page-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
					<input type="search" id="<?php echo $unique_id; ?>" class="search-field" placeholder="<?php echo esc_attr_x( 'Ieškoti svetainėje', 'placeholder', 'keltas-theme' ); ?>"
						   value="<?php echo get_search_query(); ?>" name="s" />
					<button type="submit" class="search-submit search-icon"><?php get_template_part('assets/svg/search'); ?></button>
				</form>

			</div>
		</div>
	</div>

<?php get_footer();