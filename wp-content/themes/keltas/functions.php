<?php
/**
 * Keltas functions and definitions
 */

// Custom Theme Scripts
function custom_theme_scripts() {
	wp_enqueue_script( 'jquery', 'https://code.jquery.com/jquery-3.3.1.slim.min.js');
	wp_enqueue_script( 'popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js');
	wp_enqueue_script( 'bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js');
	wp_enqueue_script( 'core',  get_template_directory_uri() . '/assets/js/core.js');
	wp_enqueue_script( 'slider', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js');
	wp_enqueue_script( 'lightbox', get_stylesheet_directory_uri() . '/assets/js/lightbox.js');
}
add_action( 'wp_enqueue_scripts', 'custom_theme_scripts' );

// Custom Theme Styles
function custom_theme_style() {
	wp_enqueue_style( 'slider', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css');
	wp_enqueue_style( 'bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css');
	wp_enqueue_style( 'fontawesome', 'https://use.fontawesome.com/releases/v5.0.13/css/all.css');
	wp_enqueue_style( 'mobile-style', get_template_directory_uri() . '/assets/css/mobile-style.css');
	wp_enqueue_style( 'lightbox', get_template_directory_uri(). '/assets/css/lightbox.min.css');
}
add_action( 'wp_enqueue_scripts', 'custom_theme_style' );

// Navigation Menus
register_nav_menus( array(
	'header-menu' => __( 'Top navigation', '' ),
	'icon-menu' => __( 'Icon navigation', '' ),
	'icon-menu-mobile' => __( 'Icon navigation for mobile version', '' ),
	'side-menu' => __( 'Side navigation', '' ),
) );

// Add theme support for Featured images
add_theme_support('post-thumbnails', array( 'post', 'page' ));

// PDF files filter
function modify_post_mime_types( $post_mime_types ) {

	$post_mime_types['application/pdf'] = array( __( 'PDFs' ), __( 'Manage PDFs' ), _n_noop( 'PDF <span class="count">(%s)</span>', 'PDFs <span class="count">(%s)</span>' ) );
	return $post_mime_types;
}
add_filter( 'post_mime_types', 'modify_post_mime_types' );

// Support for uploading SVG files
function cc_mime_types($mimes) {
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

// Ferry choices
function acf_ferry_choices( $field ) {

	$field['choices'] = array(); // reset choices

	$choices = array();

	if( have_rows('ferrys-repeater', get_option( 'page_on_front')) ):
		while ( have_rows('ferrys-repeater', get_option( 'page_on_front')) ) : the_row();

			array_push($choices, get_sub_field('ferry-side') .": ". get_sub_field('ferry-name'));

		endwhile;
	endif;

	$choices = array_map('trim', $choices); // remove any unwanted white space

	if( is_array($choices) ) {
		foreach( $choices as $choice ) {
			$field['choices'][ $choice ] = $choice;
		}
	}
	return $field;
}
add_filter('acf/load_field/name=ferry-flights-from', 'acf_ferry_choices');
add_filter('acf/load_field/name=ferry-flights-to', 'acf_ferry_choices');


// Google map api for ACF plugin
function acf_google_map_key() {
	acf_update_setting('google_api_key', 'AIzaSyBsmQXZzSgKHwePNb-8hIyrN0T3X3aaKEc');
}
add_action('acf/init', 'acf_google_map_key');


add_filter('site_transient_update_plugins', 'remove_update_notification');
function remove_update_notification($value) {
	unset($value->response["advanced-custom-fields-pro/acf.php"]);
	return $value;
}

// Responsive tables
function table_wrapper($content) {
	return preg_replace_callback('~<table.*?</table>~is', function($match) {
		return '<div class="table-responsive">' . $match[0] . '</div>';
	}, $content);
}
add_filter('get_the_content', 'table_wrapper');


// Max posts to show in search
function custom_search_posts_per_page($query) {
	if ( $query->is_search ) $query->set( 'posts_per_page', '10' );
	return $query;
}
add_filter( 'pre_get_posts','custom_search_posts_per_page' );


// Load theme translations
add_action('after_setup_theme', 'keltas_theme_setup');
function keltas_theme_setup(){
	load_theme_textdomain('keltas-theme', get_template_directory() . '/languages');
}


// Option Page for Maps API
if( function_exists('acf_add_options_page') ) {

	acf_add_options_sub_page(array(
		'page_title' 	=> 'API Settings',
		'menu_title' 	=> 'API',
		'parent_slug' 	=> 'options-general.php'
	));

}


function printFerryTimes($data_array, $terminal, $source, $destination) {

	echo '<div class="col-6 p-0">';

	$valid_hours = array();

	// $terminal . "@". $source ."@". $destination ."@". $departure_time ."@". $category  ."@". $exception
	for ($a = 0; count($data_array) != $a ;$a++) {
		$line_array = explode('@', $data_array[$a]);
		if ($line_array[0] == $terminal && $line_array[1] == $source && $line_array[2] == $destination) {

			$departure_time = $line_array[3];
			if ($line_array[5] != "") $departure_time = $line_array[3] . '<span class="ati" >*</span>';

			if (!in_array($departure_time, $valid_hours)) array_push($valid_hours, $departure_time);
		}
	}
	$next_start = count($valid_hours) / 2;
	$current_hour = "";
	$current_hour_num = 0;

	for ($k = 0; round($next_start) > $k; $k++) {

		$hour = explode(":", $valid_hours[$k])[0];
		if ($current_hour != $hour && $current_hour_num < round($next_start)) {
			$current_hour = $hour;
			$current_hour_num++;
		}
	}

	for ($k = 0; round($next_start) > $k; $k++) {

		$hour = explode(":", $valid_hours[$k])[0];
		if ($current_hour != $hour ) {
			$current_hour = $hour;

			$times = explode(":", $valid_hours[$k])[1] . " ";
			$c = 1;
			while ( $c != 5 ) {
				if ( explode(":", $valid_hours[$k])[0] == explode(":",$valid_hours[($k + $c)])[0] ) {
					$times .=  explode(":", $valid_hours[($k + $c)])[1] ." ";
				} $c++;
			}

			echo getTimeFormat($hour, $times);
		}
	}

	echo '</div>';

	echo '<div class="col-6 p-0">';
		$current_hour = "";
		$current_hour_num2 = 0;
		for ($k = 0; round(count($valid_hours)) > $k; $k++) {

			$hour = explode(":", $valid_hours[$k])[0];
			if ($current_hour != $hour ) {
				$current_hour = $hour;
				$current_hour_num2++;

				if ($current_hour_num2 > $current_hour_num) {

					$times = explode(":", $valid_hours[$k])[1] . " ";
					$c = 1;
					while ($c != 5) {
						if (explode(":", $valid_hours[$k])[0] == explode(":", $valid_hours[($k + $c)])[0]) {
							$times .= explode(":", $valid_hours[($k + $c)])[1] . " ";
						}
						$c++;
					}

					echo getTimeFormat($hour, $times);
				}
			}
		}
	echo '</div>';
}

function printFerryTimesCategorized($data_array, $terminal, $source, $destination, $category, $title) {

	$valid_hours = array();
	// $terminal . "@". $source ."@". $destination ."@". $departure_time ."@". $category
	for ($a = 0; count($data_array) != $a; $a++) {
		$line_array = explode('@', $data_array[$a]);
		if ($line_array[0] == $terminal && $line_array[1] == $source && $line_array[2] == $destination && $line_array[4] == $category) {

			$departure_time = $line_array[3];
			if ($line_array[5] != "") $departure_time = $line_array[3] . '<span class="ati" >*</span>';

			if (!in_array($departure_time, $valid_hours)) array_push($valid_hours, $departure_time);
		}
	}

	if (count($valid_hours) != 0) {
		echo '<h4 class="sm-2-title">' . esc_html__($title, "keltas-theme") . '</h4>';

		echo '<div class="col-lg">';
		echo '<div class="row">';

		echo '<div class="col-6 p-0">';
		$next_start = count($valid_hours) / 2;
		$current_hour = "";
		$current_hour_num = 0;

		for ($k = 0; round($next_start) > $k; $k++) {

			$hour = explode(":", $valid_hours[$k])[0];
			if ($current_hour != $hour && $current_hour_num < round($next_start)) {
				$current_hour = $hour;
				$current_hour_num++;
			}
		}

		for ($k = 0; $next_start > $k; $k++) {

			$hour = explode(":", $valid_hours[$k])[0];
			//if ($current_hour != $hour ) {
			$current_hour = $hour;

			$times = explode(":", $valid_hours[$k])[1] . " ";
			$c = 1;
			while ($c != 5) {
				if (explode(":", $valid_hours[$k])[0] == explode(":", $valid_hours[($k + $c)])[0]) {
					$times .= explode(":", $valid_hours[($k + $c)])[1] . " ";
				}
				$c++;
			}

			echo getTimeFormat($hour, $times);
			//}
		}
		echo '</div>';
		echo '<div class="col-6 p-0">';
		$current_hour = "";
		$current_hour_num2 = 0;
		for ($k = 0; round(count($valid_hours)) != $k; $k++) {

			$hour = explode(":", $valid_hours[$k])[0];
			if ($current_hour != $hour) {
				$current_hour = $hour;
				$current_hour_num2++;

				if ($current_hour_num2 > $current_hour_num) {

					$times = explode(":", $valid_hours[$k])[1] . " ";
					$c = 1;
					while ($c != 5) {
						if (explode(":", $valid_hours[$k])[0] == explode(":", $valid_hours[($k + $c)])[0]) {
							$times .= explode(":", $valid_hours[($k + $c)])[1] . " ";
						}
						$c++;
					}

					echo getTimeFormat($hour, $times);
				}
			}
		}
		echo '</div>';

		echo '</div>';
		echo '</div>';
	}

}


function printFerryTimes2($data_array, $terminal, $source, $category) {

	echo '<div class="col-6 p-0">';
	$valid_hours = array();
	for ($a = 0; count($data_array) != $a ;$a++) {
		$line_array = explode('@', $data_array[$a]);
		if ($line_array[0] == $terminal && $line_array[1] == $source && $line_array[4] == $category) {

			$departure_time = $line_array[3];
			if ($line_array[5] != "") $departure_time = $line_array[3]. '<span class="ati" >*</span>';

			if ( !in_array($departure_time, $valid_hours )) array_push($valid_hours, $departure_time);
		}
	}
	$next_start = count($valid_hours) / 2;
	$current_hour = "";
	$current_hour_num = 0;

	for ($k = 0; round($next_start) > $k; $k++) {

		$hour = explode(":", $valid_hours[$k])[0];
		if ($current_hour != $hour && $current_hour_num < round($next_start)) {
			$current_hour = $hour;
			$current_hour_num++;
		}
	}

	for ($k = 0; round($next_start) > $k; $k++) {

		$hour = explode(":", $valid_hours[$k])[0];
		if ($current_hour != $hour ) {
			$current_hour = $hour;

			$times = explode(":", $valid_hours[$k])[1] . " ";
			$c = 1;
			while ( $c != 5 ) {
				if ( explode(":", $valid_hours[$k])[0] == explode(":",$valid_hours[($k + $c)])[0] ) {
					$times .=  explode(":", $valid_hours[($k + $c)])[1] ." ";
				} $c++;
			}

			echo getTimeFormat($hour, $times);
		}
	}
	echo '</div>';
	echo '<div class="col-6 p-0">';
	$current_hour = "";
	$current_hour_num2 = 0;
	for ($k = 0; round(count($valid_hours)) > $k; $k++) {

		$hour = explode(":", $valid_hours[$k])[0];
		if ($current_hour != $hour ) {
			$current_hour = $hour;
			$current_hour_num2++;

			if ($current_hour_num2 > $current_hour_num) {

				$times = explode(":", $valid_hours[$k])[1] . " ";
				$c = 1;
				while ($c != 5) {
					if (explode(":", $valid_hours[$k])[0] == explode(":", $valid_hours[($k + $c)])[0]) {
						$times .= explode(":", $valid_hours[($k + $c)])[1] . " ";
					}
					$c++;
				}

				echo getTimeFormat($hour, $times);
			}
		}
	}
	echo '</div>';

}


function getTimeFormat($hour, $minutes) {

	return '<p class="schedule-time"><span class="hour-class">' . $hour . '</span><span class="minute-class">' . $minutes . '</span></p>';
}

add_action( 'send_headers', function()
{
	header('Content-Security-Policy: default-src \'self\' \'unsafe-inline\' \'unsafe-eval\' https: data:');
	header('Strict-Transport-Security:max-age=31536000; includeSubdomains; preload');
	header( 'X-XSS-Protection: 1; mode=block' );
	header( 'X-Frame-Options: SAMEORIGIN' );
	header( 'X-Content-Type-Options: nosniff' );
});

remove_action('wp_head', 'wp_generator');
?>
