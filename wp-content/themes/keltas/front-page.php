<?php
/**
 * The front page template file
 */

$front_page = get_option( 'page_on_front' );

get_header(); ?>

<div class="slideshow-container">
	<div class="container" style="max-width: 1560px;">
		<div class="page-image-title-container">
			<?php
			$slogan = "Jungiame Krantus";
			if (ICL_LANGUAGE_CODE == 'ru') $slogan = "Соединяем берега!";
			if (ICL_LANGUAGE_CODE == 'en') $slogan = "Connecting the shores";
			?>
			<h3 class="page-image-title"><?php echo $slogan ?></h3>
		</div>
	</div>

	<?php
	$size = 'full';

	if( have_rows('image-video-slider-content', $front_page) ) :
		echo '<ul style="margin: 0;margin-bottom: -13px;">';
		$id = 0;

		while(the_flexible_field("image-video-slider-content", $front_page)):

			if (get_row_layout() == "slider-image") :
				echo '<li>';
					$image = get_sub_field('slide-image');
					if ( $image != null ) {
						echo wp_get_attachment_image( $image['ID'], $size );
					}
				echo '</li>';
			elseif (get_row_layout() == "slider-video-local") :
				echo '<li>';
					$video = get_sub_field('slide-video-file');
					if ( $video != "" ) {
						echo '<video id="cover-video-'. $id  .'" width="100" height="100" class="slideshow-video" loop style="transform: translate(0, -1%); width: 100%; height: 100%" autoplay="autoplay" muted>';
							echo '<source src="'. $video .'" type="video/webm" />';
							echo '<source src="'. $video .'" type="video/mp4" />';
							echo '<source src="'. $video .'" type="video/ogg" />';
							echo 'Jūsų naršyklė nepalaiko video formato.';
						 echo '</video>';
						echo '<script> $(document).ready(function() { document.getElementById("cover-video-'. $id  .'").play(); }); </script>';
						$id++;
					}
				echo '</li>';
			else :
				echo '<li>';
				$video_url = get_sub_field('slide-video');
					if ( $video_url != null )  {
						$video_url = str_replace("watch?v=", "embed/", $video_url);
						echo '<iframe id="video" width="100%" height="100%" src="'. $video_url .'?rel=0&amp;controls=0&amp;showinfo=0&autoplay=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
					}
				echo '</li>';
			endif;


		endwhile;
		echo '</ul>';
	endif; ?>

	<div class="scroll-indicator" style="display: none;">
		<?php get_template_part( 'assets/svg/mouse' ); ?>
	</div>
</div>

<div class="row m-0">
	<div class="destination-container">

		<?php get_template_part( 'template-parts/destination-map' ); ?>

	</div>
</div>

<?php if ( get_field("banner-control", get_option( 'page_on_front')) === "top") : ?>

	<?php get_template_part( 'template-parts/banner/nida-banner' ); ?>

<?php endif; ?>

<?php if (ICL_LANGUAGE_CODE == 'lt'): ?>
<div class="news-container-block">
	<div class="container" style="max-width: 1420px; padding: 50px 0;">
		<div class="row news-container-title">

			<h4 class="title"><?php echo esc_html__( 'Naujienos', 'keltas-theme' ); ?></h4>
			<p class="all-news-link"><a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>"><i class="fas fa-file-alt"></i><?php echo esc_html__( 'Visos naujienos', 'keltas-theme' ); ?></a></p>

		</div>

		<div class="row m-0 front-news-row">

			<div class="row front-news-container">

				<?php
				$args = array(
					'posts_per_page'=> 3,
					'post_type'		=> 'post',
				);

				$news = new WP_Query( $args );

				if( $news->have_posts() ):
					while( $news->have_posts() ) : $news->the_post();

						get_template_part( 'template-parts/post/news' );

					endwhile;
					wp_reset_postdata();
					wp_reset_query();
				endif; ?>

			</div>

			<div class="row m-0 mobile-app-container">

				<div class="mobile-application-title">
					<h4 class="title"><?php echo esc_html__( 'Mobili aplikacija', 'keltas-theme' ); ?></h4>
				</div>

				<div class="row">
					<div class="col-md-5">
						<p><?php echo esc_html__( 'Visa informacija apie keltų kainas ir tvarkaraščius jūsų telefone! Atsisiųskite Keltas mobiliąją programėlę', 'keltas-theme' ); ?></p>

						<div class="mobile-app-svg-white">
							<a target="_blank" href="<?php the_field("apple_store_url", get_option( 'page_on_front')) ?>">
								<?php echo '<img alt="" src="' . get_template_directory_uri() . '/assets/images/apple-store-white.png' . '" />' ?>
							</a>
						</div>
						<div class="mobile-app-svg-white">
							<a target="_blank" href="<?php the_field("google_play_url", get_option( 'page_on_front')) ?>">
								<?php echo '<img alt="" src="' . get_template_directory_uri() . '/assets/images/google-store-white.png' . '" />' ?>
							</a>
						</div>
					</div>
					<div class="col-md phone-svg">
						<?php echo '<img alt="" src="' . get_template_directory_uri() . '/assets/images/phone.png' . '" />' ?>
					</div>
				</div>

			</div>
		</div>

	</div>
</div>
<?php endif; ?>

<?php get_footer();
