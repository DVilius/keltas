<?php
/**
 * The template for displaying all pages
 */

get_header();

global $wp;
$url = home_url( $wp->request );
$url_array = explode("/", $url);

$part_url = $url_array[0] ."/". $url_array[1] ."/". $url_array[2] ."/". $url_array[3];
$level_one_page_id = url_to_postid($part_url);
if (ICL_LANGUAGE_CODE != 'lt') {
    $part_url = $url_array[0] ."/". $url_array[1] ."/". $url_array[2] ."/". $url_array[3] ."/". $url_array[4];
    $level_one_page_id = url_to_postid($part_url);
}
?>

<?php
$background = 'background: url('. get_the_post_thumbnail_url() .')';
if ( get_the_post_thumbnail_url() == null ) $background = 'background: url('. get_the_post_thumbnail_url( $level_one_page_id ) .')';

if ( get_the_post_thumbnail_url( $level_one_page_id ) == null ) $background = "height: 50px;";
?>

<div class="header-image" style="<?php echo $background ?>"></div>

<div class="container" style="margin: auto; max-width: 1420px; padding: 50px 0;">
	<div class="row m-0">
		<div class="col-lg-3">
			<div class="sidebar-container">

				<h4 class="mid-title"><?php echo get_the_title( $level_one_page_id ) ?></h4>

				<?php
				$locations = get_nav_menu_locations();
				$menu = wp_get_nav_menu_object($locations['header-menu']);
				$menu_items = wp_get_nav_menu_items($menu->term_id);

				$menu_id = 0;
				foreach ( (array) $menu_items as $key => $menu_item ) {
					if (strcmp(get_the_title($level_one_page_id), $menu_item->title) == 0) {
						$menu_id = $menu_item->db_id;
					}
				}

				echo '<ul class="archive-filter-block">';
				foreach ( (array) $menu_items as $key => $menu_item ) {
					if ($menu_item->menu_item_parent == $menu_id ) {
						$title = $menu_item->title;
						$url = $menu_item->url;

						echo '<li><a class="archive-filter-link" href="'. $url .'">'. $title .'</a></li>';
					}
				}
				echo '</ul>';

				?>
			</div>
		</div>

		<div class="col-lg">
			<?php if ( have_posts() ) : ?>
				<?php
				while ( have_posts() ) : the_post();

					echo '<h1 class="big-title">'. get_the_title() .'</h1>';

					echo '<div class="information-container">';
						get_template_part( 'template-parts/page/content-page' );
					echo '</div>';

				endwhile;
			endif; ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>
