<?php
/**
 * The template for displaying the footer
 */
?>

<?php

$slug = explode("/", get_page_template_slug());
$slugExp = isset($slug[1]) ? $slug[1] : false;

if ($slugExp != "simple-schedule.php" && $slugExp != "new-part.php" && $slugExp != "old-part.php") : ?>

    <footer>
        <?php get_template_part('template-parts/footer/footer-part'); ?>
    </footer>
<?php endif; ?>

<?php wp_footer(); ?>

</body>
</html>
