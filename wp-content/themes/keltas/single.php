<?php
/**
 * The template for displaying all single posts
 */

get_header();
?>

<div class="header-image" style="background-image: url(<?php echo get_the_post_thumbnail_url(  get_option( 'page_for_posts' ) ) ?>)"></div>

<div class="container" style="margin: auto; max-width: 1420px; padding: 50px 0;">
	<div class="row">

		<div class="col-md-3 mr-4">
			<div class="sidebar-container">

				<h4 class="mid-title"><?php echo get_the_title( get_option( 'page_for_posts' ) ) ?></h4>

				<?php if ( have_posts() ) :

					$years = array();
					$args = array('post_type' => 'post', 'posts_per_page' => -1,);
					$all_posts = new WP_Query( $args );

					while ( $all_posts->have_posts() ) : $all_posts->the_post();

						if ( !in_array(get_the_date('Y'), $years) )
							array_push($years, get_the_date('Y'));

					endwhile;

					foreach ($years as $year) {

						echo '<a class="archive-filter-link" href="'. get_site_url() . "/". $year .'">'. $year .'</a><br>';
					}
				endif; ?>

			</div>
		</div>

		<div class="col-lg">
			<h1 class="big-title"><?php echo esc_html__( 'Naujienos', 'keltas-theme' ); ?></h1>

			<div class="row">
				<div class="col-lg">
					<?php while ( have_posts() ) : the_post();

						get_template_part( 'template-parts/post/news-full' );

					endwhile; ?>
				</div>
			</div>
		</div>

	</div>
</div>

<?php get_footer();
