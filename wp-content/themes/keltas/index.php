<?php
/**
 * The main template file
 */

get_header(); ?>

<div class="header-image" style="background-image: url(<?php echo get_the_post_thumbnail_url(  get_option( 'page_for_posts' ) ) ?>)"></div>

<div class="container" style="margin: auto; max-width: 1420px; padding: 50px 0;">

	<div class="row">

		<h1 class="big-title"><?php echo get_the_title() ?></h1>

		<div class="row">
			<?php if ( have_posts() ) : ?>
				<?php
				while ( have_posts() ) : the_post();

					get_template_part( 'template-parts/post/news' );

				endwhile;
			endif; ?>
		</div>

	</div>
</div>

<?php get_footer();
