
<?php $front_page = 31; ?>

<?php if ( get_field("banner-control", get_option( 'page_on_front')) === "bot") : ?>

    <?php get_template_part( 'template-parts/banner/nida-banner' ); ?>

<?php endif; ?>

<div class="footer-container-background">
    <div class="footer-container-top">
        <div class="top-footer">
            <div class="row">

                <div class="row row-top-footer-part m-0">

                    <div class="col-auto" style="min-width: 250px; margin-top: -7px;">
                        <div class="footer-question">
                            <h3 class="footer-question-text"><?php echo esc_html__( 'Turite klausimų?', 'keltas-theme' ); ?></h3>
                        </div>
                    </div>

                    <div class="col-auto">
                        <div class="footer-question">
                            <h3 class="footer-short-number">
                                <a href="tel:<?php the_field("short-number", get_option( 'page_on_front')) ?>"> <?php the_field("short-number", get_option( 'page_on_front')) ?></a>
                            </h3>
                            <p class="email-address">
                                <a href="mailto:<?php the_field("email", get_option( 'page_on_front')) ?>"> <?php the_field("email", get_option( 'page_on_front')) ?></a>
                            </p>
                        </div>
                    </div>

                    <div class="col-lg-auto report-button-container">
                        <button onclick="window.open('<?php the_field("homepage-url-contacts", get_option( 'page_on_front')) ?>', '_self')" class="button-contact-us"><?php echo esc_html__( 'Susisiekite su mumis', 'keltas-theme' ); ?></button>
                    </div>

                </div>

                <div class="row row-top-footer-part m-0">

                    <div class="col-md-auto ferry-info-footer">
                        <p class="port-name"><?php the_field("port-name", get_option( 'page_on_front')) ?></p>

                        <?php $map_url = 'https://www.google.lt/maps/place/'. str_replace( " ", "%20", get_field("port-address", get_option( 'page_on_front')) ) ?>
                        <p class="port-address"><a target="_blank" href="<?php echo $map_url ?>"><i class="fas fa-map-marker-alt"></i> <?php the_field("port-address", get_option( 'page_on_front')) ?></a></p>
                        <p class="port-cordinates"><?php echo esc_html__( 'GPS koordinatės:', 'keltas-theme' ); ?><?php the_field("port-cordinates", get_option( 'page_on_front')) ?></p>
                        <p class="port-open-hours"><?php echo esc_html__( 'Darbo laikas:', 'keltas-theme' ); ?><?php the_field("port-open-hours", get_option( 'page_on_front')) ?></p>
                    </div>

                    <div class="col-md-auto ferry-info-footer">
                        <p class="port-name"><?php the_field("new-port-name", get_option( 'page_on_front')) ?></p>

                        <?php $map_url = 'https://www.google.lt/maps/place/'. str_replace( " ", "%20", get_field("new-port-address", get_option( 'page_on_front')) ) ?>
                        <p class="port-address"><a target="_blank" href="<?php echo $map_url ?>"><i class="fas fa-map-marker-alt"></i> <?php the_field("new-port-address", get_option( 'page_on_front')) ?></a></p>
                        <p class="port-cordinates"><?php echo esc_html__( 'GPS koordinatės:', 'keltas-theme' ); ?><?php the_field("new-port-cordinates", get_option( 'page_on_front')) ?></p>
                        <p class="port-open-hours"><?php echo esc_html__( 'Darbo laikas:', 'keltas-theme' ); ?><?php the_field("new-port-open-hours", get_option( 'page_on_front')) ?></p>
                    </div>

                    <div class="col-lg-auto report-button-container">
                        <button onclick="window.open('<?php the_field("homepage-url-corruption", get_option( 'page_on_front')) ?>', '_blank');" class="button-report"><i class="fas fa-exclamation-circle"></i><?php echo esc_html__( 'Pranešti apie korupciją', 'keltas-theme' ); ?></button>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>

<div class="footer-container-background">
    <div class="footer-container-top">
        <div class="bottom-footer">
            <div class="row m-0">

                <div class="col-md-auto footer-box p-0" style="width: 370px;">
                    <div class="mobile-app">
                        <div class="mobile-app-text">
                            <p><?php echo esc_html__( 'Parsisiųskite mūsų mobilią aplikaciją', 'keltas-theme' ); ?></p>
                        </div>

                        <div class="mobile-app-svg">
                            <a target="_blank" href="<?php the_field("apple_store_url", get_option( 'page_on_front')) ?>">
                                <?php echo '<img alt="" src="' . get_template_directory_uri() . '/assets/images/apple-store.png' . '" />' ?>
                            </a>
                        </div>
                        <div class="mobile-app-svg">
                            <a target="_blank" href="<?php the_field("google_play_url", get_option( 'page_on_front')) ?>">
                                <?php echo '<img alt="" src="' . get_template_directory_uri() . '/assets/images/google-store.png' . '" />' ?>
                            </a>
                        </div>

                        <img height="50" width="50" alt="visa" src="<?php echo get_template_directory_uri() . '/assets/images/visa.svg' ?>"/>
                        <img height="50" width="50" alt="mastercard" src="<?php echo get_template_directory_uri() . '/assets/images/mastercard.svg' ?>"/>

                    </div>
                </div>

                <div class="col-lg-auto footer-box footer-address">
                    <p class="location-address">
                        <?php $map_url = 'https://www.google.lt/maps/place/'. str_replace( " ", "%20", get_field('address', get_option( 'page_on_front')) ); ?>
                        <a target="_blank" href="<?php echo $map_url ?>"> <i class="fas fa-map-marker-alt"></i> <?php the_field("address", get_option( 'page_on_front')) ?></a>
                    </p>
                </div>

                <div class="col-md-auto footer-box footer-business">
                    <p><?php echo esc_html__( 'Įmonės kodas:', 'keltas-theme' ); ?><?php the_field("business-code", get_option( 'page_on_front')) ?></p>
                    <p><?php echo esc_html__( 'PVM mokėtojo kodas:', 'keltas-theme' ); ?><?php the_field("business-payer-code", get_option( 'page_on_front')) ?></p>
                </div>

                <div class="col-lg-auto footer-box">
                    <div class="certificates">
                        <img alt="" src="<?php the_field("web-certificates", $front_page) ?>" />
                    </div>
                </div>

                <div class="col-md-auto footer-box footer-copyright">
                    <p>© 2020 AB Smiltynės Perkėla</p>
                </div>

                <div class="col-md-auto footer-box creative-partner">
                    <a href="https://saskaita123.lt/" target="_blank"><span><?php echo esc_html__( 'Sukurė', 'keltas-theme' ); ?>:</span></a>
                    <a href="https://www.cpartner.lt/" target="_blank"><span><?php echo '<img alt="" src="' . get_template_directory_uri() . '/assets/images/creativepartner.png' . '" />' ?></span></a>
                </div>

            </div>

        </div>
    </div>
</div>
