<?php
/**
 * Template Name: Paprastas-Tvarkaraštis-TV
 */

get_header();
?>

    <div class="container inside-page" style="margin: auto; max-width: 1420px; padding: 50px 0;">
        <div class="row m-0">
            <h1 class="big-title"><?php echo get_the_title() ?></h1>
        </div>

        <div class="row m-0">
            <div class="col-lg-16 pt-4">
                <?php
                $url = "http://37.187.163.199/api/v2/timetable/";
                $contents = file_get_contents($url);
                $decoded = json_decode($contents);

                $decoded_array = array();
                for ($i = 0; count($decoded) != $i; $i++) {

                    $terminal = $decoded[$i]->terminal;
                    $category = $decoded[$i]->category;
                    $day_of_week = $decoded[$i]->day_of_week;
                    $source = $decoded[$i]->source;
                    $destination = $decoded[$i]->destination;
                    $time = $decoded[$i]->departure_time;
                    $exception = $decoded[$i]->exception;

                    $data = array($time, $day_of_week, $terminal, $category, $source, $destination, $exception);
                    array_push($decoded_array, $data);
                }
                sort($decoded_array);

                $week_one_array = array();
                $week_two_array = array();
                $week_three_array = array();
                $week_four_array = array();
                $week_five_array = array();
                $week_six_array = array();
                $week_seven_array = array();

                for ($i = 0; count($decoded_array) != $i; $i++) {

                    $departure_time = $decoded_array[$i][0];
                    $terminal = $decoded_array[$i][2];
                    $category =  $decoded_array[$i][3];
                    $source = $decoded_array[$i][4];
                    $destination = $decoded_array[$i][5];
                    $exception = $decoded_array[$i][6];

                    $push_string = $terminal . "@". $source ."@". $destination ."@". $departure_time ."@". $category  ."@". $exception;

                    switch($decoded_array[$i][1]) {
                        case "1": array_push($week_one_array, $push_string); break;
                        case "2": array_push($week_two_array, $push_string); break;
                        case "3": array_push($week_three_array, $push_string); break;
                        case "4": array_push($week_four_array, $push_string); break;
                        case "5": array_push($week_five_array, $push_string); break;
                        case "6": array_push($week_six_array, $push_string); break;
                        case "7": array_push($week_seven_array, $push_string); break;
                    }
                }
                ?>

                <div id="accordion">

                    <div class="row m-0 week-days-tabs-container" style="justify-content: space-around;">
                        <?php
                        $monday = esc_html__( 'Pirmadienis', 'keltas-theme' );
                        $tuesday = esc_html__( 'Antradienis', 'keltas-theme' );
                        $wednesday = esc_html__( 'Trečiadienis', 'keltas-theme' );
                        $thursday = esc_html__( 'Ketvirtadienis', 'keltas-theme' );
                        $friday = esc_html__( 'Penktadienis', 'keltas-theme' );
                        $saturday = esc_html__( 'Šeštadienis', 'keltas-theme' );
                        $sunday = esc_html__( 'Sekmadienis', 'keltas-theme' );

                        $tabs = array("$monday", "$tuesday", "$wednesday", "$thursday", "$friday", "$saturday", "$sunday");
                        $current_week_day = current_time("N");
                        $tabID = 1;
                        foreach ( $tabs as $tab ) { ?>

                            <div class="col-auto p-0 m-0 text-center schedule-navigation-tab">

                                <?php if ( $tabID == $current_week_day ) : ?>
                                    <button class="schedule-navigation-button btn btn-link tab-active" data-toggle="collapse" data-target="#skirtukas-<?php echo $tabID ?>" aria-expanded="true" aria-controls="skirtukas-<?php echo $tabID ?>">
                                        <?php echo $tab ?>
                                    </button>

                                <?php else : ?>
                                    <button class="schedule-navigation-button btn btn-link" data-toggle="collapse" data-target="#skirtukas-<?php echo $tabID ?>" aria-expanded="true" aria-controls="skirtukas-<?php echo $tabID ?>">
                                        <?php echo $tab ?>
                                    </button>
                                <?php endif; ?>
                            </div>

                            <?php
                            $tabID++;
                        } ?>
                    </div>

                    <div class="row m-0">
                        <?php
                        $tabID = 1;
                        $class_active = "show";
                        foreach ( $tabs as $tab ) { ?>

                            <?php
                            if ( $tabID != $current_week_day ) $class_active = "";
                            else $class_active = "show";
                            ?>

                            <div class="col-lg-12">
                                <div id="skirtukas-<?php echo $tabID ?>" class="collapse <?php echo $class_active ?>" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="card-body">

                                        <?php
                                        $data_array = array();
                                        switch ($tabID) {
                                            case "1": $data_array = new ArrayObject($week_one_array); break;
                                            case "2": $data_array = new ArrayObject($week_two_array); break;
                                            case "3": $data_array = new ArrayObject($week_three_array); break;
                                            case "4": $data_array = new ArrayObject($week_four_array); break;
                                            case "5": $data_array = new ArrayObject($week_five_array); break;
                                            case "6": $data_array = new ArrayObject($week_six_array); break;
                                            case "7": $data_array = new ArrayObject($week_seven_array); break;
                                            default: $data_array = null; break;
                                        }

                                        if ( $data_array != null ) {

                                            echo '<div class="row schedule-top">';

                                            echo '<div class="col-lg-3 p-0 schedule-info-block">';
                                            echo '<h2 class="big-title"> Senoji perkėla </h2>';

                                            $map_url = 'https://www.google.lt/maps/place/'. get_field("port-address", get_option( 'page_on_front'));
                                            echo '<p class="schedule-port-address"><a target="_blank" href="'. $map_url .'"><i class="fas fa-map-marker-alt"></i>'. get_field("port-address", get_option( 'page_on_front')) .'</a></p>';

                                            echo '<div class="schedule-info">';
                                            echo '<div class="information-container">';
                                            echo '<p style="white-space: pre-wrap;">'. get_field('schedule-info-port-1') .'</p>';
                                            echo '</div>';
                                            echo '</div>';

                                            echo '<div class="schedule-info-mobile">'; ?>
                                            <button class="btn schedule-info-collapse-button" type="button" data-toggle="collapse" data-target=".multi-collapse" aria-expanded="false" aria-controls="scheduleInfo"> + informacija</button>
                                            <div class="collapse multi-collapse" id="scheduleInfo">
                                                <div class="information-container">
                                                    <?php echo '<p style="white-space: pre-wrap;">'. get_field('schedule-info-port-1') .'</p>'; ?>
                                                </div>
                                            </div>
                                            <?php ;
                                            echo '</div>';

                                            $transport_type = get_field('schedule-ferry-transport-1');

                                            echo '<span class="ferry-transport senoji-perkela" id="'. $transport_type .'">';
                                            echo '</span>';

                                            echo '</div>';

                                            echo '<div class="col-md p-0">';
                                            echo '<h4 class="sm-2-title">Iš Klaipėdos į Smiltynę</h4>';

                                            echo '<div class="col-lg">';
                                            echo '<div class="row">';

                                            echo '<div class="col-6 p-0">';
                                            $valid_hours = array();
                                            // $terminal . "@". $source ."@". $destination ."@". $departure_time ."@". $category  ."@". $exception
                                            for ($a = 0; count($data_array) != $a ;$a++) {
                                                $line_array = explode('@', $data_array[$a]);
                                                if ($line_array[0] == "1" && $line_array[4] == "1" && $line_array[1] == "KLAIPĖDA" && $line_array[2] == "SMILTYNĖ") {

                                                    $departure_time = $line_array[3];
                                                    if ($line_array[5] != "") $departure_time = $line_array[3]. '<span class="ati" >*</span>';

                                                    if ( !in_array($departure_time, $valid_hours )) array_push($valid_hours, $departure_time);
                                                }
                                            }
                                            $next_start = count($valid_hours) / 2;
                                            $current_hour = "";

                                            for ($k = 0; round($next_start) > $k; $k++) {

                                                $hour = explode(":", $valid_hours[$k])[0];
                                                if ($current_hour != $hour ) {
                                                    $current_hour = $hour;

                                                    $times = explode(":", $valid_hours[$k])[1] . " ";
                                                    $c = 1;
                                                    while ( $c != 5 ) {
                                                        if ( explode(":", $valid_hours[$k])[0] == explode(":",$valid_hours[($k + $c)])[0] ) {
                                                            $times .=  explode(":", $valid_hours[($k + $c)])[1] ." ";
                                                        } $c++;
                                                    }

                                                    echo '<p class="schedule-time"><span class="hour-class">'. $hour .'</span> '.  $times . '</p>';
                                                }
                                            }

                                            echo '</div>';
                                            echo '<div class="col-6 p-0">';
                                            $current_hour = "";
                                            for ($k = round($next_start +0.6); round(count($valid_hours)) > $k; $k++) {

                                                $hour = explode(":", $valid_hours[$k])[0];
                                                if ($current_hour != $hour ) {
                                                    $current_hour = $hour;

                                                    $times = explode(":", $valid_hours[$k])[1] . " ";
                                                    $c = 1;
                                                    while ( $c != 5 ) {
                                                        if ( explode(":", $valid_hours[$k])[0] == explode(":",$valid_hours[($k + $c)])[0] ) {
                                                            $times .=  explode(":", $valid_hours[($k + $c)])[1] ." ";
                                                        } $c++;
                                                    }

                                                    echo '<p class="schedule-time"><span class="hour-class">'. $hour .'</span> '.  $times . '</p>';
                                                }
                                            }
                                            echo '</div>';

                                            echo '</div>';
                                            echo '</div>';
                                            echo '</div>';

                                            echo '<div class="col-md p-0">';
                                            echo '<h4 class="sm-2-title">Iš Smiltynės į Klaipėdą</h4>';

                                            echo '<div class="col-lg">';
                                            echo '<div class="row">';

                                            echo '<div class="col-6 p-0">';
                                            $valid_hours = array();
                                            // $terminal . "@". $source ."@". $destination ."@". $departure_time ."@". $category
                                            for ($a = 0; count($data_array) != $a ;$a++) {
                                                $line_array = explode('@', $data_array[$a]);
                                                if ($line_array[0] == "1" && $line_array[4] == "1" && $line_array[1] == "SMILTYNĖ" && $line_array[2] == "KLAIPĖDA") {

                                                    $departure_time = $line_array[3];
                                                    if ($line_array[5] != "") $departure_time = $line_array[3]. '<span class="ati" >*</span>';

                                                    if ( !in_array($departure_time, $valid_hours )) array_push($valid_hours, $departure_time);
                                                }
                                            }
                                            $next_start = count($valid_hours) / 2;
                                            $current_hour = "";

                                            for ($k = 0; round($next_start) > $k; $k++) {

                                                $hour = explode(":", $valid_hours[$k])[0];
                                                if ($current_hour != $hour ) {
                                                    $current_hour = $hour;

                                                    $times = explode(":", $valid_hours[$k])[1] . " ";
                                                    $c = 1;
                                                    while ( $c != 5 ) {
                                                        if ( explode(":", $valid_hours[$k])[0] == explode(":",$valid_hours[($k + $c)])[0] ) {
                                                            $times .=  explode(":", $valid_hours[($k + $c)])[1] ." ";
                                                        } $c++;
                                                    }

                                                    echo '<p class="schedule-time"><span class="hour-class">'. $hour .'</span> '.  $times . '</p>';
                                                }
                                            }
                                            echo '</div>';
                                            echo '<div class="col-6 p-0">';
                                            $current_hour = "";
                                            for ($k = round($next_start +0.6); round(count($valid_hours)) > $k; $k++) {

                                                $hour = explode(":", $valid_hours[$k])[0];
                                                if ($current_hour != $hour ) {
                                                    $current_hour = $hour;

                                                    $times = explode(":", $valid_hours[$k])[1] . " ";
                                                    $c = 1;
                                                    while ( $c != 5 ) {
                                                        if ( explode(":", $valid_hours[$k])[0] == explode(":",$valid_hours[($k + $c)])[0] ) {
                                                            $times .=  explode(":", $valid_hours[($k + $c)])[1] ." ";
                                                        } $c++;
                                                    }

                                                    echo '<p class="schedule-time"><span class="hour-class">'. $hour .'</span> '.  $times . '</p>';
                                                }
                                            }
                                            echo '</div>';

                                            echo '</div>';
                                            echo '</div>';
                                            echo '</div>';

                                            echo '<div class="col-md">';
                                            echo '<div class="row">';

                                            $valid_hours = array();
                                            // $terminal . "@". $source ."@". $destination ."@". $departure_time ."@". $category
                                            for ($a = 0; count($data_array) != $a ;$a++) {
                                                $line_array = explode('@', $data_array[$a]);
                                                if ($line_array[0] == "1" && $line_array[1] == "KLAIPĖDA" && $line_array[2] == "JŪRŲ MUZIEJUS" && $line_array[4] == "2") {

                                                    $departure_time = $line_array[3];
                                                    if ($line_array[5] != "") $departure_time = $line_array[3]. '<span class="ati" >*</span>';

                                                    if ( !in_array($departure_time, $valid_hours )) array_push($valid_hours, $departure_time);
                                                }
                                            }

                                            if ( count($valid_hours) != 0 ) :
                                                echo '<h4 class="sm-2-title">Iš Klaipėdos į Jūrų muziejų</h4>';

                                                echo '<div class="col-lg">';
                                                echo '<div class="row">';

                                                echo '<div class="col-6 p-0">';
                                                $next_start = count($valid_hours) / 2;

                                                $current_hour = "";
                                                for ($k = 0; $next_start > $k; $k++) {

                                                    $hour = explode(":", $valid_hours[$k])[0];
                                                    if ($current_hour != $hour ) {
                                                        $current_hour = $hour;

                                                        $times = explode(":", $valid_hours[$k])[1] . " ";
                                                        $c = 1;
                                                        while ( $c != 5 ) {
                                                            if ( explode(":", $valid_hours[$k])[0] == explode(":",$valid_hours[($k + $c)])[0] ) {
                                                                $times .=  explode(":", $valid_hours[($k + $c)])[1] ." ";
                                                            } $c++;
                                                        }

                                                        echo '<p class="schedule-time"><span class="hour-class">'. $hour .'</span> '.  $times . '</p>';
                                                    }
                                                }
                                                echo '</div>';
                                                echo '<div class="col-6 p-0">';
                                                $current_hour = "";
                                                for ($k = round($next_start); round(count($valid_hours)) != $k; $k++) {

                                                    $hour = explode(":", $valid_hours[$k])[0];
                                                    if ($current_hour != $hour ) {
                                                        $current_hour = $hour;

                                                        $times = explode(":", $valid_hours[$k])[1] . " ";
                                                        $c = 1;
                                                        while ( $c != 5 ) {
                                                            if ( explode(":", $valid_hours[$k])[0] == explode(":",$valid_hours[($k + $c)])[0] ) {
                                                                $times .=  explode(":", $valid_hours[($k + $c)])[1] ." ";
                                                            } $c++;
                                                        }

                                                        echo '<p class="schedule-time"><span class="hour-class">'. $hour .'</span> '.  $times . '</p>';
                                                    }
                                                }
                                                echo '</div>';

                                                echo '</div>';
                                                echo '</div>';
                                            endif;
                                            echo '</div>';

                                            echo '<div class="row">';

                                            $valid_hours = array();
                                            for ($a = 0; count($data_array) != $a ;$a++) {
                                                $line_array = explode('@', $data_array[$a]);
                                                if ($line_array[0] == "1" && $line_array[1] == "JŪRŲ MUZIEJUS" && $line_array[2] == "SMILTYNĖ" && $line_array[4] == "2") {

                                                    $departure_time = $line_array[3];
                                                    if ($line_array[5] != "") $departure_time = $line_array[3]. '<span class="ati" >*</span>';

                                                    if ( !in_array($departure_time, $valid_hours )) array_push($valid_hours, $departure_time);
                                                }
                                            }

                                            if ( count($valid_hours) != 0 ) :

                                                echo '<h4 class="sm-2-title">Iš Jūrų muziejaus į Smiltynę</h4>';

                                                echo '<div class="col-lg">';
                                                echo '<div class="row">';

                                                echo '<div class="col-6 p-0">';
                                                $next_start = count($valid_hours) / 2;
                                                $current_hour = "";
                                                for ($k = 0; round($next_start) > $k; $k++) {

                                                    $hour = explode(":", $valid_hours[$k])[0];
                                                    if ($current_hour != $hour ) {
                                                        $current_hour = $hour;

                                                        $times = explode(":", $valid_hours[$k])[1] . " ";
                                                        $c = 1;
                                                        while ( $c != 5 ) {
                                                            if ( explode(":", $valid_hours[$k])[0] == explode(":",$valid_hours[($k + $c)])[0] ) {
                                                                $times .=  explode(":", $valid_hours[($k + $c)])[1] ." ";
                                                            } $c++;
                                                        }

                                                        echo '<p class="schedule-time"><span class="hour-class">'. $hour .'</span> '.  $times . '</p>';
                                                    }
                                                }
                                                echo '</div>';
                                                echo '<div class="col-6 p-0">';
                                                $current_hour = "";
                                                for ($k = round($next_start); round(count($valid_hours)) > $k; $k++) {

                                                    $hour = explode(":", $valid_hours[$k])[0];
                                                    if ($current_hour != $hour ) {
                                                        $current_hour = $hour;

                                                        $times = explode(":", $valid_hours[$k])[1] . " ";
                                                        $c = 1;
                                                        while ( $c != 5 ) {
                                                            if ( explode(":", $valid_hours[$k])[0] == explode(":",$valid_hours[($k + $c)])[0] ) {
                                                                $times .=  explode(":", $valid_hours[($k + $c)])[1] ." ";
                                                            } $c++;
                                                        }

                                                        echo '<p class="schedule-time"><span class="hour-class">'. $hour .'</span> '.  $times . '</p>';
                                                    }
                                                }
                                                echo '</div>';

                                                echo '</div>';
                                                echo '</div>';
                                            endif;
                                            echo '</div>';

                                            echo '<div class="row">';

                                            $valid_hours = array();
                                            for ($a = 0; count($data_array) != $a ;$a++) {
                                                $line_array = explode('@', $data_array[$a]);
                                                if ($line_array[0] == "1" && $line_array[1] == "SMILTYNĖ" && $line_array[2] == "JŪRŲ MUZIEJUS" && $line_array[4] == "2") {

                                                    $departure_time = $line_array[3];
                                                    if ($line_array[5] != "") $departure_time = $line_array[3]. '<span class="ati" >*</span>';

                                                    if ( !in_array($departure_time, $valid_hours )) array_push($valid_hours, $departure_time);
                                                }
                                            }

                                            if ( count($valid_hours) != 0 ) :

                                                echo '<h4 class="sm-2-title">Iš Smiltynės į Jūrų muziejų</h4>';

                                                echo '<div class="col">';
                                                echo '<div class="row">';

                                                echo '<div class="col-6 p-0">';

                                                $next_start = count($valid_hours) / 2;
                                                $current_hour = "";

                                                for ($k = 0; round($next_start) > $k; $k++) {

                                                    $hour = explode(":", $valid_hours[$k])[0];
                                                    if ($current_hour != $hour ) {
                                                        $current_hour = $hour;

                                                        $times = explode(":", $valid_hours[$k])[1] . " ";
                                                        $c = 1;
                                                        while ( $c != 5 ) {
                                                            if ( explode(":", $valid_hours[$k])[0] == explode(":",$valid_hours[($k + $c)])[0] ) {
                                                                $times .=  explode(":", $valid_hours[($k + $c)])[1] ." ";
                                                            } $c++;
                                                        }

                                                        echo '<p class="schedule-time"><span class="hour-class">'. $hour .'</span> '.  $times . '</p>';
                                                    }
                                                }
                                                echo '</div>';
                                                echo '<div class="col-6 p-0">';
                                                $current_hour = "";
                                                for ($k = round($next_start); round(count($valid_hours)) > $k; $k++) {

                                                    $hour = explode(":", $valid_hours[$k])[0];
                                                    if ($current_hour != $hour ) {
                                                        $current_hour = $hour;

                                                        $times = explode(":", $valid_hours[$k])[1] . " ";
                                                        $c = 1;
                                                        while ( $c != 5 ) {
                                                            if ( explode(":", $valid_hours[$k])[0] == explode(":",$valid_hours[($k + $c)])[0] ) {
                                                                $times .=  explode(":", $valid_hours[($k + $c)])[1] ." ";
                                                            } $c++;
                                                        }

                                                        echo '<p class="schedule-time"><span class="hour-class">'. $hour .'</span> '.  $times . '</p>';
                                                    }
                                                }
                                                echo '</div>';

                                                echo '</div>';
                                                echo '</div>';
                                            endif;

                                            echo '</div>';

                                            echo '<div class="row">';

                                            $valid_hours = array();
                                            for ($a = 0; count($data_array) != $a ;$a++) {
                                                $line_array = explode('@', $data_array[$a]);
                                                if ($line_array[0] == "1" && $line_array[1] == "JŪRŲ MUZIEJUS" && $line_array[2] == "KLAIPĖDA" && $line_array[4] == "2") {

                                                    $departure_time = $line_array[3];
                                                    if ($line_array[5] != "") $departure_time = $line_array[3]. '<span class="ati" >*</span>';

                                                    if ( !in_array($departure_time, $valid_hours )) array_push($valid_hours, $departure_time);
                                                }
                                            }

                                            if ( count($valid_hours) != 0 ) :

                                                echo '<h4 class="sm-2-title">Iš Jūrų Muziejaus į Klaipėdą</h4>';

                                                echo '<div class="col">';
                                                echo '<div class="row">';

                                                echo '<div class="col-6 p-0">';
                                                $next_start = count($valid_hours) / 2;
                                                $current_hour = "";

                                                for ($k = 0; round($next_start) > $k; $k++) {

                                                    $hour = explode(":", $valid_hours[$k])[0];
                                                    if ($current_hour != $hour ) {
                                                        $current_hour = $hour;

                                                        $times = explode(":", $valid_hours[$k])[1] . " ";
                                                        $c = 1;
                                                        while ( $c != 5 ) {
                                                            if ( explode(":", $valid_hours[$k])[0] == explode(":",$valid_hours[($k + $c)])[0] ) {
                                                                $times .=  explode(":", $valid_hours[($k + $c)])[1] ." ";
                                                            } $c++;
                                                        }

                                                        echo '<p class="schedule-time"><span class="hour-class">'. $hour .'</span> '.  $times . '</p>';
                                                    }
                                                }
                                                echo '</div>';
                                                echo '<div class="col-6 p-0">';
                                                $current_hour = "";
                                                for ($k = round($next_start); round(count($valid_hours)) > $k; $k++) {

                                                    $hour = explode(":", $valid_hours[$k])[0];
                                                    if ($current_hour != $hour ) {
                                                        $current_hour = $hour;

                                                        $times = explode(":", $valid_hours[$k])[1] . " ";
                                                        $c = 1;
                                                        while ( $c != 5 ) {
                                                            if ( explode(":", $valid_hours[$k])[0] == explode(":",$valid_hours[($k + $c)])[0] ) {
                                                                $times .=  explode(":", $valid_hours[($k + $c)])[1] ." ";
                                                            } $c++;
                                                        }

                                                        echo '<p class="schedule-time"><span class="hour-class">'. $hour .'</span> '.  $times . '</p>';
                                                    }
                                                }
                                                echo '</div>';

                                                echo '</div>';
                                                echo '</div>';
                                            endif;

                                            echo '</div>';


                                            echo '</div>';

                                            echo '</div>';

                                            echo '<div class="row schedule-bottom">';

                                            echo '<div class="col-lg-3 p-0 schedule-info-block">';
                                            echo '<h2 class="big-title"> Naujoji perkėla </h2>';

                                            $map_url = 'https://www.google.lt/maps/place/'. get_field("new-port-address", get_option( 'page_on_front'));
                                            echo '<p class="schedule-port-address"><a target="_blank" href="'. $map_url .'"><i class="fas fa-map-marker-alt"></i>'. get_field("new-port-address", get_option( 'page_on_front')) .'</a></p>';

                                            echo '<div class="schedule-info">';
                                            echo '<div class="information-container">';
                                            echo '<p style="white-space: pre-wrap;">'. get_field('schedule-info-port-2') .'</p>';
                                            echo '</div>';
                                            echo '</div>';

                                            echo '<div class="schedule-info-mobile">'; ?>
                                            <button class="btn schedule-info-collapse-button" type="button" data-toggle="collapse" data-target=".multi-collapse" aria-expanded="false" aria-controls="scheduleInfo2"> + informacija</button>
                                            <div class="collapse multi-collapse" id="scheduleInfo2">
                                                <div class="information-container">
                                                    <?php  echo '<p style="white-space: pre-wrap;">'. get_field('schedule-info-port-2') .'</p>'; ?>
                                                </div>
                                            </div>
                                            <?php ;
                                            echo '</div>';

                                            $transport_type = get_field('schedule-ferry-transport-2');
                                            echo '<span class="ferry-transport naujoji-perkela" id="'. $transport_type .'">';
                                            echo '</span>';

                                            echo '</div>';

                                            echo '<div class="col-md-4 p-0">';
                                            echo '<h4 class="sm-2-title">Iš Klaipėdos į Smiltynę</h4>';

                                            echo '<div class="col">';
                                            echo '<div class="row">';

                                            echo '<div class="col-6 p-0">';
                                            $valid_hours = array();
                                            for ($a = 0; count($data_array) != $a ;$a++) {
                                                $line_array = explode('@', $data_array[$a]);
                                                if ($line_array[0] == "2" && $line_array[1] == "KLAIPĖDA" && $line_array[4] == "1") {

                                                    $departure_time = $line_array[3];
                                                    if ($line_array[5] != "") $departure_time = $line_array[3]. '<span class="ati" >*</span>';

                                                    if ( !in_array($departure_time, $valid_hours )) array_push($valid_hours, $departure_time);
                                                }
                                            }
                                            $next_start = count($valid_hours) / 2;

                                            $current_hour = "";
                                            for ($k = 0; round($next_start) > $k; $k++) {

                                                $hour = explode(":", $valid_hours[$k])[0];
                                                if ($current_hour != $hour ) {
                                                    $current_hour = $hour;

                                                    $times = explode(":", $valid_hours[$k])[1] . " ";
                                                    $c = 1;
                                                    while ( $c != 5 ) {
                                                        if ( explode(":", $valid_hours[$k])[0] == explode(":",$valid_hours[($k + $c)])[0] ) {
                                                            $times .=  explode(":", $valid_hours[($k + $c)])[1] ." ";
                                                        } $c++;
                                                    }

                                                    echo '<p class="schedule-time"><span class="hour-class">'. $hour .'</span> '.  $times . '</p>';
                                                }
                                            }
                                            echo '</div>';
                                            echo '<div class="col-6 p-0">';
                                            $current_hour = "";
                                            for ($k = round($next_start +0.6); round(count($valid_hours)) > $k; $k++) {

                                                $hour = explode(":", $valid_hours[$k])[0];
                                                if ($current_hour != $hour ) {
                                                    $current_hour = $hour;

                                                    $times = explode(":", $valid_hours[$k])[1] . " ";
                                                    $c = 1;
                                                    while ( $c != 5 ) {
                                                        if ( explode(":", $valid_hours[$k])[0] == explode(":",$valid_hours[($k + $c)])[0] ) {
                                                            $times .=  explode(":", $valid_hours[($k + $c)])[1] ." ";
                                                        } $c++;
                                                    }

                                                    echo '<p class="schedule-time"><span class="hour-class">'. $hour .'</span> '.  $times . '</p>';
                                                }
                                            }
                                            echo '</div>';

                                            echo '</div>';
                                            echo '</div>';
                                            echo '</div>';

                                            echo '<div class="col-md-4 p-0">';
                                            echo '<h4 class="sm-2-title">Iš Smiltynės į Klaipėdą</h4>';

                                            echo '<div class="col">';
                                            echo '<div class="row">';

                                            echo '<div class="col-6 p-0">';
                                            $valid_hours = array();
                                            for ($a = 0; count($data_array) != $a ;$a++) {
                                                $line_array = explode('@', $data_array[$a]);
                                                if ($line_array[0] == "2" && $line_array[1] == "SMILTYNĖ" && $line_array[4] == "1") {

                                                    $departure_time = $line_array[3];
                                                    if ($line_array[5] != "") $departure_time = $line_array[3]. '<span class="ati" >*</span>';

                                                    if ( !in_array($departure_time, $valid_hours )) array_push($valid_hours, $departure_time);
                                                }
                                            }
                                            $next_start = count($valid_hours) / 2;
                                            $current_hour = "";

                                            for ($k = 0; round($next_start) > $k; $k++) {

                                                $hour = explode(":", $valid_hours[$k])[0];
                                                if ($current_hour != $hour ) {
                                                    $current_hour = $hour;

                                                    $times = explode(":", $valid_hours[$k])[1] . " ";
                                                    $c = 1;
                                                    while ( $c != 5 ) {
                                                        if ( explode(":", $valid_hours[$k])[0] == explode(":",$valid_hours[($k + $c)])[0] ) {
                                                            $times .=  explode(":", $valid_hours[($k + $c)])[1] ." ";
                                                        } $c++;
                                                    }

                                                    echo '<p class="schedule-time"><span class="hour-class">'. $hour .'</span> '.  $times . '</p>';
                                                }
                                            }

                                            echo '</div>';
                                            echo '<div class="col-6 p-0">';
                                            $current_hour = "";
                                            for ($k = round($next_start +0.6); round(count($valid_hours)) > $k; $k++) {

                                                $hour = explode(":", $valid_hours[$k])[0];
                                                if ($current_hour != $hour ) {
                                                    $current_hour = $hour;

                                                    $times = explode(":", $valid_hours[$k])[1] . " ";
                                                    $c = 1;
                                                    while ( $c != 5 ) {
                                                        if ( explode(":", $valid_hours[$k])[0] == explode(":",$valid_hours[($k + $c)])[0] ) {
                                                            $times .=  explode(":", $valid_hours[($k + $c)])[1] ." ";
                                                        } $c++;
                                                    }

                                                    echo '<p class="schedule-time"><span class="hour-class">'. $hour .'</span> '.  $times . '</p>';
                                                }
                                            }
                                            echo '</div>';

                                            echo '</div>';
                                            echo '</div>';
                                            echo '</div>';

                                            echo '</div>';

                                        } ?>
                                    </div>
                                </div>
                            </div>

                            <?php
                            $tabID++;
                        } ?>
                    </div>

                </div>

            </div>
        </div>

    </div>
    </div>

    <script>
        $( document ).ready(function() {
            $(".schedule-navigation-tab button").click(function () {

                $(".week-days-tabs-container button").each(function (e) {
                    $(this).removeClass("tab-active");
                });
                $(this).addClass("tab-active");
            });

            var svg;
            var prevTab = null;
            var tab = $(".tab-active").attr("data-target");
            $(tab + ' .naujoji-perkela').empty();
            $(tab + ' .senoji-perkela').empty();

            if ($(tab + ' .naujoji-perkela').attr("id") == "all") {
                svg = '<?php echo '<img src="' . get_template_directory_uri() . '/assets/images/transport-all.png' . '" />' ?>';
                $(tab + ' .naujoji-perkela').append(svg);
            } else {
                svg = '<?php echo '<img src="' . get_template_directory_uri() . '/assets/images/transport.jpg' . '" / >' ?>';
                $( tab+' .naujoji-perkela').append(svg);
            }

            if ($( tab+' .senoji-perkela').attr("id") == "all") {
                svg = '<?php echo '<img src="' . get_template_directory_uri() . '/assets/images/transport-all.png' . '" />' ?>';
                $( tab+' .senoji-perkela').append(svg);
            } else {
                svg = '<?php echo '<img src="' . get_template_directory_uri() . '/assets/images/transport.jpg' . '" / >' ?>';
                $(tab + ' .senoji-perkela').append(svg);
            }

            prevTab = tab;

            $('.schedule-navigation-button').on('click', function (e) {

                var tab = $(this).attr("data-target");

                $( tab+' .naujoji-perkela').empty();
                $( tab+' .senoji-perkela').empty();
                $( prevTab +' .naujoji-perkela').empty();
                $( prevTab +' .senoji-perkela').empty();

                if ($(tab + ' .naujoji-perkela').attr("id") == "all") {
                    svg = '<?php echo '<img src="' . get_template_directory_uri() . '/assets/images/transport-all.png' . '" />' ?>';
                    $(tab + ' .naujoji-perkela').append(svg);
                } else {
                    svg = '<?php echo '<img src="' . get_template_directory_uri() . '/assets/images/transport.jpg' . '" / >' ?>';
                    $( tab+' .naujoji-perkela').append(svg);
                }

                if ($( tab+' .senoji-perkela').attr("id") == "all") {
                    svg = '<?php echo '<img src="' . get_template_directory_uri() . '/assets/images/transport-all.png' . '" />' ?>';
                    $( tab+' .senoji-perkela').append(svg);
                } else {
                    svg = '<?php echo '<img src="' . get_template_directory_uri() . '/assets/images/transport.jpg' . '" / >' ?>';
                    $(tab + ' .senoji-perkela').append(svg);
                }
                prevTab = tab;
            });
        });
    </script>

<?php get_footer();