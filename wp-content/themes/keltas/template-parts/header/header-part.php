<div class="container header-container-navigation">

    <div class="row logo-container-block" style="float: left">
        <div class="col-lg-auto" style="padding-top: 20px;">
            <div class="header-logo">
                <a style="text-decoration: none" href="<?php echo home_url() ?>">
                    <div><?php get_template_part('assets/svg/logo'); ?></div>
                </a>
            </div>
        </div>
    </div>

    <div class="row top-menu-row">
        <div class="col-lg-10 top-menu-container" style="padding-left: 10px;align-items: baseline;">

            <div class="svg-icon icon-menu" id="svg-search-icon">
                <a><?php get_template_part('assets/svg/search'); ?></a>
                <div class="icon-search-container" id="icon-search-container">
                    <?php $unique_id = esc_attr(uniqid('search-form-')); ?>
                    <form role="search" method="get" class="search-form" action="<?php echo esc_url(home_url('/')); ?>">
                        <input type="search" id="<?php echo $unique_id; ?>" class="search-field"
                               placeholder="<?php echo esc_attr_x('Ieškoti svetainėje', 'placeholder', 'keltas-theme'); ?>"
                               value="<?php echo get_search_query(); ?>" name="s"/>
                        <button type="submit" class="search-submit"><?php get_template_part('assets/svg/search'); ?></button>
                    </form>
                </div>
            </div>

            <?php
            $locations = get_nav_menu_locations();
            $menu = wp_get_nav_menu_object($locations['icon-menu']);
            $menu_items = wp_get_nav_menu_items($menu->term_id);

            $a = 0;
            foreach ((array)$menu_items as $key => $menu_item) {
                if ($menu_item->menu_item_parent != 0) {
                    continue;
                }

                $url = $menu_item->url;
                echo '<div class="svg-icon icon-menu">';
                echo '<a href="' . $url . '">';
                if ($a == 0) {
                    echo get_template_part('assets/svg/camera');
                } else {
                    echo get_template_part('assets/svg/disabled');
                }
                echo '</a>';
                echo '</div>';
                $a++;
            }
            ?>

            <div class="weather-container">
                <span><?php echo esc_html__('Orai Klaipėdoje: ', 'keltas-theme'); ?> <strong id="opentemperature"><?php echo "0°C" ?></strong></span>
                <span style="border-left: 1px solid;"><?php echo esc_html__('Vėjo greitis: ', 'keltas-theme'); ?> <strong id="openwindspeed"><?php echo "0 m/s" ?></strong></span>
                <span style="border-left: 1px solid;"><?php echo esc_html__('Drėgnumas: ', 'keltas-theme'); ?> <strong id="openhumidity"><?php echo "0%" ?></strong></span>
            </div>
            <a href="<?php echo esc_html__('/profilis', 'keltas-theme') ?>" class="pofile_login_link"
               style="cursor:pointer;float: left;margin-left: 10%;color: #ffffff;display: flex;font-family: 'Effra Light', sans-serif;align-items: center;">
                <?php echo '<img style="height: 20px;width: 20px;margin-right: 5px;" alt="" src="' . get_template_directory_uri() . '/assets/images/icon_profile.png' . '" />' ?>
                <?php
                if (is_logged_in()) {
                    if (isset($_SESSION['user_name'])) {
                        echo $_SESSION['user_name'];
                    } else {
                        Api::getProfile($_SESSION['user_id']);
                        if(isset($_SESSION['user_name'])){
                            echo $_SESSION['user_name'];
                        } else {
                            echo esc_html__('Profilis', 'keltas-theme');
                        }
                    }
                } else {
                    echo esc_html__('Prisijungti', 'keltas-theme');
                }
                ?>
            </a>

            <?php if (is_logged_in()): ?>
                <a href="<?php echo get_home_url() . '?api=logout' ?>" class="button-logout" style="cursor:pointer;float: left;margin-left: 2%;display:
                            inherit;">
                    <?php echo '<img style="height: 20px;width: 20px;" alt="" src="' . get_template_directory_uri() . '/assets/images/icon_logout.png' . '" />' ?>
                </a>
            <?php endif; ?>

            <div class="extra-info extra-info-mobile">

                <p class="short-number"><a
                            href="tel:<?php the_field("short-number", get_option('page_on_front')) ?>"> <?php the_field("short-number", get_option('page_on_front')) ?></a></p>

                <div class="social-icon">
                    <?php if (get_field("facebook", get_option('page_on_front')) != null) : ?>
                        <a target="_blank" href="<?php the_field("facebook", get_option('page_on_front')) ?>">
                            <?php get_template_part('assets/svg/facebook'); ?>
                        </a>
                    <?php endif; ?>

                    <?php if (get_field("instagram", get_option('page_on_front')) != null) : ?>
                        <a target="_blank" href="<?php the_field("instagram", get_option('page_on_front')) ?>">
                            <?php get_template_part('assets/svg/instagram'); ?>
                        </a>
                    <?php endif; ?>
                </div>

                <div class="language-switcher">
                    <?php if (has_nav_menu('header-menu')) :
                        wp_nav_menu(array(
                            'menu_class' => 'header-navigation-top',
                            'theme_location' => 'side-menu',
                            'depth' => '2',
                            'fallback_cb' => null,
                            'container' => false,
                        ));
                    endif; ?>
                </div>

            </div>

        </div>

        <div class="menu-main-mobile">
            <i class="fas fa-bars"></i>

            <div class="hidden-main-menu-mobile">
                <i class="fas fa-times close-mobile-navigation"></i>

                <div class="top-menu-mobile-top">
                    <div class="top-menu-mobile-2">
                        <?php
                        $locations = get_nav_menu_locations();
                        $menu = wp_get_nav_menu_object($locations['icon-menu-mobile']);
                        $menu_items = wp_get_nav_menu_items($menu->term_id);

                        $a = 0;
                        foreach ((array)$menu_items as $key => $menu_item) {
                            if ($menu_item->menu_item_parent != 0) {
                                continue;
                            }

                            $url = $menu_item->url;
                            //$icon = get_field("menu-icon", $menu_item);
                            //if ( get_field("menu-icon", $menu_item) != null ) :
                            echo '<div class="svg-icon icon-menu">';
                            echo '<a href="' . $url . '">';
                            if ($a == 0) {
                                echo get_template_part('assets/svg/camera');
                            } else {
                                echo get_template_part('assets/svg/disabled');
                            }
                            echo '</a>';
                            echo '</div>';
                            //endif;
                            $a++;
                        }
                        ?>

                        <div class="svg-icon icon-menu">
                            <?php if (get_field("facebook", get_option('page_on_front')) != null) : ?>
                                <a target="_blank" href="<?php the_field("facebook", get_option('page_on_front')) ?>">
                                    <?php get_template_part('assets/svg/facebook'); ?>
                                </a>
                            <?php endif; ?>
                        </div>

                        <div class="svg-icon icon-menu">
                            <?php if (get_field("instagram", get_option('page_on_front')) != null) : ?>
                                <a target="_blank" href="<?php the_field("instagram", get_option('page_on_front')) ?>">
                                    <?php get_template_part('assets/svg/instagram'); ?>
                                </a>
                            <?php endif; ?>
                        </div>

                        <div class="language-switcher">
                            <ul>
                                <?php pll_the_languages(); ?>
                            </ul>
                        </div>

                    </div>

                    <div class="search-mobile-2">
                        <div class="icon-search-container-mobile">
                            <?php $unique_id = esc_attr(uniqid('search-form-')); ?>
                            <form role="search" method="get" class="search-form" action="<?php echo esc_url(home_url('/')); ?>">
                                <input type="search" id="<?php echo $unique_id; ?>" class="search-field"
                                       placeholder="<?php echo esc_attr_x('Ieškoti svetainėje', 'placeholder', 'keltas-theme'); ?>"
                                       value="<?php echo get_search_query(); ?>" name="s"/>
                                <button type="submit" class="search-submit"><?php get_template_part('assets/svg/search'); ?></button>
                            </form>
                        </div>
                    </div>

                    <div class="extra-info extra-info-mobile-2" style="display: flex;">
                        <p class="short-number">
                            <a href="tel:<?php the_field("short-number", get_option('page_on_front')) ?>">
                                <?php the_field("short-number", get_option('page_on_front')) ?>
                            </a>
                        </p>

                        <a href="<?php echo esc_html__('/profilis', 'keltas-theme') ?>" class="pofile_login_link"
                           style="cursor:pointer;float: left;margin-left: 10%;color: #ffffff;display: flex;font-family: 'Effra Light', sans-serif;align-items: center;">
                            <?php echo '<img style="height: 20px;width: 20px;margin-right: 5px;" alt="" src="' . get_template_directory_uri() . '/assets/images/icon_profile.png' . '" />' ?>
                            <?php
                            if (is_logged_in()) {
                                if (isset($_SESSION['user_name'])) {
                                    echo $_SESSION['user_name'];
                                } else {
                                    Api::getProfile($_SESSION['user_id']);
                                    if(isset($_SESSION['user_name'])){
                                        echo $_SESSION['user_name'];
                                    } else {
                                        echo esc_html__('Profilis', 'keltas-theme');
                                    }
                                }
                            } else {
                                echo esc_html__('Prisijungti', 'keltas-theme');
                            }
                            ?>
                        </a>

                        <?php if (is_logged_in()): ?>
                            <a href="<?php echo get_template_directory_uri() . '/logout' ?>" class="button-logout" style="cursor:pointer;float: left;margin-left: 2%;display:
                            inherit;align-self: center;">
                                <?php echo '<img style="height: 20px;width: 20px;margin-left: 20px;" alt="" src="' . get_template_directory_uri() . '/assets/images/icon_logout.png' . '" />' ?>
                            </a>
                        <?php endif; ?>
                    </div>
                </div>

                <?php
                if (has_nav_menu('header-menu')) :
                    wp_nav_menu(array(
                        'menu_id' => 'header-main',
                        'menu_class' => 'header-navigation-top-mobile',
                        'theme_location' => 'header-menu',
                        'depth' => '3',
                        'fallback_cb' => null,
                        'container' => false,
                    ));
                endif; ?>
            </div>
        </div>

        <div class="col-xs main-menu-container">
            <?php
            if (has_nav_menu('header-menu')) : ?>
                <?php
                wp_nav_menu(array(
                    'menu_class' => 'header-navigation-top',
                    'theme_location' => 'header-menu',
                    'depth' => '3',
                    'fallback_cb' => null,
                    'container' => false,
                ));
                ?>
            <?php endif; ?>
        </div>

        <div class="col" style="text-align: center;">
            <div class="extra-info">

                <p class="short-number"><a
                            href="tel:<?php the_field("short-number", get_option('page_on_front')) ?>"> <?php the_field("short-number", get_option('page_on_front')) ?></a></p>
                <div class="short-number-icon"><?php get_template_part('assets/svg/phone-icon'); ?></div>

                <div class="social-icon">
                    <?php if (get_field("facebook", get_option('page_on_front')) != null) : ?>
                        <a target="_blank" href="<?php the_field("facebook", get_option('page_on_front')) ?>">
                            <?php get_template_part('assets/svg/facebook'); ?>
                        </a>
                    <?php endif; ?>

                    <?php if (get_field("instagram", get_option('page_on_front')) != null) : ?>
                        <a target="_blank" href="<?php the_field("instagram", get_option('page_on_front')) ?>">
                            <?php get_template_part('assets/svg/instagram'); ?>
                        </a>
                    <?php endif; ?>
                </div>

                <div class="language-switcher">
                    <?php if (has_nav_menu('header-menu')) :
                        wp_nav_menu(array(
                            'menu_class' => 'header-navigation-top',
                            'theme_location' => 'side-menu',
                            'depth' => '2',
                            'fallback_cb' => null,
                            'container' => false,
                        ));
                    endif; ?>
                </div>

            </div>
        </div>
    </div>

</div>

<script>
    $(document).ready(function () {
        jQuery.ajax({
            url: 'https://api.openweathermap.org/data/2.5/weather?id=598098&APPID=a4a0f34f42664dc1eb19828784e6e990',
            dataType: "json",
            type: "GET",
            success: function (data) {

                var temperature_kelvin = data.main.temp;
                var temperature_celsius = temperature_kelvin - 273.15; // Convert from Kelvin to Celsius
                var humidity = data.main.humidity;
                var wind_speed = data.wind.speed;

                jQuery('#opentemperature').text(temperature_celsius.toFixed(0) + "°C");
                jQuery('#openwindspeed').text(wind_speed.toFixed(1) + " m/s");
                jQuery('#openhumidity').text(humidity + "%");
            }
        });
    });
</script>
