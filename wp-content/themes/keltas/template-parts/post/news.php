<?php
/**
 * Template part for displaying posts
 */
?>

<article id="post-<?php the_ID(); ?>" class="col-md-4 news-article" >

    <header class="entry-header">

        <?php if ( !is_single() ) : ?>

            <?php if ( '' !== get_the_post_thumbnail() ) : ?>

                <div class="post-thumbnail">
                    <a href="<?php the_permalink(); ?>">

                        <?php the_post_thumbnail(); ?>

                    </a>
                </div>

            <?php else : ?>

                <div class="post-thumbnail-no-thumbnail"></div>

            <?php endif; ?>
        <?php endif; ?>

        <?php

        echo '<p class="post-date">' . get_the_date('‘y m d') . '</p>';

        if ( is_single() )
            the_title( '<h1 class="news-title">', '</h1>' );

        elseif ( is_front_page() && is_home() )
            the_title( '<h3 class="news-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );

        else {
            the_title( '<h2 class="news-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
        }

        ?>
    </header>

    <div class="entry-content">
        <?php
        $content = apply_filters( 'the_content', get_the_content() );
        $content = str_replace('„', '"', $content);

        if ( is_single() ) {
            echo $content;
        } else {
            echo wp_trim_words( $content, 20, '...' );
        }
        ?>
    </div>

</article><!-- #post-## -->