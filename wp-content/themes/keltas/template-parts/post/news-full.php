<?php
/**
 * Template part for displaying posts
 */
?>

<div class="news-container">
    <article id="post-<?php the_ID(); ?>" class="">

        <header class="entry-header">

            <?php if ( is_single() ) : ?>
                <?php if ( '' !== get_the_post_thumbnail() ) : ?>
                    <div class="news-post-thumbnail-full">
                        <a href="<?php echo get_the_post_thumbnail_url(); ?>" data-lightbox="lightbox" >
                            <?php the_post_thumbnail(); ?>
                        </a>
                    </div>
                <?php else : ?>
                    <div class="post-thumbnail-no-thumbnail"></div>
                <?php endif; ?>
            <?php endif; ?>

            <?php

            echo '<p class="post-date-full">' . get_the_date('d m ‘y') . '</p>';

            if ( is_single() )
                the_title( '<h1 class="news-title-full">', '</h1>' );

            elseif ( is_front_page() && is_home() )
                the_title( '<h3 class="news-title-full"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );

            else {
                the_title( '<h2 class="news-title-full"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
            }

            ?>
        </header>

        <div class="entry-content">
            <?php
			$content = apply_filters( 'the_content', str_replace('„', '"', get_the_content() ));

            if ( is_single() ) {

                echo '<div class="information-container">';
                    echo table_wrapper($content);
                echo '</div>';

            } else {

                echo wp_trim_words( $content, 20, '...' );
            }
            ?>
        </div>
    </article>
</div>