<?php
/**
 * Template Name: Kamera
 */

if(isset($_GET['isApp']) && $_GET['isApp'] == '1') {
    $data = [];

    if (have_rows('ccctv-repeater')){
        while (have_rows('ccctv-repeater')) {
            the_row();

            $imgTitle = get_sub_field('cctv-name');
            $imgUrl = get_sub_field('cctv-image');

            $data []= [
                'title' => $imgTitle,
                'url' => $imgUrl
            ];
        }
    }

    header('Content-type:application/json;charset=utf-8');
    echo json_encode($data);
    die();
}

get_header();

global $wp;
$url = home_url($wp->request);
$url_array = explode("/", $url);

$part_url = $url_array[0] . "/" . $url_array[1] . "/" . $url_array[2] . "/" . $url_array[3];

if (ICL_LANGUAGE_CODE != 'lt') {
    $part_url = $url_array[0] . "/" . $url_array[1] . "/" . $url_array[2] . "/" . $url_array[3] . "/" . $url_array[4];
}
$level_one_page_id = url_to_postid($part_url);

$background = 'background: url(' . get_the_post_thumbnail_url() . ')';
if (get_the_post_thumbnail_url() == null) {
    $background = 'background: url(' . get_the_post_thumbnail_url($level_one_page_id) . ')';
}
?>

    <div class="header-image" style="<?php echo $background ?>"></div>
    <div class="container" style="margin: auto; max-width: 1420px; padding: 50px 0;">

        <div class="row m-0">
            <div class="col-lg-3">
                <div class="sidebar-container">

                    <h4 class="mid-title"><?php echo get_the_title($level_one_page_id) ?></h4>

                    <?php
                    $locations = get_nav_menu_locations();
                    $menu = wp_get_nav_menu_object($locations['header-menu']);
                    $menu_items = wp_get_nav_menu_items($menu->term_id);

                    $menu_id = 0;
                    foreach ((array)$menu_items as $key => $menu_item) {

                        if (strcmp(get_the_title($level_one_page_id), $menu_item->title) == 0) {
                            $menu_id = $menu_item->db_id;
                        }
                    }

                    foreach ((array)$menu_items as $key => $menu_item) {


                        if ($menu_item->menu_item_parent == $menu_id) {
                            $title = $menu_item->title;
                            $url = $menu_item->url;

                            echo '<a class="archive-filter-link" href="' . $url . '">' . $title . '</a><br>';
                        }
                    } ?>
                </div>
            </div>
            <div class="col-lg">
                <div class="row m-0 mb-4">
                    <h1 class="big-title"><?php echo get_the_title() ?></h1>
                </div>

                <div class="row">
                    <?php if (have_rows('ccctv-repeater')): ?>
                        <?php while (have_rows('ccctv-repeater')) : the_row() ?>
                            <div class="col-lg-5 pb-4">
                                <div class="camera-holder">
                                    <img alt="" width="400" height="400" src="<?php echo get_sub_field('cctv-image') . '?' ?>">
                                </div>
                                <div class="sm-title"><?php echo get_sub_field('cctv-name') ?></div>
                            </div>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            </div>

        </div>
    </div>

    <script>
        var loading_cameras = false;

        $(document).ready(function () {
            setInterval(function () {
                if (loading_cameras) {
                    return;
                }

                loading_cameras = true;

                $('.camera-holder').each((index, item) => {
                    var real_src = $(item).find('img').attr('src');
                    var src = real_src.substring(0, real_src.indexOf('?'));
                    var image = new Image();
                    image.alt = "";
                    image.width = 400;
                    image.height = 400;
                    image.src = src + '?' + (new Date()).getTime();
                    image.onerror = function () {
                    };
                    image.onload = function () {
                        $(item).html(image);
                    }
                });

                loading_cameras = false;
            }, 5000);
        });

    </script>

<?php get_footer();
