<?php
/**
 * Template part for displaying page content in page.php
 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="information-container">
		<?php
			$content = apply_filters( 'the_content', str_replace('„', '"', get_the_content() ));
			echo table_wrapper($content);
		?>
	</div>
</article>

<script>
    $(document).ready(function() {
        var active = false;
        $('.privacy-policy-consent').on('click', function (e) {

            if (active == false) {
                $('.consent-checkmark-after').css('display', "block");
                active = true;
            }
            else {
                $('.consent-checkmark-after').css('display', "none");
                active = false;
            }
        });
    });
</script>