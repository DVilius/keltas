<?php
/**
 * Template part for displaying page content in page.php
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<a href="<?php echo get_permalink() ?>">
		<h4 class="search-title"><?php echo get_the_title() ?></h4>
	</a>

</article>