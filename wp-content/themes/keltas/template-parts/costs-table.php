<?php
/**
 * Template Name: Kainoraštis
 */

if(isset($_GET['isApp']) && $_GET['isApp'] == '1') {
    header('Content-type:application/json;charset=utf-8');
    echo json_encode(get_field('informacion-wysiwyg'));
    die();
}

get_header();

global $wp;
$url = home_url( $wp->request );
$url_array = explode("/", $url);

$part_url = $url_array[0] ."/". $url_array[1] ."/". $url_array[2] ."/". $url_array[3];
$level_one_page_id = url_to_postid($part_url);
?>

<?php
$background = 'background: url('. get_the_post_thumbnail_url() .')';
if ( get_the_post_thumbnail_url() == null ) $background = 'background: url('. get_the_post_thumbnail_url( get_option( "page_for_posts" ) ) .')';
?>

    <div class="header-image" style="<?php echo $background ?>"></div>

    <div class="container inside-page" style="margin: auto; max-width: 1420px; padding: 50px 0;">

        <div class="row m-0">
            <h1 class="big-title"><?php echo get_the_title(); ?></h1>
        </div>

        <div class="row m-0">
            <div class="col-lg-12 p-0">
                <div class="costs-table-container">
                    <div class="information-container table-container">
                        <?php echo get_field('costs-table') ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row m-0">
            <div class="col-lg-12 p-0">
                <div class="cost-information-container">
                    <div class="information-container">
                        <?php echo get_field('informacion-wysiwyg') ?>
                    </div>
                </div>
            </div>
        </div>

    </div>
    </div>

<?php get_footer();