<?php

$locale = isset($_GET['locale']) ? $_GET['locale'] : null;
$dateToPick = isset($_GET['date']) ? $_GET['date'] : date('Y-m-d');

$apiContents = Api::timeTable3($locale, $dateToPick);
$groupedEntries = json_decode($apiContents, true);

$monday = esc_html__('Pirmadienis', 'keltas-theme');
$tuesday = esc_html__('Antradienis', 'keltas-theme');
$wednesday = esc_html__('Trečiadienis', 'keltas-theme');
$thursday = esc_html__('Ketvirtadienis', 'keltas-theme');
$friday = esc_html__('Penktadienis', 'keltas-theme');
$saturday = esc_html__('Šeštadienis', 'keltas-theme');
$sunday = esc_html__('Sekmadienis', 'keltas-theme');

$tabs = array(
    "$monday",
    "$tuesday",
    "$wednesday",
    "$thursday",
    "$friday",
    "$saturday",
    "$sunday"
);


$current_week_day = date('N', strtotime($dateToPick));
$tabID = 1;

function getTimeEntriesRows($entries, $ferryID, $ferryType) {
    $timeElements = [];
    foreach ($entries as $dayEntry) {
        foreach ($dayEntry as $ferryEntry) {
            foreach ($ferryEntry as $ferry) {
                if ($ferry['ferry_type'] === $ferryType && $ferry['id'] === $ferryID) {
                    $timeElements[] = $ferry;
                }
            }
        }
    }

    usort($timeElements, function ($a, $b) {
        return $a['hour'] - $b['hour'];
    });

    $firstHalf = array_slice($timeElements, 0, round(count($timeElements) / 2));
    $secondHalf = array_slice($timeElements, round(count($timeElements) / 2));

    return [
        $firstHalf,
        $secondHalf
    ];
}

?>

<div class="row m-0">
    <div class="col-lg-16 pt-4">
        <div id="accordion">
            <div class="row m-0 week-days-tabs-container" style="justify-content: space-around;">
                <?php

                foreach ($tabs as $tab) { ?>
                    <div class="col-auto p-0 m-0 text-center schedule-navigation-tab">

                        <?php if ($tabID == $current_week_day) : ?>
                            <button class="schedule-navigation-button btn btn-link tab-active" data-toggle="collapse" data-target="<?php echo $tabID ?>"
                                    aria-expanded="true">
                                <?php echo $tab ?>
                            </button>

                        <?php else : ?>
                            <button class="schedule-navigation-button btn btn-link" data-toggle="collapse" data-target="<?php echo $tabID == 7 ? '0' : $tabID ?>"
                                    aria-expanded="true">
                                <?php echo $tab ?>
                            </button>
                        <?php endif; ?>

                    </div>

                    <?php
                    $tabID++;
                } ?>
            </div>

            <div class="row m-0">
                <div class="col-lg-12">
                    <div class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
                            <div class="row schedule-top">
                                <div class="col-lg-3 p-0 schedule-info-block">
                                    <h2 class="big-title"><?php echo esc_html__("Senoji perkėla", "keltas-theme") ?></h2>

                                    <?php $map_url = 'https://www.google.lt/maps/place/' . get_field("port-address", get_option('page_on_front')); ?>
                                    <p class="schedule-port-address"><a target="_blank" href="<?php echo $map_url ?>"><i
                                                    class="fas fa-map-marker-alt"></i><?php echo get_field("port-address", get_option('page_on_front')) ?></a></p>

                                    <div class="schedule-info">
                                        <div class="information-container">
                                            <p style="white-space: pre-wrap;"><?php echo get_field('schedule-info-port-1', 5) ?></p>
                                        </div>
                                    </div>

                                    <div class="schedule-info-mobile">'
                                        <button class="btn schedule-info-collapse-button" type="button" data-toggle="collapse" data-target=".multi-collapse"
                                                aria-expanded="false" aria-controls="scheduleInfo"> + <?php echo esc_html__("informacija", "keltas-theme") ?></button>
                                        <div class="collapse multi-collapse" id="scheduleInfo">
                                            <div class="information-container">
                                                <?php echo '<p style="white-space: pre-wrap;">' . get_field('schedule-info-port-1', 5) . '</p>' ?>
                                            </div>
                                        </div>
                                    </div>

                                    <span class="ferry-transport senoji-perkela">
                                        <img alt="" src="<?php echo get_template_directory_uri() . '/assets/images/transport.jpg' ?>"/>
                                    </span>
                                    <p class="clock-string" style="font-size: 40px"></p>
                                </div>

                                <div class="col-md p-0">
                                    <h4 class="sm-2-title"><?php echo esc_html__("Iš Klaipėdos į Smiltynę", "keltas-theme") ?></h4>
                                    <div class="col-lg">
                                        <div class="row">
                                            <?php $rows = getTimeEntriesRows($groupedEntries, '16', '1') ?>
                                            <?php if (!empty($rows[0]) || !empty($rows[1])): ?>
                                                <?php foreach ($rows as $row): ?>
                                                    <div class="col-6 p-0">
                                                        <?php foreach ($row as $entry): ?>
                                                            <p class="schedule-time">
                                                                <span class="hour-class"><?php echo $entry['hour'] ?></span>
                                                                <?php foreach ($entry['minutes'] as $minute): ?>
                                                                    <span class="minute-class">
                                                                            <?php echo str_replace('*', '<span style="color:red">*</span>', $minute) ?>
                                                                        </span>
                                                                <?php endforeach; ?>
                                                            </p>
                                                        <?php endforeach; ?>
                                                    </div>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md p-0">
                                    <h4 class="sm-2-title"><?php echo esc_html__("Iš Smiltynės į Klaipėdą", "keltas-theme") ?></h4>
                                    <div class="col-lg">
                                        <div class="row">
                                            <?php $rows = getTimeEntriesRows($groupedEntries, '19', '1') ?>
                                            <?php if (!empty($rows[0]) || !empty($rows[1])): ?>
                                                <?php foreach ($rows as $row): ?>
                                                    <div class="col-6 p-0">
                                                        <?php foreach ($row as $entry): ?>
                                                            <p class="schedule-time">
                                                                <span class="hour-class"><?php echo $entry['hour'] ?></span>
                                                                <?php foreach ($entry['minutes'] as $minute): ?>
                                                                    <span class="minute-class">
                                                                        <?php echo str_replace('*', '<span style="color:red">*</span>', $minute) ?>
                                                                    </span>
                                                                <?php endforeach; ?>
                                                            </p>
                                                        <?php endforeach; ?>
                                                    </div>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md">
                                    <div class="row">
                                        <?php $rows = getTimeEntriesRows($groupedEntries, '20', '1') ?>
                                        <?php if (!empty($rows[0]) || !empty($rows[1])): ?>
                                            <h4 class="sm-2-title"><?php echo esc_html__("Iš Smiltynės į Jūrų muziejų", "keltas-theme") ?></h4>
                                            <?php foreach ($rows as $row): ?>
                                                <div class="col-6 p-0">
                                                    <?php foreach ($row as $entry): ?>
                                                        <p class="schedule-time">
                                                            <span class="hour-class"><?php echo $entry['hour'] ?></span>
                                                            <?php foreach ($entry['minutes'] as $minute): ?>
                                                                <span class="minute-class">
                                                                        <?php echo str_replace('*', '<span style="color:red">*</span>', $minute) ?>
                                                                    </span>
                                                            <?php endforeach; ?>
                                                        </p>
                                                    <?php endforeach; ?>
                                                </div>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </div>
                                    <div class="row">
                                        <?php $rows = getTimeEntriesRows($groupedEntries, '21', '1') ?>
                                        <?php if (!empty($rows[0]) || !empty($rows[1])): ?>
                                            <h4 class="sm-2-title"><?php echo esc_html__("Iš Jūrų Muziejaus į Klaipėdą", "keltas-theme") ?></h4>
                                            <?php foreach ($rows as $row): ?>
                                                <div class="col-6 p-0">
                                                    <?php foreach ($row as $entry): ?>
                                                        <p class="schedule-time">
                                                            <span class="hour-class"><?php echo $entry['hour'] ?></span>
                                                            <?php foreach ($entry['minutes'] as $minute): ?>
                                                                <span class="minute-class">
                                                                        <?php echo str_replace('*', '<span style="color:red">*</span>', $minute) ?>
                                                                    </span>
                                                            <?php endforeach; ?>
                                                        </p>
                                                    <?php endforeach; ?>
                                                </div>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </div>
                                </div>

                            </div>

                            <div class="row schedule-bottom">
                                <div class="col-lg-3 p-0 schedule-info-block">
                                    <h2 class="big-title"><?php echo esc_html__("Naujoji perkėla", "keltas-theme") ?></h2>

                                    <?php $map_url = 'https://www.google.lt/maps/place/' . get_field("new-port-address", get_option('page_on_front')); ?>
                                    <p class="schedule-port-address"><a target="_blank" href="<?php echo $map_url ?>"><i
                                                    class="fas fa-map-marker-alt"></i><?php echo get_field("new-port-address", get_option('page_on_front')) ?></a></p>

                                    <div class="schedule-info">
                                        <div class="information-container">
                                            <p style="white-space: pre-wrap;"><?php echo get_field('schedule-info-port-2', 5) ?></p>
                                        </div>
                                    </div>

                                    <div class="schedule-info-mobile">
                                        <button class="btn schedule-info-collapse-button" type="button" data-toggle="collapse" data-target=".multi-collapse"
                                                aria-expanded="false" aria-controls="scheduleInfo2"> + <?php echo esc_html__("informacija", "keltas-theme") ?></button>
                                        <div class="collapse multi-collapse" id="scheduleInfo2">
                                            <div class="information-container">
                                                <?php echo '<p style="white-space: pre-wrap;">' . get_field('schedule-info-port-2', 5) . '</p>'; ?>
                                            </div>
                                        </div>
                                    </div>

                                    <span class="ferry-transport senoji-perkela">
                                        <img alt="" src="<?php echo get_template_directory_uri() . '/assets/images/transport-all.png' ?>"/>
                                    </span>
                                    <p class="clock-string" style="font-size: 40px"></p>
                                </div>

                                <div class="col-md-4 p-0">
                                    <h4 class="sm-2-title"><?php echo esc_html__("Iš Klaipėdos į Smiltynę", "keltas-theme") ?></h4>

                                    <div class="col">
                                        <div class="row">
                                            <?php $rows = getTimeEntriesRows($groupedEntries, '17', '2') ?>
                                            <?php if (!empty($rows[0]) || !empty($rows[1])): ?>
                                                <?php foreach ($rows as $row): ?>
                                                    <div class="col-6 p-0">
                                                        <?php foreach ($row as $entry): ?>
                                                            <p class="schedule-time">
                                                                <span class="hour-class"><?php echo $entry['hour'] ?></span>
                                                                <?php foreach ($entry['minutes'] as $minute): ?>
                                                                    <span class="minute-class">
                                                                        <?php echo str_replace('*', '<span style="color:red">*</span>', $minute) ?>
                                                                    </span>
                                                                <?php endforeach; ?>
                                                            </p>
                                                        <?php endforeach; ?>
                                                    </div>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 p-0">
                                    <h4 class="sm-2-title"><?php echo esc_html__("Iš Smiltynės į Klaipėdą", "keltas-theme") ?></h4>
                                    <div class="col">
                                        <div class="row">
                                            <?php $rows = getTimeEntriesRows($groupedEntries, '18', '2') ?>
                                            <?php if (!empty($rows[0]) || !empty($rows[1])): ?>
                                                <?php foreach ($rows as $row): ?>
                                                    <div class="col-6 p-0">
                                                        <?php foreach ($row as $entry): ?>
                                                            <p class="schedule-time">
                                                                <span class="hour-class"><?php echo $entry['hour'] ?></span>
                                                                <?php foreach ($entry['minutes'] as $minute): ?>
                                                                    <span class="minute-class">
                                                                        <?php echo str_replace('*', '<span style="color:red">*</span>', $minute) ?>
                                                                    </span>
                                                                <?php endforeach; ?>
                                                            </p>
                                                        <?php endforeach; ?>
                                                    </div>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('.schedule-navigation-button').on('click', function (e) {
        var weekDay = $(this).attr("data-target");
        var today = new Date();
        var dateToLoad = null;

        for (var i = 0; i < 7; i++) {
            var addedDate = addDays(today, i);
            if (addedDate.getDay().toString() === weekDay) {
                dateToLoad = addedDate;
            }
        }

        if (dateToLoad === null) {
            return;
        }

        var formattedDate = dateToLoad.getFullYear() + '-' +
            ((dateToLoad.getMonth() + 1) < 10 ? '0' + (dateToLoad.getMonth() + 1) : (dateToLoad.getMonth() + 1)) +
            '-' + (dateToLoad.getDate() < 10 ? '0' + dateToLoad.getDate() : dateToLoad.getDate());

        window.location.replace(window.location.protocol + '//' + window.location.host + window.location.pathname + '?date=' + formattedDate);
    });


    function addDays(date, days) {
        var result = new Date(date);
        result.setDate(result.getDate() + days);
        return result;
    }
</script>
