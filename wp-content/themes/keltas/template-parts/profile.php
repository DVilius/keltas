<?php

/**
 * Template Name: profile
 */

get_header();

$background = 'background: url(' . get_the_post_thumbnail_url() . ')';
if (get_the_post_thumbnail_url() == null) {
    $background = 'background: url(' . get_the_post_thumbnail_url(get_option("page_for_posts")) . ')';
}

esc_html__('Registracijos patvirtinimas', 'keltas-theme');
esc_html__('Užbaikite registracija paspausdami čią.', 'keltas-theme');
esc_html__('(Paskyros patvirtinimas)', 'keltas-theme');
esc_html__('Pagarbiai,
AB „Smiltynės perkėla“

Tel.: 1870 arba +370 46 365018
El. paštas: info@keltas.lt
Pirm.-Sekm. 00:00 -24:00', 'keltas-theme');

$icon_important_info = '<img alt="" style="width: 20px;height: 20px;margin-right:5px;" src="' . get_template_directory_uri() . '/assets/images/icon_important_info.png' . '" />';

$user_name = '';
$user_surname = '';
$user_email = '';
$jurid_code = '';
$jurid_country = '';
$jurid_pvmcode = '';
$jurid_address = '';
$jurid = false;

$tickets = [];
$ticketsArchived = [];
$availableNationalities = [];

$isActivated = false;
if (isset($_GET['c'])) {
    $isActivated = Api::activateUser($_GET['c']);
}

$availableNationalities = Api::getNationalities();

if (is_logged_in()) {
    $response = json_decode(Api::getProfile($_SESSION['user_id']), true);
    if ($response['success']) {
        $ticketsResponse = Api::getTickets($_SESSION['user_token'], ICL_LANGUAGE_CODE, true);
        $ticketsArchived = json_decode($ticketsResponse, true)['archived'];
        $tickets = json_decode($ticketsResponse, true)['valid'];
        $user_name = $response['data']['sub_vardas'];
        $user_surname = $response['data']['sub_pavarde'];
        $user_email = $response['data']['sub_email'];
        $jurid_code = $response['data']['sub_akodas'];
        $jurid_pvmcode = $response['data']['sub_pvmkodas'];
        $jurid_address = $response['data']['sub_adr_miestas'];
        $jurid = $response['data']['sub_tipas'] === 3;

        foreach ($availableNationalities as $nationality) {
            if ($nationality['id'] === $response['data']['sub_adr_salis']) {
                $jurid_country = $nationality['name'];
                break;
            }
        }

        usort($tickets, static function ($a, $b) {
            return date('Y-m-d', strtotime($a['ticket_sell_date'])) < date('Y-m-d', strtotime($b['ticket_sell_date']));
        });

        usort($ticketsArchived, static function ($a, $b) {
            return date('Y-m-d', strtotime($a['ticket_sell_date'])) < date('Y-m-d', strtotime($b['ticket_sell_date']));
        });
    }
}

function extractTicketInfo($ticket)
{
    $info = $ticket['event']['custom_info'];

    switch (ICL_LANGUAGE_CODE) {
        case 'lt':
            return trim(substr($info['info1'], 3));
        case 'en':
            return trim(substr($info['info2'], 3));
        case 'ru':
            return trim(substr($info['info3'], 3));
        default:
            return ' ';
    }
}

function idToIcon($id)
{
    $icon_passenger = '<img alt="" style="width: 12px;" src="' . get_template_directory_uri() . '/assets/images/icon_passenger.png' . '" />';
    $icon_passenger_big_car = '<img alt="" style="width: 26px;" src="' . get_template_directory_uri() . '/assets/images/icon_passenger_big_car.png' . '" />';
    $icon_passenger_bike = '<img alt="" style="width: 29px;" src="' . get_template_directory_uri() . '/assets/images/icon_passenger_bike.png' . '" />';
    $icon_passenger_bus = '<img alt="" style="width: 40px;" src="' . get_template_directory_uri() . '/assets/images/icon_passenger_bus.png' . '" />';
    $icon_passenger_luggage = '<img alt="" style="width: 20px;" src="' . get_template_directory_uri() . '/assets/images/icon_passenger_luggage.png' . '" />';
    $icon_passenger_motorcycle = '<img alt="" style="width: 23px;" src="' . get_template_directory_uri() . '/assets/images/icon_passenger_motorcycle.png' . '" />';
    $icon_passenger_small_car = '<img alt="" style="width: 28px;" src="' . get_template_directory_uri() . '/assets/images/icon_passenger_small_car.png' . '" />';
    //    $icon_passenger_special_trip = '<img alt="" style="width: 20px;" src="' . get_template_directory_uri() . '/assets/images/icon_passenger_special_trip.png' . '" />';

    switch ($id) {
        case 156:
            return $icon_passenger_small_car;
        case 180:
        case 176:
            return $icon_passenger_bus;
        case 166:
            return $icon_passenger_motorcycle;
        case 208:
            return $icon_passenger_luggage;
        case 152:
            return $icon_passenger_bike;
        case 178:
            return $icon_passenger_big_car;
        default:
            return $icon_passenger;
    }
}

?>

    <style>

        .chk-container {
            text-align: left;
            display: block;
            position: relative;
            padding-left: 35px;
            margin-bottom: 25px;
            cursor: pointer;
            font-size: 22px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        .chk-container input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
            height: 0;
            width: 0;
        }

        .checkbox {
            border: 2px solid #099bc4;
            position: absolute;
            top: 0;
            left: 0;
            height: 25px;
            width: 25px;
        }

        .chk-container:hover input ~ .checkbox {
            background-color: #ccc;
        }

        .chk-container input:checked ~ .checkbox {
            background-color: #2196F3;
        }

        .checkbox:after {
            content: "";
            position: absolute;
            display: none;
        }

        .chk-container input:checked ~ .checkbox:after {
            display: block;
        }

        .inactive-tickets-group {
            outline: none;
            user-select: none;
        }

        .active-tickets-group {
            outline: none;
            user-select: none;
            display: none;
        }

        .archived-tickets-group {
            display: none;
        }

        .chk-container .checkbox:after {
            left: 7px;
            top: 1px;
            width: 7px;
            height: 16px;
            border: solid white;
            border-width: 0 3px 3px 0;
            -webkit-transform: rotate(45deg);
            -ms-transform: rotate(45deg);
            transform: rotate(45deg);
        }

        .chk-text {
            text-transform: none;
            color: #434f5b;
        }

        .button-defaults {
            font-family: "Neusa Bold", sans-serif;
            margin-top: 0;
            margin-bottom: 1rem;
            font-size: 18px;
            font-weight: 800;
        }

        .loader {
            display: none;
            width: 20px;
            height: 20px;
            border: 3px solid rgba(255, 255, 255, .3);
            border-radius: 50%;
            border-top-color: #fff;
            animation: spin 1s ease-in-out infinite;
            -webkit-animation: spin 1s ease-in-out infinite;
        }

        .ticket-inner {
            background-color: white;
            padding: 30px;
        }

        .ticket-outer {
            width: 270px;
            display: flex;
            flex-direction: column;
            margin: auto 10px 40px;
            cursor: pointer;
            transition: all .5s ease;
        }

        .ticket-outer:hover {
            box-shadow: 0 17px 38px 0 rgba(0, 0, 0, 0.1);
        }

        .ticket-title {
            padding-left: 10px;
            font-family: "Neusa Bold", sans-serif;
            line-height: normal;
            font-size: 17px;
            letter-spacing: 0.05em;
            text-transform: uppercase;
            color: #00518B;
            margin-bottom: 9px !important;
        }

        .ticket-hr {
            background-color: #E7F5FF;
        }

        .ticket-info {
            display: flex;
        }

        .ticket-info-title {
            color: #8EBCDD;
            font-size: 14px;
            font-family: 'Effra Light', sans-serif;
            padding-right: 5px;
        }

        .ticket-info-value {
            color: #00518B;
            font-size: 14px;
            font-family: 'Effra Light', sans-serif;
            font-weight: bold;
        }

        .tickets-background {
            background: #F5F5F5;
        }

        .error_messages {
            color: red;
            padding-left: 15px;
        }

        .success_messages {
            color: green;
            padding-left: 15px;
        }

        .validation-error {
            color: #ff0000;
            font-size: 15px;
        }

        .active-tab {
            background: #FFED00 !important;
            color: #00518B !important;
        }

        @keyframes spin {
            to {
                -webkit-transform: rotate(360deg);
            }
        }

        @-webkit-keyframes spin {
            to {
                -webkit-transform: rotate(360deg);
            }
        }

        .hover_bkgr_fricc {
            background: rgba(0, 0, 0, .4);
            cursor: pointer;
            display: none;
            height: 100%;
            position: fixed;
            text-align: center;
            top: 0;
            width: 100%;
            z-index: 10000;
        }

        .hover_bkgr_fricc .helper {
            display: inline-block;
            height: 100%;
            vertical-align: middle;
        }

        .hover_bkgr_fricc > div {
            background-color: #fff;
            box-shadow: 10px 10px 60px #555;
            display: inline-block;
            height: auto;
            max-width: 491px;
            min-height: 100px;
            vertical-align: middle;
            width: 60%;
            position: relative;
            border-radius: 8px;
            padding: 25px 4%;
        }

        .popupCloseButton {
            background-color: #fff;
            border: 3px solid #999;
            border-radius: 50px;
            cursor: pointer;
            display: inline-block;
            font-family: arial, serif;
            font-weight: bold;
            position: absolute;
            top: -20px;
            right: -20px;
            font-size: 25px;
            line-height: 26px;
            width: 30px;
            height: 30px;
            text-align: center;
        }

        .popupCloseButton:hover {
            background-color: #ccc;
        }

        .button-contact-us {
            width: unset;
        }

        .button-contact-us:focus {
            width: unset;
        }



        .acian-buttons {
            display: flex;
            justify-content: center;
            margin-bottom: 30px;
        }

        .acian-buttons {
            display: flex !important;
            justify-content: center !important;
            margin-bottom: 30px !important;
        }

        .acian-left {
            height: initial !important;
            padding: 10px 10px !important;
            margin-right: 10px !important;
            outline: none !important;
            width: unset !important;
            font-family: "Neusa Bold", sans-serif !important;
        }

        .acian-right {
            height: initial !important;
            padding: 10px 10px !important;
            margin-left: 10px !important;
            outline: none !important;
            width: unset !important;
            font-family: "Neusa Bold", sans-serif !important;
        }

        .acian-left > span {
            font-family: "Neusa Bold", sans-serif !important;
        }
        .acian-right > span {
            font-family: "Neusa Bold", sans-serif !important;
        }

        @media only screen and (max-width: 550px) {
            .acian-buttons {
                display: block !important;
            }

            .acian-right {
                width: 100% !important;
                margin-left: 0 !important;
            }

            .acian-left {
                width: 100% !important;
            }
        }

    </style>


    <div class="header-image" style="<?php echo $background ?>"></div>
    <div class="tickets-background">
    <div class="container inside-page" style="margin: 0 auto 100px; max-width: 1420px; padding: 0 !important;">

        <?php if (is_logged_in()): ?>

            <div class="row m-0" style="padding: 0 20px;">
                <div class="col-lg p-0">
                    <div class="row m-0 mb-4">
                        <h1 class="big-title"
                            style="margin: 25px auto auto;font-size: 50px;"><?php echo esc_html__('Jūsų bilietai', 'keltas-theme') ?></h1>
                    </div>

                    <div class="acian-buttons">
                        <button id="inactive_tab" class="button-contact-us active-tab acian-right">
                            <span class="button-defaults"
                                  style="margin-bottom: 0;padding: 5px;"><?php echo esc_html__('Neaktyvuoti', 'keltas-theme') ?></span>
                        </button>
                        <button id="active_tab" class="button-contact-us acian-right">
                            <span class="button-defaults"
                                  style="margin-bottom: 0;padding: 5px;"><?php echo esc_html__('Aktyvuoti', 'keltas-theme') ?></span>
                        </button>
                        <button id="inactive_tab_archive" class="button-contact-us acian-right">
                            <span class="button-defaults"
                                  style="margin-bottom: 0;padding: 5px;"><?php echo esc_html__('Archyvas', 'keltas-theme') ?></span>
                        </button>
                    </div>

                    <div class="row m-0" style="justify-content: center;">

                        <div class="archived-tickets-group" style="width: 100%;">
                            <div class="costs-table-container" style="border-bottom: none">
                                <div class="information-container table-container" style="overflow-x: auto;">
                                    <table style="border-collapse: collapse;    text-align: center;" border="1">
                                        <thead>
                                        <tr style="height: 24px;">
                                            <td style="height: 24px;"><span style="color: #00518b;"><strong><?php echo esc_html__('Bilietas', 'keltas-theme') ?></strong></span></td>
                                            <td style="height: 24px;"><span style="color: #00518b;"><strong><?php echo esc_html__('Kaina', 'keltas-theme') ?></strong></span></td>
                                            <td style="height: 24px;"><span style="color: #00518b;"><strong><?php echo esc_html__('Kodas', 'keltas-theme') ?></strong></span></td>
                                            <td style="height: 24px;"><span style="color: #00518b;"><strong><?php echo esc_html__('Pirkimo data', 'keltas-theme') ?></strong></span></td>
                                            <td style="height: 24px;"><span style="color: #00518b;"><strong><?php echo esc_html__('Aktyvuotas nuo', 'keltas-theme') ?></strong></span></td>
                                            <td style="height: 24px;"><span style="color: #00518b;"><strong><?php echo esc_html__('Galioja iki', 'keltas-theme') ?></strong></span></td>
                                            <td style="height: 24px;"><span style="color: #00518b;"><strong><?php echo esc_html__('Galiojimo laikas', 'keltas-theme') ?></strong></span></td>
                                            <td style="height: 24px;"><span style="color: #00518b;"><strong><?php echo esc_html__('Panaudotas', 'keltas-theme') ?></strong></span></td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($ticketsArchived as $ticket): ?>
                                            <?php
                                            $validDate = '---';
                                            if($ticket['is_activated']) {
                                                $d1 = date('Y-m-d', strtotime($ticket['show_date']));
                                                $d2 = date('Y-m-d', strtotime("$d1 +1 days"));
                                                $validDate = $d2;
                                            } else {
                                                $d1 = date('Y-m-d', strtotime($ticket['ticket_sell_date']));
                                                $d2 = date('Y-m-d', strtotime("$d1 +1 years"));
                                                $validDate = $d2;
                                            }
                                            ?>

                                            <tr style="height: 24px;">
                                                <td style="height: 24px;max-width: 300px;">
                                                    <?php echo idToIcon($ticket['ticket_id']) ?>
                                                    <span class="ticket-title" style="font-family: 'Effra Light', sans-serif;color: black;"><?php echo $ticket['sector'] ?></span>
                                                </td>
                                                <td style="height: 24px;"><span style="color: black;"><?php echo $ticket['ticket_price_archive'] ?: '---' ?> €</span></td>
                                                <td style="height: 24px;"><span style="color: black;"><?php echo $ticket['number'] ?></span></td>
                                                <td style="height: 24px;"><span style="color: black;"><?php echo $ticket['ticket_sell_date'] ?: '---' ?></span></td>
                                                <td style="height: 24px;"><span style="color: black;"><?php echo $ticket['is_activated'] ? date('Y-m-d', strtotime($ticket['show_date'])) ?: '---' : '---' ?></span></td>
                                                <td style="height: 24px;"><span style="color: black;"><?php echo $validDate ?></span></td>
                                                <td style="height: 24px;"><span style="color: black;">1 <?php echo $ticket['is_activated'] ? esc_html__('diena', 'keltas-theme') : esc_html__('metai', 'keltas-theme') ?></span></td>
                                                <td style="height: 24px;"><span style="color: black;"><?php echo $ticket['used_on'] ?: '---' ?></span></td>
                                            </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <?php foreach ($tickets as $ticket): ?>

                            <?php if ($ticket['is_activated']): ?>

                                <div class="active-tickets-group">

                                    <?php

                                    $validDate = '';

                                    if ($ticket['is_activated']) {
                                        $d1 = date('Y-m-d', strtotime($ticket['show_date']));
                                        $d2 = date('Y-m-d', strtotime("$d1 +1 days"));
                                        $validDate = $d2;
                                    } else {
                                        $d1 = date('Y-m-d', strtotime($ticket['ticket_sell_date']));
                                        $d2 = date('Y-m-d', strtotime("$d1 +1 years"));
                                        $validDate = $d2;
                                    }

                                    $isValid = $validDate > date('Y-m-d');

                                    if ($isValid): ?>

                                        <a href="<?php echo get_home_url() . '?api=printTicket&printTicket=' . base64_encode($ticket['id']) . '&locale=' . ICL_LANGUAGE_CODE ?>"
                                           class="button-contact-us"
                                           target="_blank"
                                           style="text-decoration: none;
                                        display: block;
                                        height: initial;
                                        border-radius: 10px 10px 0 0;
                                        width: 300px;
                                        margin: auto 10px 0;
                                        min-height: 50px;
                                        outline:none;
                                        ">
                                            <span class="button-defaults" style="margin-bottom: 0;height: 30px;">
                                                <span class="button-defaults" style="margin-bottom: 0">
                                                    <?php echo esc_html__('Spausdinti bilietą', 'keltas-theme') ?>
                                                </span>
                                            </span>
                                        </a>

                                    <?php endif; ?>

                                    <?php include 'ticket.php' ?>
                                </div>

                            <?php else: ?>

                                <div class="inactive-tickets-group">
                                    <button class="button-contact-us activate_ticket_btn"
                                            style="display: block;height: initial;width: 300px;margin: auto 10px 0;border-radius: 10px 10px 0 0;min-height: 50px;outline:none;">
                                            <span class="button-defaults" style="margin-bottom: 0;height: 30px;">
                                                <span class="is_activated" style="display: none">0</span>

                                                <span class="button-defaults activate_ticket_text"
                                                      style="margin-bottom: 0"><?php echo esc_html__('Aktyvuoti bilietą', 'keltas-theme') ?></span>
                                                <span class="button-defaults loader activate_ticket_loading"
                                                      style="margin-bottom: 0"></span>
                                            </span>

                                        <span class="ticket-id-hidden"
                                              style="display: none"><?php echo $ticket['id'] ?></span>
                                        <span class="ticket-is-activated-hidden"
                                              style="display: none"><?php echo $ticket['is_activated'] ? '1' : '0' ?></span>
                                    </button>

                                    <a href="<?php echo get_home_url() . '?api=printTicket&printTicket=' . base64_encode($ticket['id']) . '&locale=' . ICL_LANGUAGE_CODE ?>"
                                       class="button-contact-us print-ticket-temp"
                                       target="_blank"
                                       style="text-decoration: none;
                                        display: none;
                                        outline:none;
                                        height: initial;
                                        width: 300px;
                                        margin: auto 10px 0;
                                        min-height: 50px;
                                        ">
                                            <span class="button-defaults" style="margin-bottom: 0;height: 30px;">
                                                <span class="button-defaults" style="margin-bottom: 0">
                                                    <?php echo esc_html__('Spausdinti bilietą', 'keltas-theme') ?>
                                                </span>
                                            </span>
                                    </a>
                                    <?php include 'ticket.php' ?>
                                </div>

                            <?php endif; ?>

                        <?php endforeach; ?>
                    </div>

                </div>
            </div>

            <div class="row m-0" style="padding: 0 20px;">
                <div class="col-lg p-0">
                    <div class="row m-0 mb-4">
                        <h1 class="big-title"
                            style="margin: 25px auto auto;font-size: 50px;"><?php echo esc_html__('Profilis', 'keltas-theme') ?></h1>
                    </div>

                    <div>
                        <div style="display: flex">
                            <p class="contact-title"
                               style="padding-right: 30px;"><?php echo $jurid ? esc_html__('Teisinė forma', 'keltas-theme') : esc_html__('Vardas', 'keltas-theme') ?>: </p>
                            <p style="line-height: normal;"><?php echo $user_name ?></p>
                        </div>

                        <div style="display: flex">
                            <p class="contact-title"
                               style="padding-right: 30px;"><?php echo $jurid ? esc_html__('Įmonės pavadinimas', 'keltas-theme') : esc_html__('Pavardė', 'keltas-theme') ?>: </p>
                            <p style="line-height: normal;"><?php echo $user_surname ?></p>
                        </div>

                        <div style="display: flex">
                            <p class="contact-title"
                               style="padding-right: 30px;"><?php echo esc_html__('El. paštas', 'keltas-theme') ?>: </p>
                            <p style="line-height: normal;"><?php echo $user_email ?></p>
                        </div>

                        <?php if($jurid): ?>
                            <div style="display: flex">
                                <p class="contact-title"
                                   style="padding-right: 30px;"><?php echo esc_html__('Įmonės kodas', 'keltas-theme') ?>: </p>

                                <div id="static_code_holder" style="display: flex;">
                                    <p style="line-height: normal;" id="static_code"><?php echo $jurid_code ?></p>
                                    <a onclick="$('#static_code_holder').css('display', 'none');$('#editable_code_holder').css('display', 'flex')" href="#a" style="padding-left: 10px;transform: translateY(-2px);"><?php echo esc_html__('Redaguoti', 'keltas-theme') ?></a>
                                </div>
                                <div id="editable_code_holder" style="display: none;">
                                    <input id="editable_code" value="<?php echo $jurid_code ?>" type="text" style="padding: 0px 10px;transform: translateY(-5px); border: 1px solid #C2DEF2 !important;box-sizing: border-box;border-radius: 3px !important;">
                                    <a onclick="saveJuridCode()" href="#a" style="padding-left: 10px;transform: translateY(-2px);"><?php echo esc_html__('Išsaugoti', 'keltas-theme') ?></a>
                                    <a onclick="$('#editable_code_holder').css('display', 'none');$('#static_code_holder').css('display', 'flex')" href="#a" style="padding-left: 10px;transform: translateY(-2px);"><?php echo esc_html__('Atšaukti', 'keltas-theme') ?></a>
                                </div>
                            </div>

                            <div style="display: flex">
                                <p class="contact-title"
                                   style="padding-right: 30px;"><?php echo esc_html__('PVM kodas', 'keltas-theme') ?>: </p>

                                <div id="static_pvm_holder" style="display: flex;">
                                    <p style="line-height: normal;" id="static_pvm_code"><?php echo $jurid_pvmcode ?></p>
                                    <a onclick="$('#static_pvm_holder').css('display', 'none');$('#editable_pvm_holder').css('display', 'flex')" href="#a" style="padding-left: 10px;transform: translateY(-2px);"><?php echo esc_html__('Redaguoti', 'keltas-theme') ?></a>
                                </div>
                                <div id="editable_pvm_holder" style="display: none;">
                                    <input id="editable_pvm_code" value="<?php echo $jurid_pvmcode ?>" type="text" style="padding: 0px 10px;transform: translateY(-5px); border: 1px solid #C2DEF2 !important;box-sizing: border-box;border-radius: 3px !important;">
                                    <a onclick="saveJuridPVMCode()" href="#a" style="padding-left: 10px;transform: translateY(-2px);"><?php echo esc_html__('Išsaugoti', 'keltas-theme') ?></a>
                                    <a onclick="$('#editable_pvm_holder').css('display', 'none');$('#static_pvm_holder').css('display', 'flex')" href="#a" style="padding-left: 10px;transform: translateY(-2px);"><?php echo esc_html__('Atšaukti', 'keltas-theme') ?></a>
                                </div>
                            </div>

                            <div style="display: flex">
                                <p class="contact-title"
                                   style="padding-right: 30px;"><?php echo esc_html__('Šalis', 'keltas-theme') ?>: </p>

                                <div id="static_country_holder" style="display: flex;">
                                    <p style="line-height: normal;" id="static_country_code"><?php echo $jurid_country ?></p>
                                    <a onclick="$('#static_country_holder').css('display', 'none');$('#editable_country_holder').css('display', 'flex')" href="#a" style="padding-left: 10px;transform: translateY(-2px);"><?php echo esc_html__('Redaguoti', 'keltas-theme') ?></a>
                                </div>
                                <div id="editable_country_holder" style="display: none;padding-bottom: 20px;">
                                    <select id="editable_country" style="width: 100%;padding: 10px;border: 1px solid #C2DEF2 !important;">
                                        <?php foreach($availableNationalities as $nationality): ?>
                                            <option value="<?php echo $nationality['code'] ?>"><?php echo $nationality['name'] ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <a onclick="saveJuridCountry()" href="#a" style="padding-left: 10px;transform: translateY(10px);"><?php echo esc_html__('Išsaugoti', 'keltas-theme') ?></a>
                                    <a onclick="$('#editable_country_holder').css('display', 'none');$('#static_country_holder').css('display', 'flex')" href="#a" style="padding-left: 10px;transform: translateY(10px);"><?php echo esc_html__('Atšaukti', 'keltas-theme') ?></a>
                                </div>
                            </div>

                            <div style="display: flex">
                                <p class="contact-title"
                                   style="padding-right: 30px;"><?php echo esc_html__('Adresas', 'keltas-theme') ?>: </p>

                                <div id="static_address_holder" style="display: flex;">
                                    <p style="line-height: normal;" id="static_address_code"><?php echo $jurid_address ?></p>
                                    <a onclick="$('#static_address_holder').css('display', 'none');$('#editable_address_holder').css('display', 'flex')" href="#a" style="padding-left: 10px;transform: translateY(-2px);"><?php echo esc_html__('Redaguoti', 'keltas-theme') ?></a>
                                </div>
                                <div id="editable_address_holder" style="display: none;">
                                    <input id="editable_address" value="<?php echo $jurid_address ?>" type="text" style="padding: 0px 10px;transform: translateY(-5px); border: 1px solid #C2DEF2 !important;box-sizing: border-box;border-radius: 3px !important;">
                                    <a onclick="saveJuridAddress()" href="#a" style="padding-left: 10px;transform: translateY(-2px);"><?php echo esc_html__('Išsaugoti', 'keltas-theme') ?></a>
                                    <a onclick="$('#editable_address_holder').css('display', 'none');$('#static_address_holder').css('display', 'flex')" href="#a" style="padding-left: 10px;transform: translateY(-2px);"><?php echo esc_html__('Atšaukti', 'keltas-theme') ?></a>
                                </div>
                            </div>

                        <?php endif;?>

                        <script>

                            function saveJuridCountry() {
                                var code = $('#editable_country').val();
                                var text = $('#editable_country option:selected').text();

                                $('#static_country_code').html(text);
                                $('#editable_country_holder').css('display', 'none');
                                $('#static_country_holder').css('display', 'flex');

                                jQuery.ajax({
                                    type: "post",
                                    dataType: "json",
                                    url: window.location.protocol + '//' + window.location.host,
                                    data: {
                                        api: "updateUserCountry",
                                        code: code
                                    },
                                })
                            }

                            function saveJuridCode() {
                                var code = $('#editable_code').val();

                                $('#static_code').html(code);
                                $('#editable_code_holder').css('display', 'none');
                                $('#static_code_holder').css('display', 'flex')

                                jQuery.ajax({
                                    type: "post",
                                    dataType: "json",
                                    url: window.location.protocol + '//' + window.location.host,
                                    data: {
                                        api: "updateUserCode",
                                        code: code
                                    },
                                })
                            }

                            function saveJuridPVMCode() {
                                var code = $('#editable_pvm_code').val();

                                $('#static_pvm_code').html(code);
                                $('#editable_pvm_holder').css('display', 'none');
                                $('#static_pvm_holder').css('display', 'flex')

                                jQuery.ajax({
                                    type: "post",
                                    dataType: "json",
                                    url: window.location.protocol + '//' + window.location.host,
                                    data: {
                                        api: "updateUserPVMCode",
                                        code: code
                                    },
                                })
                            }

                            function saveJuridAddress() {
                                var address = $('#editable_address').val();

                                $('#static_address_code').html(address);
                                $('#editable_address_holder').css('display', 'none');
                                $('#static_address_holder').css('display', 'flex')

                                jQuery.ajax({
                                    type: "post",
                                    dataType: "json",
                                    url: window.location.protocol + '//' + window.location.host,
                                    data: {
                                        api: "updateUserAddress",
                                        address: address,
                                    },
                                })
                            }

                        </script>
                    </div>

                    <div style="margin-top: 60px;">
                        <a class="see-more-link" style="font-size: 30px;"
                           href="<?php echo esc_html__('/bilietu-pirkimas', 'keltas-theme') ?>">
                            <?php echo esc_html__('Pirkti bilietus ', 'keltas-theme'); ?>
                            <i style="color: #288CCD;" class="fas fa-long-arrow-alt-right"></i>
                        </a>
                    </div>

                </div>
                <div class="col-lg p-0">
                    <div class="row m-0 mb-4">
                        <h1 class="big-title"
                            style="margin: 25px auto auto;font-size: 50px;"><?php echo esc_html__('Slaptažodžio keitimas', 'keltas-theme') ?></h1>
                    </div>

                    <div id="pass_change_form" style="max-width: 450px;margin: auto;">
                        <div class="information-container table-container" style="border: 1px solid transparent">
                            <div class="form-background" style="padding: 0">
                                <label> <?php echo esc_html__('Senas slaptažodis', 'keltas-theme') ?><br>
                                    <input type="password" id="old_pass" value="" size="40">
                                </label>
                                <div class="validation-error"></div>

                                <label> <?php echo esc_html__('Naujas slaptažodis', 'keltas-theme') ?><br>
                                    <input type="password" id="new_pass" value="" size="40">
                                </label>
                                <div class="validation-error"></div>

                                <div style="display: flex;">
                                    <button class="button-contact-us" id="submit_change_pass"
                                            style="height: initial; padding: 10px 10px;">
                                            <span class="button-defaults" style="margin-bottom: 0;height: 30px;">
                                                <span class="button-defaults" id="change_pass_text"
                                                      style="margin-bottom: 0"><?php echo esc_html__('Keisti', 'keltas-theme')
                                                    ?></span>
                                                <span class="button-defaults loader" id="change_pass_loading"
                                                      style="margin-bottom: 0"></span>
                                            </span>
                                    </button>
                                </div>

                                <div class="error_messages" id="error_message_pass_change"></div>
                                <div class="success_messages" id="success_message_pass_change"></div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        <?php else: ?>

            <div id="login_form" style="max-width: 450px;margin: auto;">
                <h1 class="big-title"
                    style="margin: 25px 40px auto;font-size: 50px;"><?php echo esc_html__('Prisijungti', 'keltas-theme') ?></h1>

                <?php if ($isActivated): ?>

                    <span style="color: #00518B;margin: 25px 40px auto;font-size: 24px;font-weight: bold;display: block;">
                        <?php echo esc_html__('Jūsų paskyra aktyvuota, galite prisijungti.', 'keltas-theme') ?>
                    </span>

                <?php endif; ?>

                <div class="information-container table-container" style="border: 1px solid transparent">
                    <div class="form-background">
                        <div class="col-lg">
                            <label> <?php echo esc_html__('El. paštas', 'keltas-theme') ?><br>
                                <input type="email" id="login_email" size="40">
                            </label>
                            <div class="validation-error"></div>

                            <label> <?php echo esc_html__('Slaptažodis', 'keltas-theme') ?><br>
                                <input type="password" id="login_password" size="40">
                            </label>
                            <div class="validation-error"></div>
                        </div>

                        <div style="display: flex;padding-left: 15px;">
                            <button class="button-contact-us" id="submit_login"
                                    style="height: initial; padding: 10px 10px;">
                                    <span class="button-defaults" style="margin-bottom: 0;height: 30px;">
                                        <span class="button-defaults" id="login_text"
                                              style="margin-bottom: 0"><?php echo esc_html__('Prisijungti', 'keltas-theme') ?></span>
                                        <span class="button-defaults loader" id="login_loading"
                                              style="margin-bottom: 0"></span>
                                    </span>
                            </button>

                            <div style="margin-bottom: 0;margin-left: 10px;">
                                <p class="port-address" style="margin: auto;"><a id="register_form_button" href="#"
                                                                                 style="font-size: 20px;"><?php echo esc_html__('Registruotis', 'keltas-theme') ?></a>
                                </p>
                                <p class="port-address" style="margin: auto;"><a id="remind_form_button" href="#"
                                                                                 style="font-size: 20px;"><?php echo esc_html__('Priminti slaptažodį', 'keltas-theme') ?></a>
                                </p>
                            </div>

                        </div>

                        <div class="error_messages" id="error_message"></div>
                    </div>
                </div>
            </div>

            <div id="remind_form" style="max-width: 450px;margin: auto;display: none">
                <h1 class="big-title"
                    style="margin: 25px 40px auto;font-size: 50px;"><?php echo esc_html__('Priminti slaptažodį', 'keltas-theme') ?></h1>
                <div class="information-container table-container" style="border: 1px solid transparent">
                    <div class="form-background">
                        <div class="col-lg">
                            <label> <?php echo esc_html__('El. paštas', 'keltas-theme') ?><br>
                                <input type="email" id="remind_email" value="" size="40">
                            </label>
                            <div class="validation-error"></div>
                        </div>

                        <div style="display: flex;padding-left: 15px;">
                            <button class="button-contact-us" id="submit_remind"
                                    style="height: initial; padding: 10px 10px;">
                                    <span class="button-defaults" style="margin-bottom: 0;height: 30px;">
                                        <span class="button-defaults" id="remind_text"
                                              style="margin-bottom: 0"><?php echo esc_html__('Siųsti priminimą', 'keltas-theme') ?></span>
                                        <span class="button-defaults loader" id="remind_loading"
                                              style="margin-bottom: 0"></span>
                                    </span>
                            </button>

                            <div style="margin-bottom: 0;margin-left: 10px;">
                                <p class="port-address" style="margin: auto;"><a id="register_form_button_remind"
                                                                                 href="#"
                                                                                 style="font-size: 20px;"><?php echo esc_html__('Registruotis', 'keltas-theme') ?></a>
                                </p>
                                <p class="port-address" style="margin: auto;"><a id="login_form_button_remind" href="#"
                                                                                 style="font-size: 20px;"><?php echo esc_html__('Prisijungti', 'keltas-theme') ?></a>
                                </p>
                            </div>

                        </div>

                        <div class="error_messages" id="error_message_pass_remind"></div>
                        <div class="success_messages" id="success_message_pass_remind"></div>
                    </div>
                </div>
            </div>

            <div id="register_form" style="max-width: 450px;margin: auto;display: none">
                <h1 class="big-title"
                    style="margin: 25px 40px auto;font-size: 50px;"><?php echo esc_html__('Registracija', 'keltas-theme') ?></h1>
                <div class="information-container table-container" style="border: 1px solid transparent">
                    <div class="form-background">
                        <div class="col-lg">

                            <label class="chk-container"><span class="chk-text">
                                    <?php echo esc_html__('Registruotis kaip juridinis asmuo', 'keltas-theme') ?>
                                </span>
                                <input type="checkbox" name="jurid" class="jurid-check">
                                <span class="checkbox"></span>
                            </label>

                            <label> <?php echo esc_html__('Vardas', 'keltas-theme') ?><br>
                                <input type="text" id="register_name" value="" size="40">
                            </label>
                            <div class="validation-error"></div>

                            <label> <?php echo esc_html__('Pavardė', 'keltas-theme') ?><br>
                                <input type="text" id="register_surname" value="" size="40">
                            </label>
                            <div class="validation-error"></div>

                            <label> <?php echo esc_html__('El. paštas', 'keltas-theme') ?><br>
                                <input type="email" id="register_email" value="" size="40">
                            </label>
                            <div class="validation-error"></div>

                            <label> <?php echo esc_html__('Slaptažodis', 'keltas-theme') ?><br>
                                <input type="password" id="register_password" value="" size="40">
                            </label>
                            <div class="validation-error"></div>

                            <label> <?php echo esc_html__('Pakartokite slaptažodį', 'keltas-theme') ?><br>
                                <input type="password" id="register_repassword" value="" size="40">
                            </label>
                            <div class="validation-error"></div>

                            <label class="chk-container"><span class="chk-text">
                                    <?php echo esc_html__('Sutinku su ', 'keltas-theme') ?>
                                    <a href="https://www.keltas.lt/apie-mus/bilietu-pirkimo-internetu-taisykles/"
                                       target="_blank">
                                        <?php echo esc_html__('taisyklėmis.', 'keltas-theme') ?>
                                    </a>
                                </span>
                                <div class="validation-error" id="chk_rules"></div>
                                <input type="checkbox" id="accept_rules">
                                <span class="checkbox"></span>
                            </label>

                            <label class="chk-container"><span class="chk-text">
                                    <?php echo esc_html__('Sutinku su ', 'keltas-theme') ?>
                                    <a href="https://www.keltas.lt/apie-mus/privatumo-politika/" target="_blank">
                                        <?php echo esc_html__('privatumo politika.', 'keltas-theme') ?>
                                    </a>
                                </span>
                                <div class="validation-error" id="chk_privacy"></div>
                                <input name="accept_privacy" type="checkbox" id="accept_privacy">
                                <span class="checkbox"></span>
                            </label>

                        </div>

                        <div style="display: flex;padding-left: 15px;">
                            <button class="button-contact-us" id="submit_register"
                                    style="height: initial; padding: 10px 10px;">
                                    <span class="button-defaults" style="margin-bottom: 0;height: 30px;">
                                        <span class="button-defaults" id="register_text"
                                              style="margin-bottom: 0"><?php echo esc_html__('Registruotis', 'keltas-theme') ?></span>
                                        <span class="button-defaults loader" id="register_loading"
                                              style="margin-bottom: 0"></span>
                                    </span>
                            </button>

                            <p class="port-address" style="margin: auto;"><a id="login_form_button" href="#"
                                                                             style="font-size: 20px;padding: 0 10px;"><?php echo esc_html__('Prisijungti', 'keltas-theme') ?></a>
                            </p>
                        </div>

                        <div class="error_messages" id="error_message_register"></div>
                    </div>
                </div>
            </div>

            <div id="register_form_jurid" style="max-width: 450px;margin: auto;display: none">
                <h1 class="big-title"
                    style="margin: 25px 40px auto;font-size: 50px;"><?php echo esc_html__('Registracija', 'keltas-theme') ?></h1>
                <div class="information-container table-container" style="border: 1px solid transparent">
                    <div class="form-background">
                        <div class="col-lg">

                            <label class="chk-container"><span class="chk-text">
                                    <?php echo esc_html__('Registruotis kaip juridinis asmuo', 'keltas-theme') ?>
                                </span>
                                <input type="checkbox" name="jurid" class="jurid-check">
                                <span class="checkbox"></span>
                            </label>

                            <label> <?php echo esc_html__('Įmonės pavadinimas', 'keltas-theme') ?><br>
                                <input type="text" id="jurid_register_title" value="" size="40">
                            </label>
                            <div class="validation-error"></div>

                            <label> <?php echo esc_html__('Teisinė forma', 'keltas-theme') ?><br>
                                <select name="" id="jurid_register_teisine" style="width: 100%;padding: 10px;border: 1px solid #C2DEF2 !important;">
                                    <option value="UAB"><?php echo esc_html__('UAB', 'keltas-theme') ?></option>
                                    <option value="UAB"><?php echo esc_html__('AB', 'keltas-theme') ?></option>
                                    <option value="UAB"><?php echo esc_html__('VšĮ', 'keltas-theme') ?></option>
                                    <option value="UAB"><?php echo esc_html__('IĮ', 'keltas-theme') ?></option>
                                    <option value="UAB"><?php echo esc_html__('UADBB', 'keltas-theme') ?></option>
                                    <option value="UAB"><?php echo esc_html__('MB', 'keltas-theme') ?></option>
                                    <option value="UAB"><?php echo esc_html__('VĮ', 'keltas-theme') ?></option>
                                    <option value="UAB"><?php echo esc_html__('TŪB', 'keltas-theme') ?></option>
                                    <option value="UAB"><?php echo esc_html__('KŪB', 'keltas-theme') ?></option>
                                    <option value="UAB"><?php echo esc_html__('ŽŪB', 'keltas-theme') ?></option>
                                    <option value="UAB"><?php echo esc_html__('BĮ', 'keltas-theme') ?></option>
                                    <option value="UAB"><?php echo esc_html__('N/A', 'keltas-theme') ?></option>
                                </select>
                            </label>
                            <div class="validation-error"></div>

                            <label> <?php echo esc_html__('El. paštas', 'keltas-theme') ?><br>
                                <input type="email" id="jurid_register_email" value="" size="40">
                            </label>
                            <div class="validation-error"></div>

                            <label> <?php echo esc_html__('Įmonės kodas', 'keltas-theme') ?><br>
                                <input type="text" id="jurid_register_code" value="" size="40">
                            </label>
                            <div class="validation-error"></div>

                            <label> <?php echo esc_html__('Įmonės PVM kodas', 'keltas-theme') ?><br>
                                <input type="text" id="jurid_register_pvm_code" value="" size="40">
                            </label>
                            <div class="validation-error"></div>

                            <label> <?php echo esc_html__('Šalis', 'keltas-theme') ?><br>
                                <select name="" id="jurid_register_nationality" style="width: 100%;padding: 10px;border: 1px solid #C2DEF2 !important;">
                                    <?php foreach($availableNationalities as $nationality): ?>

                                        <option value="<?php echo $nationality['code'] ?>"><?php echo $nationality['name'] ?></option>

                                    <?php endforeach; ?>
                                </select>
                            </label>
                            <div class="validation-error"></div>

                            <label> <?php echo esc_html__('Adresas', 'keltas-theme') ?><br>
                                <input type="text" id="jurid_address" value="" size="40">
                            </label>
                            <div class="validation-error"></div>

                            <label> <?php echo esc_html__('Slaptažodis', 'keltas-theme') ?><br>
                                <input type="password" id="jurid_register_password" value="" size="40">
                            </label>
                            <div class="validation-error"></div>

                            <label> <?php echo esc_html__('Pakartokite slaptažodį', 'keltas-theme') ?><br>
                                <input type="password" id="jurid_register_repassword" value="" size="40">
                            </label>
                            <div class="validation-error"></div>

                            <label class="chk-container"><span class="chk-text">
                                    <?php echo esc_html__('Sutinku su ', 'keltas-theme') ?>
                                    <a href="https://www.keltas.lt/apie-mus/bilietu-pirkimo-internetu-taisykles/"
                                       target="_blank">
                                        <?php echo esc_html__('taisyklėmis.', 'keltas-theme') ?>
                                    </a>
                                </span>
                                <div class="validation-error" id="jurid_chk_rules"></div>
                                <input type="checkbox" id="jurid_accept_rules">
                                <span class="checkbox"></span>
                            </label>

                            <label class="chk-container"><span class="chk-text">
                                    <?php echo esc_html__('Sutinku su ', 'keltas-theme') ?>
                                    <a href="https://www.keltas.lt/apie-mus/privatumo-politika/" target="_blank">
                                        <?php echo esc_html__('privatumo politika.', 'keltas-theme') ?>
                                    </a>
                                </span>
                                <div class="validation-error" id="jurid_chk_privacy"></div>
                                <input name="accept_privacy" type="checkbox" id="jurid_accept_privacy">
                                <span class="checkbox"></span>
                            </label>

                        </div>

                        <div style="display: flex;padding-left: 15px;">
                            <button class="button-contact-us" id="jurid_submit_register"
                                    style="height: initial; padding: 10px 10px;">
                                    <span class="button-defaults" style="margin-bottom: 0;height: 30px;">
                                        <span class="button-defaults" id="jurid_register_text"
                                              style="margin-bottom: 0"><?php echo esc_html__('Registruotis', 'keltas-theme') ?></span>
                                        <span class="button-defaults loader" id="jurid_register_loading"
                                              style="margin-bottom: 0"></span>
                                    </span>
                            </button>

                            <p class="port-address" style="margin: auto;"><a id="login_form_button_jurid" href="#"
                                                                             style="font-size: 20px;padding: 0 10px;"><?php echo esc_html__('Prisijungti', 'keltas-theme') ?></a>
                            </p>
                        </div>

                        <div class="error_messages" id="jurid_error_message_register"></div>
                    </div>
                </div>
            </div>

        <?php endif; ?>

    </div>


    <div style="width: 100%;
            height: 100%;
            position: fixed;
            background: rgba(255,255,255,1);top: 0;
            z-index: 99999;
            display: none;
            justify-content: center;
            align-items: center;
            cursor: pointer;" id="popup_backdrop">

        <div>
            <div id="popup_img" style="margin: auto;width: 50%;user-select: none;"></div>
        </div>

    </div>

    <div class="hover_bkgr_fricc">
        <span class="helper"></span>
        <div>
            <div class="popupCloseButton">&times;</div>
            <div id="expiry_calc_date"
                 style="text-align: left;"><?php echo esc_html__('BILIETAS GALIOS IKI :date D. 24:00 VAL.', 'keltas-theme') ?></div>
            <div style="text-align: left;"><?php echo esc_html__('AR TIKRAI NORITE AKTYVUOTI BILIETĄ?', 'keltas-theme') ?></div>
            <div style="display: flex;padding-top: 20px;flex-wrap: wrap;">
                <button id="deny_btn" class="button-contact-us"
                        style="min-width: 150px;height: initial; padding: 10px 10px;outline:none;margin-top: 10px;width: unset; margin-right: 20px;">
                    <span class="button-defaults"
                          style="margin-bottom: 0;padding: 5px;"><?php echo esc_html__('NE', 'keltas-theme') ?></span>
                </button>
                <button id="accept_btn" class="button-contact-us"
                        style="min-width: 150px;height: initial; padding: 10px 10px;outline:none;margin-top: 10px;width: unset">
                    <span class="button-defaults"
                          style="margin-bottom: 0;padding: 5px;"><?php echo esc_html__('TAIP', 'keltas-theme') ?></span>
                </button>
            </div>
        </div>
    </div>

    <script>
        var is_loading_login = false;
        var is_loading_register = false;
        var is_loading_register_jurid = false;
        var is_loading_pass_change = false;
        var is_loading_pass_remind = false;
        var is_activation_loading = false;
        var selected_ticket_activated = false;
        var token = '<?php echo isset($_SESSION['user_token']) ? $_SESSION['user_token'] : 'null' ?>';
        var was_ticket_activated_recently = false;

        var current_pressed_btn = null;

        var jurid_checked = false;

        $('.jurid-check').on('change', function(e) {
            if (!jurid_checked) {
                $("#register_form").css('display', 'none');
                $("#register_form_jurid").css('display', 'block');
                jurid_checked = true;
            } else {
                $("#register_form_jurid").css('display', 'none');
                $("#register_form").css('display', 'block');
                jurid_checked = false;
            }

            $('.jurid-check').prop('checked', jurid_checked);
        })

        $('.activate_ticket_btn').on('click', function (e) {
            current_pressed_btn = $(this);

            if (is_activation_loading || current_pressed_btn.find('.is_activated').text() === '1') {
                return;
            }

            var sd = new Date();
            var month = (sd.getMonth() + 1) < 10 ? '0' + (sd.getMonth() + 1) : (sd.getMonth() + 1);
            var day = sd.getDate() < 10 ? '0' + (sd.getDate() + 1) : (sd.getDate() + 1);
            var mdstrDate = sd.getFullYear() + "-" + month + "-" + day;
            $('#expiry_calc_date').html($('#expiry_calc_date').html().replace(':date', mdstrDate));
            $('.hover_bkgr_fricc').show();
        });

        $('#accept_btn').on('click', function (e) {
            if (!current_pressed_btn) {
                return;
            }

            tryActivateTicket();
        });

        $('.hover_bkgr_fricc').click(function () {
            $('.hover_bkgr_fricc').hide();
        });

        $('.popupCloseButton').click(function () {
            $('.hover_bkgr_fricc').hide();
        });

        function tryActivateTicket() {
            var btn = current_pressed_btn;
            var is_activated = btn.find('.is_activated');
            var selected_ticket_id = btn.find('.ticket-id-hidden').text();
            var selected_ticket_text = btn.parent().find('.activate_ticket_text');
            var ticket_print = btn.parent().find('.print-ticket-temp');
            var selected_ticket_activation_value = btn.parent().find('.ticket-activity-txt');
            var selected_ticket_validity_value = btn.parent().find('.ticket-validity-text');
            var selected_ticket_loading = btn.find('.activate_ticket_loading');

            if (is_activation_loading || is_activated.text() === '1') {
                return;
            }

            toggleActivationLoading(selected_ticket_text, selected_ticket_loading);

            jQuery.ajax({
                type: "post",
                dataType: "json",
                url: window.location.protocol + '//' + window.location.host,
                data: {
                    api: "activateTicket",
                    token: token,
                    ticket: selected_ticket_id,
                },
                success: function (response) {
                    if (response['success'] === true) {
                        toggleActivationLoading(selected_ticket_text, selected_ticket_loading);
                        btn.css('background', '#11a710');
                        selected_ticket_activation_value.css('color', '#11a710');
                        selected_ticket_validity_value.html(1 + ' ' + '<?php echo esc_html__('diena', 'keltas-theme') ?>');
                        selected_ticket_activation_value.html('<?php echo esc_html__('Bilietas aktyvuotas', 'keltas-theme') ?>');
                        selected_ticket_text.html('<?php echo esc_html__('Bilietas aktyvuotas', 'keltas-theme') ?>');
                        is_activated.text('1');
                        ticket_print.css('display', 'block');
                    } else {
                        toggleActivationLoading(selected_ticket_text, selected_ticket_loading);
                        console.log(response);
                        alert('<?php echo esc_html__('Įvyko klaida aktyvuojant bilietą. Bandykite dar karta arba kreipkitės į administraciją.', 'keltas-theme') ?>');
                    }
                },
                error: function () {
                    toggleActivationLoading(selected_ticket_text, selected_ticket_loading);
                    alert('<?php echo esc_html__('Įvyko klaida aktyvuojant bilietą. Bandykite dar karta arba kreipkitės į administraciją.', 'keltas-theme') ?>');
                }
            })
        }

        $('#inactive_tab').on('click', function (e) {
            e.preventDefault();

            $('#inactive_tab').addClass('active-tab');
            $('#active_tab').removeClass('active-tab');
            $('#inactive_tab_archive').removeClass('active-tab');

            $('.inactive-tickets-group').css('display', 'block');
            $('.active-tickets-group').css('display', 'none');
            $('.archived-tickets-group').css('display', 'none');
        });

        $('#inactive_tab_archive').on('click', function (e) {
            e.preventDefault();

            $('#inactive_tab_archive').addClass('active-tab');
            $('#active_tab').removeClass('active-tab');
            $('#inactive_tab').removeClass('active-tab');

            $('.inactive-tickets-group').css('display', 'none');
            $('.active-tickets-group').css('display', 'none');
            $('.archived-tickets-group').css('display', 'block');
        });

        $('#active_tab').on('click', function (e) {
            e.preventDefault();

            $('#active_tab').addClass('active-tab');
            $('#inactive_tab').removeClass('active-tab');
            $('#inactive_tab_archive').removeClass('active-tab');

            $('.inactive-tickets-group').css('display', 'none');
            $('.active-tickets-group').css('display', 'block');
            $('.archived-tickets-group').css('display', 'none');
        });

        $('#submit_change_pass').on('click', function (e) {
            e.preventDefault();

            if (is_loading_pass_change) {
                return;
            }

            var oldPass = $("#old_pass").val();
            var newPass = $("#new_pass").val();

            if (!oldPass || !newPass) {
                setErrorPassChange('<?php echo esc_html__('Visi laukai privalo būti užpildyti.', 'keltas-theme') ?>');
                return;
            }

            setErrorPassChange();
            setSuccessPassChange();
            togglePassChangeLoading();

            jQuery.ajax({
                type: "post",
                dataType: "json",
                url: window.location.protocol + '//' + window.location.host,
                data: {
                    api: "changePassword",
                    session_token: token,
                    old_pass: oldPass,
                    new_pass: newPass,
                },
                success: function (response) {
                    if (response['success'] === true) {
                        setSuccessPassChange('<?php echo esc_html__('Slaptažodis pakeistas sėkmingai.', 'keltas-theme') ?>');
                        togglePassChangeLoading();
                    } else {
                        setErrorPassChange('<?php echo esc_html__('Senasis slaptažodis neatitinka.', 'keltas-theme') ?>');
                        togglePassChangeLoading();
                    }
                },
                error: function () {
                    setErrorPassChange('<?php echo esc_html__('Įvyko klaida. Bandykite vėliau arba kreipkitės į administraciją.', 'keltas-theme') ?>');
                    togglePassChangeLoading();
                }
            })
        });

        $('#submit_remind').on('click', function (e) {
            e.preventDefault();

            if (is_loading_pass_remind) {
                return;
            }

            var email = $("#remind_email").val();

            if (!email) {
                setErrorPassRemind('<?php echo esc_html__('Visi laukai privalo būti užpildyti.', 'keltas-theme') ?>');
                return;
            }

            setErrorPassRemind();
            togglePassRemindLoading();

            jQuery.ajax({
                type: "post",
                dataType: "json",
                url: window.location.protocol + '//' + window.location.host,
                data: {
                    api: "remindPassword",
                    email: email,
                },
                success: function (response) {
                    if (response['success'] === true) {
                        setErrorPassRemind();
                        setSuccessPassRemind('<?php echo esc_html__('Slaptažodžio priminimas išsiųstas nurodytu el. paštu.', 'keltas-theme') ?>');
                        togglePassRemindLoading();
                    } else {
                        setSuccessPassRemind();
                        setErrorPassRemind('<?php echo esc_html__('Nurodytas el. pašto adresas nerastas.', 'keltas-theme') ?>');
                        togglePassRemindLoading();
                    }
                },
                error: function () {
                    setSuccessPassRemind();
                    setErrorPassRemind('<?php echo esc_html__('Įvyko klaida. Bandykite vėliau arba kreipkitės į administraciją.', 'keltas-theme') ?>');
                    togglePassRemindLoading();
                }
            })
        });

        $('#submit_login').on('click', function (e) {
            e.preventDefault();

            if (is_loading_login) {
                return;
            }

            var email = $("#login_email").val();
            var password = $("#login_password").val();

            if (!email || !password) {
                setError('<?php echo esc_html__('Visi laukai privalo būti užpildyti.', 'keltas-theme') ?>');
                return;
            }

            setError();
            toggleLoginLoading();

            jQuery.ajax({
                type: "post",
                dataType: "json",
                url: window.location.protocol + '//' + window.location.host,
                data: {
                    api: "loginUser",
                    email: email,
                    password: password,
                },
                success: function (response) {
                    if (response['success'] === true) {
                        window.location.reload()
                    } else {
                        console.log(response);
                        setError('<?php echo esc_html__('Įvyko klaida. Bandykite vėliau arba kreipkitės į administraciją.', 'keltas-theme') ?>');
                        toggleLoginLoading();
                    }
                },
                error: function () {
                    toggleLoginLoading();
                }
            })
        });

        $('#submit_register').on('click', function (e) {
            e.preventDefault();

            var errors = 0;

            if (is_loading_register) {
                return;
            }

            var name = $("#register_name").val();
            var surname = $("#register_surname").val();
            var email = $("#register_email").val();
            var password = $("#register_password").val();
            var repassword = $("#register_repassword").val();

            var rules = $("#accept_rules").prop("checked") === true;
            var privacy = $("#accept_privacy").prop("checked") === true;

            setError();
            $("#chk_rules").text('');
            $("#chk_privacy").text('');

            if (!name || !surname || !email || !password || !repassword) {
                errors++;
                setErrorRegister('<?php echo esc_html__('Visi laukai privalo būti užpildyti.', 'keltas-theme') ?>');
                return;
            }

            if (password.length < 6 || repassword.length < 6) {
                errors++;
                setErrorRegister('<?php echo esc_html__('Slaptažodis turi būti sudarytas mažiausiai iš 6 simbolių.', 'keltas-theme') ?>');
                return;
            }

            if (!rules) {
                errors++;
                $("#chk_rules").text('<?php echo esc_html__('Prašome sutikti su mūsų taisyklėmis.', 'keltas-theme') ?>');
            }
            if (!privacy) {
                errors++;
                $("#chk_privacy").text('<?php echo esc_html__('Prašome sutikti su privatumo politika.', 'keltas-theme') ?>');
            }

            if (errors > 0) {
                return;
            }

            toggleRegisterLoading();

            jQuery.ajax({
                type: "post",
                dataType: "json",
                url: window.location.protocol + '//' + window.location.host,
                data: {
                    api: "registerUserV2",
                    name: name,
                    surname: surname,
                    email: email,
                    password: password,
                    repassword: repassword,
                    rules: rules,
                    privacy: privacy,
                    locale: '<?php echo ICL_LANGUAGE_CODE ?>',
                },
                success: function (response) {
                    if (response.code == 2) {
                        $("#error_message_register").css('color', 'red');
                        setErrorRegister('<?php echo esc_html__('El. paštas yra užimtas. Prisijunkite arba naudokite kitą pašto adresą.', 'keltas-theme') ?>');
                        toggleRegisterLoading();
                    } else if (response.code == 5) {
                        $("#error_message_register").css('color', 'green');
                        setErrorRegister('<?php echo esc_html__('Paskyros patvirtinimas išsiųstas nurodytu el. paštu.', 'keltas-theme') ?>');
                        toggleRegisterLoading();
                    } else if (response.code == 6) {
                        $("#error_message_register").css('color', 'red');
                        setErrorRegister('<?php echo esc_html__('El. paštas yra užimtas. Prisijunkite arba naudokite kitą pašto adresą.', 'keltas-theme') ?>');
                        toggleRegisterLoading();
                    } else {
                        $("#error_message_register").css('color', 'red');
                        setErrorRegister('<?php echo esc_html__('Įvyko klaida. Bandykite vėliau arba kreipkitės į administraciją.', 'keltas-theme') ?>');
                        toggleRegisterLoading();
                    }
                },
                error: function () {
                    toggleRegisterLoading();
                }
            })
        });

        $('#jurid_submit_register').on('click', function (e) {
            e.preventDefault();

            var errors = 0;

            if (is_loading_register_jurid) {
                return;
            }

            var title = $("#jurid_register_title").val();
            var teisine = $("#jurid_register_teisine").val();
            var email = $("#jurid_register_email").val();
            var address = $("#jurid_address").val();
            var nationality = $("#jurid_register_nationality").val();
            var password = $("#jurid_register_password").val();
            var repassword = $("#jurid_register_repassword").val();
            var code = $("#jurid_register_code").val();
            var pvmcode = $("#jurid_register_pvm_code").val();

            var rules = $("#jurid_accept_rules").prop("checked") === true;
            var privacy = $("#jurid_accept_privacy").prop("checked") === true;

            setError();
            $("#jurid_accept_rules").text('');
            $("#jurid_accept_privacy").text('');

            if (!title || !address || !nationality || !teisine || !email || !password || !repassword || !code) {
                errors++;
                setErrorRegisterJurid('<?php echo esc_html__('Visi laukai privalo būti užpildyti.', 'keltas-theme') ?>');
                return;
            }

            if (password.length < 6 || repassword.length < 6) {
                errors++;
                setErrorRegisterJurid('<?php echo esc_html__('Slaptažodis turi būti sudarytas mažiausiai iš 6 simbolių.', 'keltas-theme') ?>');
                return;
            }

            if (!rules) {
                errors++;
                $("#jurid_accept_rules").text('<?php echo esc_html__('Prašome sutikti su mūsų taisyklėmis.', 'keltas-theme') ?>');
            }
            if (!privacy) {
                errors++;
                $("#jurid_accept_privacy").text('<?php echo esc_html__('Prašome sutikti su privatumo politika.', 'keltas-theme') ?>');
            }

            if (errors > 0) {
                return;
            }

            toggleRegisterLoadingJurid();

            jQuery.ajax({
                type: "post",
                dataType: "json",
                url: window.location.protocol + '//' + window.location.host,
                data: {
                    api: "registerUserJurid",
                    title: title,
                    teisine: teisine,
                    email: email,
                    address: address,
                    nationality: nationality,
                    password: password,
                    repassword: repassword,
                    code: code,
                    pvmcode: pvmcode,
                    rules: rules,
                    privacy: privacy,
                    locale: '<?php echo ICL_LANGUAGE_CODE ?>',
                },
                success: function (response) {
                    if (response.code == 2) {
                        $("#error_message_register_jurid").css('color', 'red');
                        setErrorRegisterJurid('<?php echo esc_html__('El. paštas yra užimtas. Prisijunkite arba naudokite kitą pašto adresą.', 'keltas-theme') ?>');
                        toggleRegisterLoadingJurid();
                    } else if (response.code == 5) {
                        $("#error_message_register_jurid").css('color', 'green');
                        setErrorRegisterJurid('<?php echo esc_html__('Paskyros patvirtinimas išsiųstas nurodytu el. paštu.', 'keltas-theme') ?>');
                        toggleRegisterLoadingJurid();
                    } else if (response.code == 6) {
                        $("#error_message_register_jurid").css('color', 'red');
                        setErrorRegisterJurid('<?php echo esc_html__('El. paštas yra užimtas. Prisijunkite arba naudokite kitą pašto adresą.', 'keltas-theme') ?>');
                        toggleRegisterLoadingJurid();
                    } else {
                        $("#error_message_register_jurid").css('color', 'red');
                        setErrorRegisterJurid('<?php echo esc_html__('Įvyko klaida. Bandykite vėliau arba kreipkitės į administraciją.', 'keltas-theme') ?>');
                        toggleRegisterLoadingJurid();
                    }
                },
                error: function () {
                    toggleRegisterLoadingJurid();
                }
            })
        });

        $('#register_form_button').on('click', function (e) {
            e.preventDefault();

            $("#login_form").css('display', 'none');
            $("#register_form").css('display', 'block');
            $("#remind_form").css('display', 'none');
        });

        $('#register_form_button_remind').on('click', function (e) {
            e.preventDefault();

            $("#login_form").css('display', 'none');
            $("#register_form").css('display', 'block');
            $("#remind_form").css('display', 'none');
        });

        $('#login_form_button').on('click', function (e) {
            e.preventDefault();

            $("#register_form").css('display', 'none');
            $("#login_form").css('display', 'block');
            $("#remind_form").css('display', 'none');
        });

        $('#login_form_button_jurid').on('click', function (e) {
            e.preventDefault();

            $("#register_form_jurid").css('display', 'none');
            $("#login_form").css('display', 'block');
            $("#remind_form").css('display', 'none');

            jurid_checked = false;
            $('.jurid-check').prop('checked', jurid_checked);
        });

        $('#login_form_button_remind').on('click', function (e) {
            e.preventDefault();

            $("#register_form").css('display', 'none');
            $("#login_form").css('display', 'block');
            $("#remind_form").css('display', 'none');
        });

        $('#remind_form_button').on('click', function (e) {
            e.preventDefault();

            $("#register_form").css('display', 'none');
            $("#login_form").css('display', 'none');
            $("#remind_form").css('display', 'block');
        });

        function setError(msg) {
            $("#error_message").text(msg ? msg : '');
        }

        function setErrorPassChange(msg) {
            $("#error_message_pass_change").text(msg ? msg : '');
        }

        function setErrorPassRemind(msg) {
            $("#error_message_pass_remind").text(msg ? msg : '');
        }

        function setSuccessPassChange(msg) {
            $("#success_message_pass_change").text(msg ? msg : '');
        }

        function setSuccessPassRemind(msg) {
            $("#success_message_pass_remind").text(msg ? msg : '');
        }

        function setErrorRegister(msg) {
            $("#error_message_register").text(msg ? msg : '');
        }

        function setErrorRegisterJurid(msg) {
            $("#jurid_error_message_register").text(msg ? msg : '');
        }

        function toggleLoginLoading() {
            if (is_loading_login) {
                is_loading_login = false;
                $("#login_text").css('display', 'block');
                $("#login_loading").css('display', 'none');
            } else {
                is_loading_login = true;
                $("#login_text").css('display', 'none');
                $("#login_loading").css('display', 'inline-block');
            }
        }

        function toggleActivationLoading(tex, loa) {
            if (is_activation_loading) {
                is_activation_loading = false;
                tex.css('display', 'block');
                loa.css('display', 'none');
            } else {
                is_activation_loading = true;
                tex.css('display', 'none');
                loa.css('display', 'inline-block');
            }
        }

        function togglePassChangeLoading() {
            if (is_loading_pass_change) {
                is_loading_pass_change = false;
                $("#change_pass_text").css('display', 'block');
                $("#change_pass_loading").css('display', 'none');
            } else {
                is_loading_pass_change = true;
                $("#change_pass_text").css('display', 'none');
                $("#change_pass_loading").css('display', 'inline-block');
            }
        }

        function togglePassRemindLoading() {
            if (is_loading_pass_remind) {
                is_loading_pass_remind = false;
                $("#remind_text").css('display', 'block');
                $("#remind_loading").css('display', 'none');
            } else {
                is_loading_pass_remind = true;
                $("#remind_text").css('display', 'none');
                $("#remind_loading").css('display', 'inline-block');
            }
        }

        function toggleRegisterLoading() {
            if (is_loading_register) {
                is_loading_register = false;
                $("#register_text").css('display', 'block');
                $("#register_loading").css('display', 'none');
            } else {
                is_loading_register = true;
                $("#register_text").css('display', 'none');
                $("#register_loading").css('display', 'inline-block');
            }
        }

        function toggleRegisterLoadingJurid() {
            if (is_loading_register_jurid) {
                is_loading_register_jurid = false;
                $("#jurid_register_text").css('display', 'block');
                $("#jurid_register_loading").css('display', 'none');
            } else {
                is_loading_register_jurid = true;
                $("#jurid_register_text").css('display', 'none');
                $("#jurid_register_loading").css('display', 'inline-block');
            }
        }

        $('.ticket-outer').on('click', function (e) {
            if (window.navigator.userAgent.search('Android') > -1 || window.navigator.userAgent.search('iPhone') > -1) {
                $('#popup_img').html($(this).find('.barcore-img').clone());
                $("#popup_backdrop").css('display', 'flex');
            }
        });

        $('#popup_backdrop').on('click', function (e) {
            e.stopImmediatePropagation();
            selected_ticket_id = '-1';
            selected_ticket_activated = false;
            $('#popup_img').html('');
            $('#popup_backdrop').css('display', 'none');

            if (was_ticket_activated_recently) {
                window.location.reload()
            }

            is_activation_loading = false;
            $("#activate_ticket_text").css('display', 'block');
            $("#activate_ticket_loading").css('display', 'none');

            $('#activate_ticket').css('background', '#288CCD');
            $('#activate_ticket_text').html('<?php echo esc_html__('Aktyvuoti bilietą', 'keltas-theme') ?>');
        });

    </script>

<?php get_footer();
