<?php
/**
 * Template Name: Kontaktai
 */

get_header();

global $wp;
$url = home_url( $wp->request );
$url_array = explode("/", $url);

$part_url = $url_array[0] ."/". $url_array[1] ."/". $url_array[2] ."/". $url_array[3];
$level_one_page_id = url_to_postid($part_url);
?>

<?php
$background = 'background: url('. get_the_post_thumbnail_url() .')';
if ( get_the_post_thumbnail_url() == null ) $background = 'background: url('. get_the_post_thumbnail_url( get_option( "page_for_posts" ) ) .')';
?>

    <div class="header-image" style="<?php echo $background ?>"></div>
    <div class="container inside-page">

        <div class="contacts-container">

            <div class="row m-0 mb-4">
                <h1 class="big-title"><?php echo get_the_title() ?></h1>
            </div>

            <div class="row m-0">
                <div class="col-lg-6 p-0 contacts-left-block">

                    <div class="information-container">
                        <?php the_content(); ?>
                    </div>

                    <div class="row m-0">
                        <div class="col-lg-7">
                            <div class="row mb-2">
                                <div class="col-xs p-0">
                                    <p class="number-text"><?php echo esc_html__( 'Trumpasis telefono numeris', 'keltas-theme' ); ?></p>
                                </div>
                                <div class="col p-0">
                                    <h3 class="contact-short-number">
                                        <a href="tel:<?php the_field("short-number", get_option( 'page_on_front')) ?>"> <?php the_field("short-number", get_option( 'page_on_front')) ?></a>
                                        <p><?php echo esc_html__( 'Informacija 24/7', 'keltas-theme' ); ?></p>
                                    </h3>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg p-0">
                            <p class="contact-title"><?php echo esc_html__( 'El. paštas', 'keltas-theme' ); ?></p>
                            <p><a href="mailto:<?php the_field("email", get_option( 'page_on_front')) ?>"> <?php the_field("email", get_option( 'page_on_front')) ?></a></p>
                        </div>
                    </div>

                    <div class="row m-0">
                        <div class="col-lg-7 p-0">
                            <?php $info = get_field('contact-dispatching');
                            if ( $info != null ) : ?>
                                <div class="col-lg-12 p-0">
                                    <p class="contact-title"><?php echo esc_html__( 'Dispečerinė', 'keltas-theme' ); ?></p>
                                </div>
                                <div class="col-lg-12 p-0">
                                    <?php echo '<p style="white-space: pre-wrap;">'.$info .'</p>'; ?>
                                </div>
                            <?php endif; ?>
                        </div>
                        <div class="col-lg p-0">
                            <?php $info = get_field('contact-clients');
                            if ( $info != null ) : ?>
                                <div class="col-lg-12 p-0">
                                    <p class="contact-title"><?php echo esc_html__( 'Klientų aptarnavimo skyrius', 'keltas-theme' ); ?></p>
                                </div
                                <div class="col-lg-12 p-0">
                                    <?php echo '<p style="white-space: pre-wrap;">'.$info .'</p>'; ?>
                                </div
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="row m-0">
                        <?php $info = get_field('contact-business-info');

                        if ( $info != null ) : ?>
                            <div class="col-lg-12 p-0">
                                <p class="contact-title"><?php echo esc_html__( 'Įmonės rekvizitai', 'keltas-theme' ); ?></p>
                            </div>
                            <div class="col-lg-12 p-0">
                                <?php echo '<p style="white-space: pre-wrap;">'.$info .'</p>'; ?>
                            </div>
                        <?php endif; ?>
                    </div>

                </div>
                <div class="col-lg p-0">
                    <div class="form-background">
                        <?php
                        if (ICL_LANGUAGE_CODE == 'ru'):
                            echo '<div lang="ru-RU">';
                                echo do_shortcode( '[contact-form-7 id="838" title="RU kontaktų forma"]' );
                            echo '</div>';
                        elseif (ICL_LANGUAGE_CODE == 'en'):
                            echo '<div lang="en-US">';
                                echo do_shortcode( '[contact-form-7 id="839" title="EN kontaktų forma"]' );
                            echo '</div>';
                        else :
                            echo do_shortcode( '[contact-form-7 id="497" title="Kontaktų forma"]' );
                        endif;
                        ?>

                    </div>
                </div>
            </div>
        </div>

        <div class="contact-people">
            <div class="row m-0 text-center">
                <?php
                if( have_rows('contacts-people-repeater') ):
                    $id = 0;
                    while ( have_rows('contacts-people-repeater') ) : the_row();

                        $title = get_sub_field('contacts-people-title');
                        $name = get_sub_field('contacts-people-name');
                        $mob = get_sub_field('contacts-people-mob');
                        $mail = get_sub_field('contacts-people-mail');
                        $image = get_sub_field('contacts-people-image');

                        echo '<div class="col-md-2 p-0 text-left contact-block">';

                            if ( get_sub_field('contacts-people-image') != null ) :
                                echo '<div class="contact-image"><img alt="" src="'. $image .'" /></div>';
                            endif;

                            echo '<p class="contact-title">'. $title .'</p>';
                            echo '<p class="contact-info">'. $name .'</p>';
                            echo '<p class="contact-info"> Tel. '. $mob .'</p>';
                            echo '<a href="mailto:'.$mail.'"><p class="contact-info">'. $mail .'</a></p>';

                        echo '</div>';

                        $id++;
                    endwhile;
                endif; ?>
            </div>
        </div>

        <div class="find-us-container">

            <h4 class="mid-title"><?php echo esc_html__( 'Kaip mus Rasti', 'keltas-theme' ); ?></h4>

            <div class="row">
                <div class="col-lg-6">
                    <h4 class="sm-title"><?php the_field("port-name", get_option( 'page_on_front')) ?></h4>

                    <p class="simple-info"><?php echo esc_html__( 'Informacija GPS vartotojams', 'keltas-theme' ); ?></p>
                    <p class="simple-info"><?php the_field("port-address", get_option( 'page_on_front')) ?></p>
                    <p class="simple-info"><?php echo esc_html__( 'GPS koordinatės: ', 'keltas-theme' ); ?><?php the_field("port-cordinates", get_option( 'page_on_front')) ?></p>

                    <div id="map-1" class="contact-map"></div>

                    <script>
                        $(document).ready(function() {

                            var lat = parseFloat($('#map-0-lat').val());
                            var lng = parseFloat($('#map-0-lng').val());

                            var map = new google.maps.Map(document.getElementById('map-1'), {
                                center: {lat: lat, lng: lng},
                                zoom: 12,
                                mapTypeId: google.maps.MapTypeId.ROADMAP,
                                disableDefaultUI: true,
                                styles: [
                                    {"elementType": "geometry", "stylers": [{"color": "#f5f5f5"}]},
                                    {"elementType": "labels.icon", "stylers": [{"visibility": "off"}]},
                                    {"elementType": "labels.text.fill", "stylers": [{"color": "#616161"}]},
                                    {"elementType": "labels.text.stroke", "stylers": [{"color": "#f5f5f5"}]},
                                    {"featureType": "administrative.land_parcel", "elementType": "labels.text.fill", "stylers": [{"color": "#bdbdbd"}]},
                                    {"featureType": "poi", "elementType": "geometry", "stylers": [{"color": "#eeeeee"}]},
                                    {"featureType": "poi", "elementType": "labels.text.fill", "stylers": [{"color": "#757575"}]},
                                    {"featureType": "poi.park", "elementType": "geometry", "stylers": [{"color": "#e5e5e5"}]},
                                    {"featureType": "poi.park", "elementType": "labels.text.fill", "stylers": [{"color": "#9e9e9e"}]},
                                    {"featureType": "road", "elementType": "geometry", "stylers": [{"color": "#ffffff"}]},
                                    {"featureType": "road.arterial", "elementType": "labels.text.fill", "stylers": [{"color": "#757575"}]},
                                    {"featureType": "road.highway", "elementType": "geometry", "stylers": [{"color": "#dadada"}]},
                                    {"featureType": "road.highway", "elementType": "labels.text.fill", "stylers": [{"color": "#616161"}]},
                                    {"featureType": "road.local", "elementType": "labels.text.fill", "stylers": [{"color": "#9e9e9e"}]},
                                    {"featureType": "transit.line", "elementType": "geometry", "stylers": [{"color": "#e5e5e5"}]},
                                    {"featureType": "transit.station", "elementType": "geometry", "stylers": [{"color": "#eeeeee"}]},
                                    {"featureType": "water", "elementType": "geometry", "stylers": [{"color": "#c9c9c9"}]},
                                    {"featureType": "water", "elementType": "labels.text.fill", "stylers": [{"color": "#9e9e9e"}]}
                                ]
                            });

                            var icon = {
                                path: "M36 18C36 32 18 44 18 44C18 44 0 32 0 18C0 8.05884 8.05884 0 18 0C27.9412 0 36 8.05884 36 18ZM18 25C21.866 25 25 21.866 25 18C25 14.134 21.866 11 18 11C14.134 11 11 14.134 11 18C11 21.866 14.134 25 18 25Z",
                                fillColor: '#00518B',
                                fillOpacity: 1,
                                anchor: new google.maps.Point(15,40),
                                strokeWeight: 0,
                                scale: 1
                            };

                            var marker = new google.maps.Marker({
                                position: {lat: lat, lng: lng},
                                map: map,
                                icon: icon
                            });
                        });
                    </script>

                </div>

                <div class="col-lg-6">

                    <h4 class="sm-title"><?php the_field("new-port-name", get_option( 'page_on_front')) ?></h4>
                    <p class="simple-info"><?php echo esc_html__( 'Informacija GPS vartotojams', 'keltas-theme' ); ?></p>
                    <p class="simple-info"><?php the_field("new-port-address", get_option( 'page_on_front')) ?></p>
                    <p class="simple-info"><?php echo esc_html__( 'GPS koordinatės: ', 'keltas-theme' ); ?><?php the_field("new-port-cordinates", get_option( 'page_on_front')) ?></p>

                    <div id="map-2" class="contact-map"></div>

                    <script>
                        $(document).ready(function() {

                            var lat = parseFloat($('#map-1-lat').val());
                            var lng = parseFloat($('#map-1-lng').val());

                            var map = new google.maps.Map(document.getElementById('map-2'), {
                                center: {lat: lat, lng: lng},
                                zoom: 12,
                                mapTypeId: google.maps.MapTypeId.ROADMAP,
                                disableDefaultUI: true,
                                styles: [
                                    {"elementType": "geometry", "stylers": [{"color": "#f5f5f5"}]},
                                    {"elementType": "labels.icon", "stylers": [{"visibility": "off"}]},
                                    {"elementType": "labels.text.fill", "stylers": [{"color": "#616161"}]},
                                    {"elementType": "labels.text.stroke", "stylers": [{"color": "#f5f5f5"}]},
                                    {"featureType": "administrative.land_parcel", "elementType": "labels.text.fill", "stylers": [{"color": "#bdbdbd"}]},
                                    {"featureType": "poi", "elementType": "geometry", "stylers": [{"color": "#eeeeee"}]},
                                    {"featureType": "poi", "elementType": "labels.text.fill", "stylers": [{"color": "#757575"}]},
                                    {"featureType": "poi.park", "elementType": "geometry", "stylers": [{"color": "#e5e5e5"}]},
                                    {"featureType": "poi.park", "elementType": "labels.text.fill", "stylers": [{"color": "#9e9e9e"}]},
                                    {"featureType": "road", "elementType": "geometry", "stylers": [{"color": "#ffffff"}]},
                                    {"featureType": "road.arterial", "elementType": "labels.text.fill", "stylers": [{"color": "#757575"}]},
                                    {"featureType": "road.highway", "elementType": "geometry", "stylers": [{"color": "#dadada"}]},
                                    {"featureType": "road.highway", "elementType": "labels.text.fill", "stylers": [{"color": "#616161"}]},
                                    {"featureType": "road.local", "elementType": "labels.text.fill", "stylers": [{"color": "#9e9e9e"}]},
                                    {"featureType": "transit.line", "elementType": "geometry", "stylers": [{"color": "#e5e5e5"}]},
                                    {"featureType": "transit.station", "elementType": "geometry", "stylers": [{"color": "#eeeeee"}]},
                                    {"featureType": "water", "elementType": "geometry", "stylers": [{"color": "#c9c9c9"}]},
                                    {"featureType": "water", "elementType": "labels.text.fill", "stylers": [{"color": "#9e9e9e"}]}
                                ]
                            });

                            var icon = {
                                path: "M36 18C36 32 18 44 18 44C18 44 0 32 0 18C0 8.05884 8.05884 0 18 0C27.9412 0 36 8.05884 36 18ZM18 25C21.866 25 25 21.866 25 18C25 14.134 21.866 11 18 11C14.134 11 11 14.134 11 18C11 21.866 14.134 25 18 25Z",
                                fillColor: '#00518B',
                                fillOpacity: 1,
                                anchor: new google.maps.Point(15,40),
                                strokeWeight: 0,
                                scale: 1
                            };

                            var marker = new google.maps.Marker({
                                position: {lat: lat, lng: lng},
                                map: map,
                                icon: icon
                            });
                        });
                    </script>
                </div>
            </div>

            <?php
            if( have_rows('contact-map-repeater') ):
                $id = 0;
                while ( have_rows('contact-map-repeater') ) : the_row();

                    $location = get_sub_field('contact-map');
                    if ( !empty($location) ):
                        echo '<input type="hidden" value="'. $location['lat'] .'" id="map-'. $id .'-lat"/>';
                        echo '<input type="hidden" value="'. $location['lng'] .'" id="map-'. $id .'-lng"/>';
                    endif;

                    $id++;
                endwhile;
            endif;
            ?>

        </div>
    </div>

<script>
    $(document).ready(function() {
        var active = false;
        $('.privacy-policy-consent').on('click', function (e) {

            if (active == false) {
                $('.consent-checkmark-after').css('display', "block");
                active = true;
            }
            else {
                $('.consent-checkmark-after').css('display', "none");
                active = false;
            }
        });
    });
</script>

<?php get_footer();