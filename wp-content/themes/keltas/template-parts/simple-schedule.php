<?php
/**
 * Template Name: Paprastas-Tvarkaraštis
 */

get_header();
?>

    <div class="container inside-page" style="margin: auto; max-width: 1420px; padding: 50px 0;">
        <div class="row m-0">
            <h1 class="big-title"><?php echo get_the_title() ?></h1>
        </div>

        <?php get_template_part('template-parts/schedule-part/schedule-template'); ?>

    </div>

<?php get_footer();