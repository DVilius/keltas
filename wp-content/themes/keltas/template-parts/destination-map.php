<?php $front_page = get_option('page_on_front');

$font_size = "";
if (ICL_LANGUAGE_CODE == 'ru') {
    $font_size = "font-size: 20px";
}

$dateToPick = date('Y-m-d');

$apiContents = Api::timeTable3(ICL_LANGUAGE_CODE, $dateToPick);
$groupedEntries = json_decode($apiContents, true);


function getFerriesByID($entries, $idArray) {
    $ferries = [];

    foreach ($entries as $weekDay) {
        foreach ($weekDay as $ferryId => $ferry) {
            if (in_array($ferryId, $idArray, false)) {
                $ferries [] = $ferry;
            }
        }
    }

    return $ferries;
}

?>

<div class="row row-part-left" style="width: 100%; float: left; margin: 0;">

    <div class="row row-part-top" style="float: left; margin: 0">
        <form>
            <div class="row m-0">
                <div class="col-md-auto p-0">
                    <div class="destination-klaipeda-container">
                        <h3 class="destination-title"><?php echo esc_html__('Iš Klaipėdos', 'keltas-theme'); ?></h3>

                        <?php
                        $ferries = getFerriesByID($groupedEntries, [
                            '16',
                            '17'
                        ]);
                        ?>

                        <?php foreach ($ferries as $ferry): ?>
                            <?php $ferryData = array_values($ferry)[0] ?>

                            <div class="checkbox-container">
                                <label style="<?php echo $font_size ?>"> <?php echo $ferryData['fromPoint'] ?>
                                    <input id="ferry_id_<?php echo $ferryData['id'] ?>"
                                           type="radio" value="<?php echo $ferryData['id'] ?>"
                                           class="destination-check selection-1" name="selection">
                                    <span class="checkmark"></span>
                                </label>
                            </div>

                        <?php endforeach; ?>

                    </div>
                </div>

                <div class="col-md-auto p-0">
                    <div class="destination-smiltyne-container">

                        <h3 class="destination-title"><?php echo esc_html__('Iš Smiltynės', 'keltas-theme'); ?></h3>
                        <div class="row m-0">
                            <?php
                            $ferries = getFerriesByID($groupedEntries, [
                                '18',
                                '19',
                                '21',
                            ]);
                            ?>

                            <?php foreach ($ferries as $ferry): ?>
                                <?php $ferryData = array_values($ferry)[0] ?>

                                <div class="checkbox-container">
                                    <label style="<?php echo $font_size ?>"> <?php echo $ferryData['fromPoint'] ?>
                                        <input id="ferry_id_<?php echo $ferryData['id'] ?>"
                                               type="radio" value="<?php echo $ferryData['id'] ?>"
                                               class="destination-check selection-1" name="selection">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>

                            <?php endforeach; ?>
                        </div>

                    </div>
                </div>

                <div class="col-lg-auto p-0">
                    <div class="destination-direction-container">
                        <h3 class="destination-title"><?php echo esc_html__('Kryptis', 'keltas-theme'); ?></h3>
                        <div class="destination-direction">
                            <i class="fas fa-angle-down" style="position: absolute; right: 20px; font-size: 25px; top: 20px;"></i>
                            <div id="selected_data" class="select-selected" style="border:none"></div>
                            <div id="dropdown_data" class="select-items select-hide">
                                <div class="destination-option"> test <i class="fas fa-long-arrow-alt-right"></i> test <span style="display:none">1</span></div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </form>
    </div>


    <div class="row row-part-bottom" style="float: left; margin: 0; margin-top: 20px;">

        <div class="col-md-auto p-0 destination-schedule-row">
            <div class="destination-schedule-container">

                <h3 class="destination-title"><?php echo esc_html__('Artimiausi reisai', 'keltas-theme'); ?></h3>

                <div id="ferry_closest_times" class="departure-time"></div>

                <div class="see-more-container">
                    <a class="see-more-link" href="<?php echo get_field("homepage-url-schedule", $front_page) ?>"
                    ><?php echo esc_html__('Pilnas tvarkaraštis', 'keltas-theme'); ?><i style="color: #288CCD;" class="fas fa-long-arrow-alt-right"></i></a>
                </div>

            </div>
        </div>

        <div class="col-md-auto p-0 destination-costs-row">
            <div class="destination-costs-container">

                <h3 class="destination-title"><?php echo esc_html__('Kainos', 'keltas-theme'); ?></h3>

                <div class="destination-costs">
                    <div id="ferry_costs" style="display: block;" class="ferry-tickets"></div>
                </div>

                <div class="see-more-container">
                    <a class="see-more-link" href="<?php echo get_field("homepage-url-costs-table", $front_page) ?>"
                    ><?php echo esc_html__('Kainoraštis', 'keltas-theme'); ?><i style="color: #288CCD;" class="fas fa-long-arrow-alt-right"></i></a>
                </div>

            </div>
        </div>

        <div class="col-lg-auto p-0">
            <div class="destination-info-container">

                <h3 class="destination-title"><?php echo esc_html__('Informacija', 'keltas-theme'); ?></h3>

                <div id="ferry_additional_Info"></div>

                <div class="see-more-container">
                    <a class="see-more-link" href="<?php echo esc_html__('/bilietu-pirkimas', 'keltas-theme') ?>"
                    ><?php echo esc_html__('Pirkti bilietus ', 'keltas-theme'); ?><i style="color: #288CCD;" class="fas fa-long-arrow-alt-right"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row m-0 row-part-right" style="width: 30%; position: absolute; right: 0;">
    <div class="col-lg p-0">
        <div id="destination-map"></div>
    </div>
</div>

<script type="text/javascript">
    var markers = [];
    var locationsList = [];
    var ferries = <?php echo json_encode(array_values($groupedEntries)[0]) ?>;
    var ferriesInfo = <?php echo json_encode(get_fields()['ferrys-repeater']) ?>;
    var today = new Date();


    var map = new google.maps.Map(document.getElementById('destination-map'), {
        zoom: 13,
        center: new google.maps.LatLng(55.6974855, 21.1286019),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        disableDefaultUI: true,
        styles: [
            {"elementType": "geometry", "stylers": [{"color": "#f5f5f5"}]},
            {"elementType": "labels.icon", "stylers": [{"visibility": "off"}]},
            {"elementType": "labels.text.fill", "stylers": [{"color": "#616161"}]},
            {"elementType": "labels.text.stroke", "stylers": [{"color": "#f5f5f5"}]},
            {"featureType": "administrative.land_parcel", "elementType": "labels.text.fill", "stylers": [{"color": "#bdbdbd"}]},
            {"featureType": "poi", "elementType": "geometry", "stylers": [{"color": "#eeeeee"}]},
            {"featureType": "poi", "elementType": "labels.text.fill", "stylers": [{"color": "#757575"}]},
            {"featureType": "poi.park", "elementType": "geometry", "stylers": [{"color": "#e5e5e5"}]},
            {"featureType": "poi.park", "elementType": "labels.text.fill", "stylers": [{"color": "#9e9e9e"}]},
            {"featureType": "road", "elementType": "geometry", "stylers": [{"color": "#ffffff"}]},
            {"featureType": "road.arterial", "elementType": "labels.text.fill", "stylers": [{"color": "#757575"}]},
            {"featureType": "road.highway", "elementType": "geometry", "stylers": [{"color": "#dadada"}]},
            {"featureType": "road.highway", "elementType": "labels.text.fill", "stylers": [{"color": "#616161"}]},
            {"featureType": "road.local", "elementType": "labels.text.fill", "stylers": [{"color": "#9e9e9e"}]},
            {"featureType": "transit.line", "elementType": "geometry", "stylers": [{"color": "#e5e5e5"}]},
            {"featureType": "transit.station", "elementType": "geometry", "stylers": [{"color": "#eeeeee"}]},
            {"featureType": "water", "elementType": "geometry", "stylers": [{"color": "#c9c9c9"}]},
            {"featureType": "water", "elementType": "labels.text.fill", "stylers": [{"color": "#9e9e9e"}]}
        ]
    });

    google.maps.event.addListenerOnce(map, 'idle', function () {
        $('.destination-check').first().trigger('click');
    });

    var icon_grayedout = {
        path: "M6 15C6 15 12 10.9091 12 6.13636C12 2.74733 9.31372 0 6 0C2.68628 0 0 2.74733 0 6.13636C0 10.9091 6 15 6 15ZM8 6.13636C8 7.26604 7.10457 8.18182 6 8.18182C4.89543 8.18182 4 7.26604 4 6.13636C4 5.00669 4.89543 4.09091 6 4.09091C7.10457 4.09091 8 5.00669 8 6.13636Z",
        fillColor: '#91A2AF',
        fillOpacity: 1,
        anchor: new google.maps.Point(0, 0),
        strokeWeight: 0,
        scale: 1
    };

    var icon_inactive = {
        path: "M6 15C6 15 12 10.9091 12 6.13636C12 2.74733 9.31372 0 6 0C2.68628 0 0 2.74733 0 6.13636C0 10.9091 6 15 6 15ZM8 6.13636C8 7.26604 7.10457 8.18182 6 8.18182C4.89543 8.18182 4 7.26604 4 6.13636C4 5.00669 4.89543 4.09091 6 4.09091C7.10457 4.09091 8 5.00669 8 6.13636Z",
        fillColor: '#00518B',
        fillOpacity: 1,
        anchor: new google.maps.Point(0, 0),
        strokeWeight: 0,
        scale: 1
    };

    var icon_active_to = {
        path: "M36 18C36 32 18 44 18 44C18 44 0 32 0 18C0 8.05884 8.05884 0 18 0C27.9412 0 36 8.05884 36 18ZM18 25C21.866 25 25 21.866 25 18C25 14.134 21.866 11 18 11C14.134 11 11 14.134 11 18C11 21.866 14.134 25 18 25Z",
        fillColor: '#00518B',
        fillOpacity: 1,
        anchor: new google.maps.Point(15, 40),
        strokeWeight: 0,
        scale: 1
    };

    var icon_active_from = {
        path: "M36 18C36 32 18 44 18 44C18 44 0 32 0 18C0 8.05884 8.05884 0 18 0C27.9412 0 36 8.05884 36 18ZM18 25C21.866 25 25 21.866 25 18C25 14.134 21.866 11 18 11C14.134 11 11 14.134 11 18C11 21.866 14.134 25 18 25Z",
        fillColor: '#00518B',
        fillOpacity: 1,
        anchor: new google.maps.Point(15, 40),
        strokeWeight: 0,
        scale: 0.65
    };


    $('.destination-check').on('click', function (e) {
        var ferryID = $(this).val();
        var clickedFerries = ferries[ferryID];

        createClosestFerries(getSevenClosestFerries(clickedFerries));
        createFerryPrices(first(clickedFerries));
        createFerryInformation(first(clickedFerries));
        createLine(first(clickedFerries)['id']);

        $('#selected_data').empty();
        $('#dropdown_data').empty();

        $('#selected_data').append(
            first(clickedFerries)['from'] +
            " <i class='fas fa-long-arrow-alt-right'></i> " +
            first(clickedFerries)['to'] +
            "<span style='display:none'>" + first(clickedFerries)['id'] + "</span>"
        );

        if (ferryID === '19' || ferryID === '20') {
            var additionalFerry = first(ferries[ferryID === '19' ? 20 : 19]);
            createOption(additionalFerry);
        }
    });

    function createClosestFerries(closestFerries) {
        $('#ferry_closest_times').empty();

        if (!closestFerries.length) {
            child = document.createElement("h3");
            child.setAttribute('class', 'departure-time-big');
            child.innerHTML = '<?php echo esc_html__("Nėra", "keltas-theme") ?>';

            $('#ferry_closest_times').append(child);

            return;
        }

        for (var index = 0; index < closestFerries.length; index++) {
            var time_value = closestFerries[index];
            var child;

            if (index === 0) {
                child = document.createElement("h3");
                child.setAttribute('class', 'departure-time-big');
                child.innerHTML = time_value[0] + ':' + time_value[1];
            } else {

                if (index === 4) {
                    child = document.createElement("br");
                    $('#ferry_closest_times').append(child);
                }

                child = document.createElement("span");
                child.setAttribute('class', 'departure-time-small');
                child.innerHTML = time_value[0] + ':' + time_value[1];
            }
            $('#ferry_closest_times').append(child);
        }
    }

    function createFerryPrices(selectedFerry) {
        $('#ferry_costs').empty();

        for (var infoIndex = 0; infoIndex < ferriesInfo.length; infoIndex++) {
            var ferryInfo = ferriesInfo[infoIndex];

            if (selectedFerry['id'] === '20') {
                if (ferryInfo['perkelos_id'] === '19') {
                    for (var ticketIndex = 0; ticketIndex < ferryInfo['ferry-tickets-repeater'].length; ticketIndex++) {
                        var ticket = ferryInfo['ferry-tickets-repeater'][ticketIndex];
                        var child = document.createElement("p");
                        child.innerHTML = ticket['ferry-tickets-transport-type'] + '<span>' + ticket['ferry-tickets-cost'] + ' €</span><br>';
                        $('#ferry_costs').append(child);
                    }
                }
            } else {
                if (selectedFerry['id'] === ferryInfo['perkelos_id']) {
                    for (var ticketIndex = 0; ticketIndex < ferryInfo['ferry-tickets-repeater'].length; ticketIndex++) {
                        var ticket = ferryInfo['ferry-tickets-repeater'][ticketIndex];
                        var child = document.createElement("p");
                        child.innerHTML = ticket['ferry-tickets-transport-type'] + '<span>' + ticket['ferry-tickets-cost'] + ' €</span><br>';
                        $('#ferry_costs').append(child);
                    }
                }
            }
        }
    }

    function createFerryInformation(selectedFerry) {
        $('#ferry_additional_Info').empty();

        for (var infoIndex = 0; infoIndex < ferriesInfo.length; infoIndex++) {
            var ferryInfo = ferriesInfo[infoIndex];
            if (selectedFerry['id'] === '20') {
                if (ferryInfo['perkelos_id'] === '19') {
                    var child = document.createElement("p");
                    child.setAttribute('class', 'ferry-info');
                    child.innerHTML = ferryInfo['ferry-info'];
                    $('#ferry_additional_Info').append(child);
                }
            } else {
                if (selectedFerry['id'] === ferryInfo['perkelos_id']) {
                    var child = document.createElement("p");
                    child.innerHTML = ferryInfo['ferry-info'];
                    $('#ferry_additional_Info').append(child);
                }
            }
        }
    }

    function getSevenClosestFerries(ferries) {
        var sevenClosestFerries = [];

        for (var index = 0; index < 12; index++) {
            var target_hour = today.getHours() + index;
            var formatted_hour = target_hour < 10 ? '0' + target_hour : target_hour;

            if (ferries.hasOwnProperty(formatted_hour)) {
                var ferry = ferries[formatted_hour];

                if (sevenClosestFerries.length < 7) {
                    for (var m_index = 0; m_index < ferry['minutes'].length; m_index++) {
                        if (sevenClosestFerries.length < 7) {
                            var minute = ferry['minutes'][m_index].replace('*', '');

                            if (parseInt(ferry['hour']) > today.getHours()) {
                                sevenClosestFerries.push([ferry['hour'], minute]);
                            } else {
                                if (parseInt(minute) >= today.getMinutes()) {
                                    sevenClosestFerries.push([ferry['hour'], minute]);
                                }
                            }
                        }
                    }
                }
            }
        }

        return sevenClosestFerries;
    }

    function swapSelect(sourceID) {
        var destinationFerry = first(ferries[$('#selected_data').find('span').text()]);
        var sourceFerry = first(ferries[sourceID]);
        $('#selected_data').empty();
        $('#dropdown_data').empty();

        createClosestFerries(getSevenClosestFerries(ferries[sourceID]));
        createFerryPrices(first(ferries[sourceID]));
        createFerryInformation(first(ferries[sourceID]));
        createLine(sourceID);

        var toName = '';

        if (sourceFerry['id'] === "20") {
            toName = sourceFerry['toPoint'];
        } else {
            toName = sourceFerry['to'];
        }

        $('#selected_data').append(
            sourceFerry['from'] +
            " <i class='fas fa-long-arrow-alt-right'></i> " +
            toName +
            "<span style='display:none'>" + sourceFerry['id'] + "</span>"
        );

        createOption(destinationFerry);

        if (!$('.select-items').hasClass('select-hide')) {
            $('.select-items').addClass('select-hide');
        }
    }

    $('.select-selected').on('click', function (e) {
        if ($('.select-items').hasClass('select-hide')) {
            $('.select-items').removeClass('select-hide');
        } else {
            $('.select-items').addClass('select-hide');
        }
    });

    function createOption(ferry) {
        if (ferry === undefined) {
            return;
        }

        var option = document.createElement("DIV");
        option.setAttribute('class', 'destination-option');
        option.innerHTML = ferry['from'] +
            " <i class='fas fa-long-arrow-alt-right'></i> " +
            ferry['toPoint'] +
            "<span style='display:none'>" + ferry['id'] + "</span>";

        option.addEventListener("click", function (e) {
            swapSelect($(this).find('span').text());
        });

        $('#dropdown_data').append(option);
    }

    function first(arr) {
        for (var i in arr) return arr[i];
    }

    function targetMarker(lat, lng) {
        for (var addressIndex = 0; addressIndex < ferriesInfo.length; addressIndex++) {
            var _ferry = ferriesInfo[addressIndex];
            if (_ferry['ferry-show-in'] !== 'false' && _ferry['ferry-coordinates']['lat'] === lat && _ferry['ferry-coordinates']['lng'] === lng) {
                for (var markerIndex = 0; markerIndex < markers.length; markerIndex++) {
                    var _marker = markers[markerIndex];
                    if (_marker.id === _ferry['perkelos_id']) {
                        return _marker;
                    }
                }
            }
        }
    }

    $(document).ready(function () {
        for (var addressIndex = 0; addressIndex < ferriesInfo.length; addressIndex++) {
            if (ferriesInfo[addressIndex]['ferry-show-in'] !== 'false') {
                locationsList.push([
                    ferriesInfo[addressIndex]['ferry-coordinates']['lat'],
                    ferriesInfo[addressIndex]['ferry-coordinates']['lng'],
                    ferriesInfo[addressIndex]['perkelos_id'],
                ]);
            }
        }

        for (var locationIndex = 0; locationIndex < locationsList.length; locationIndex++) {
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(locationsList[locationIndex][0], locationsList[locationIndex][1]),
                map: map,
                icon: icon_inactive,
                id: locationsList[locationIndex][2],
            });

            markers.push(marker);

            google.maps.event.addListener(marker, 'click', function () {
                $("#ferry_id_" + this.id).trigger("click");
                createLine(this.id);
            });
        }

    });

    function createLine(sourceID) {
        var targetFerry = first(ferries[sourceID]);

        if (targetFerry === undefined) {
            return;
        }

        drawLines(map, [
            {lat: parseFloat(targetFerry['fromPointX']), lng: parseFloat(targetFerry['fromPointY'])},
            {lat: parseFloat(targetFerry['toPointX']), lng: parseFloat(targetFerry['toPointY'])}
        ]);

        for (var markerIndex = 0; markerIndex < markers.length; markerIndex++) {
            var _marker = markers[markerIndex];

            if (sourceID === '20') {
                if (_marker.id === '19') {
                    _marker.setIcon(icon_active_from);
                } else {
                    _marker.setIcon(icon_grayedout);
                }
            } else {
                if (_marker.id === sourceID) {
                    _marker.setIcon(icon_active_from);
                } else {
                    _marker.setIcon(icon_grayedout);
                }
            }
        }

        targetMarker(targetFerry['toPointX'], targetFerry['toPointY']).setIcon(icon_active_to);
    }

    var flightPath;

    function drawLines(map, path) {
        if (flightPath != null) flightPath.setMap(null);

        flightPath = new google.maps.Polyline({
            path: path,
            geodesic: true,
            strokeOpacity: 0,
            icons: [{
                icon: {
                    path: 'M 0,-1 0,1',
                    strokeOpacity: 1,
                    strokeColor: '#288CCD',
                    strokeWeight: 1.5,
                    geodesic: true,
                    scale: 2
                },
                offset: '0',
                repeat: '10px'
            }]
        });

        flightPath.setMap(map);
    }

</script>
