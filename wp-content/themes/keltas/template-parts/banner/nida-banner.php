
<?php
$banner_link = get_field("banner-url", get_option( 'page_on_front'));
$banner_background = get_field("banner-back", get_option( 'page_on_front'));
?>

<div class="banner">
    <div class="banner-back-gradient"></div>
    <div class="banner-main-image"<?php echo $banner_background ? 'style="background: url('. $banner_background .')"' : ''; ?>>
        <div class="banner-content">
            <?php if (ICL_LANGUAGE_CODE == 'ru') : ?>
                <h3 class="banner-slogan" style="font-size: 60px"><?php the_field("slogan", get_option( 'page_on_front')) ?></h3>
            <?php else : ?>
                <h3 class="banner-slogan"><?php the_field("slogan", get_option( 'page_on_front')) ?></h3>
            <?php endif; ?>
            <h3 class="banner-text">
                <?php the_field("banner-text", get_option( 'page_on_front')) ?>

                <?php if ($banner_link) : ?>
                    <p class="read-more-2">
                        <a href="<?php echo $banner_link['url']; ?>"
                           target="<?php echo $banner_link['target'] ?: '_self'; ?>">
                            <?php echo $banner_link['title']; ?>
                            <span><i class="fas fa-long-arrow-alt-right"></i></span>
                        </a>
                    </p>
                <?php endif; ?>
            </h3>
        </div>
    </div>
</div>