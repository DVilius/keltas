$(document).ready(function() {

    // Navigation search container management
    var active = false;
    $('#svg-search-icon').on('click', function (e) {
        setTimeout( function() {
            $('.icon-search-container').css("display", "block");
            active = true;
        }, 10);
    });

    $('html').on('click', function (e) {
        if ( active && e.target.type != "search") {
            $('.icon-search-container').css("display", "none");
            active = false;
        }
    });

    // Add icon to downloadable files links
    jQuery.each( $("a"), function (i, item) {

        var attribute = $(item).attr("href");
        var valid_files_array = [".doc", ".docx", ".ppt", ".pptx", ".pps", ".ppsx", ".odt", ".xls", ".xlsx",  ".pdf"]; // <-- supported file extensions

        for (var f = 0; valid_files_array.length != f; f++) {
            if (attribute != null && attribute.endsWith( valid_files_array[f] )) {
                $(item).addClass( 'download-file' );
            }
        }

    });

    // Slider
    jQuery('.slideshow-container ul').slick({
        arrows: true,
        nextArrow: '<button type="button" class="slider-button right"></button>',
        prevArrow: '<button type="button" class="slider-button left"></button>',
        dots: false,
        fade: false,
        speed: 150
    });


    // Youtube video scaling
    $('#video').css({ width: $(window).innerWidth() + 'px', height: $(window).innerHeight() + 'px' });
    $(window).resize(function(){
        $('#video').css({ width: $(window).innerWidth() + 'px', height: $(window).innerHeight() + 'px' });
    });

    $('.information-container img').on('click', function (e) {

        var source = $(this).attr("src");
        $('.lb-image').addClass('lightbox-image');

        var i = $(this).attr('src').split('-').length;
        var originalImage = $(this).attr('src').replace('-' + $(this).attr('src').split('-')[i-1].split('.')[0], "");

        setTimeout(function() { $('.lb-image').attr("src", originalImage); }, 1000);
    });

    $('.lb-prev').on('click', function () {
        setTimeout(function() {
            var image = $(".lb-image");
            image.addClass('lightbox-image');

            var i = image.attr('src').split('-').length;
            var originalImage = image.attr('src').replace('-' + image.attr('src').split('-')[i-1].split('.')[0], "");

            image.attr("src", originalImage);
        }, 500);
    });

    $('.lb-next').on('click', function () {
        setTimeout(function() {
            var image = $(".lb-image");
            image.addClass('lightbox-image');

            var i = image.attr('src').split('-').length;
            var originalImage = image.attr('src').replace('-' + image.attr('src').split('-')[i-1].split('.')[0], "");

            image.attr("src", originalImage);
        }, 500);
    });


    $('.information-container img').on('click', function (e) {
        $('.information-container img').each(function(){

            var source = $(this).attr("src");


            $(this).wrap(function() {
                return '<a href="'+ source  +'" data-lightbox="lightbox" />';
            })
        });
    });


    // Lightbox settings
    lightbox.option({
        'resizeDuration': 200,
        'wrapAround': true,
        'fitImagesInViewport': false,
        'positionFromTop': 200,
        'albumLabel': "Nuotrauka %1 iš %2",
        'alwaysShowNavOnTouchDevices': true
    });


    jQuery('body').keypress(function(event) {

        if ( event.key.toLowerCase() == "s" && event.shiftKey ) {
            event.preventDefault();

            jQuery('.icon-search-container').css("display", "block");
            jQuery('.search-field').focus();
            active = true;

        } else if ( event.key.toLowerCase() == "i" && event.shiftKey ) {
            window.location.href = document.location.origin;

        } else if ( event.key.toLowerCase() == "n" && event.shiftKey ) {
            window.location.href = document.location.origin + "/versija-neigaliesiams";

        }
    });

    // Mobile Menu
    jQuery('.menu-main-mobile i').on('click', function (e) {

        jQuery('.hidden-main-menu-mobile').css("display", "block");

            jQuery('.header-navigation-top-mobile').animate({
                width: "100%"
            }, 200 );
    });

    jQuery('.hidden-main-menu-mobile i').on('click', function (e) {

        jQuery('.header-navigation-top-mobile').animate({
            width: "0%"
        }, 100 );

            setTimeout( function() {
                jQuery('.hidden-main-menu-mobile').css("display", "none");
            }, 200 );
    });


    jQuery('.menu-main-mobile li.menu-item-has-children').on('click', function (e) {

        if ( jQuery(this).hasClass("mobile-submenu-open") ) {

            jQuery(this).removeClass("mobile-submenu-open");
            jQuery(this).find( "sub-menu" ).css("display", "none");

        } else {

            e.preventDefault();
            jQuery(this).addClass("mobile-submenu-open");
            jQuery(this).find( "sub-menu").css("display", "block");
        }

    });


    if ( !window.location.href.endsWith('/') || window.location.pathname != '/' || $(this).scrollTop() > 550 ) opaqueNavigation();
    if ( window.location.href.endsWith('/ru/') && $(this).scrollTop() < 550 ) transparentNavigation();
    if ( window.location.href.endsWith('/en/') && $(this).scrollTop() < 550 ) transparentNavigation();

    var scroll_pos = 0;
    jQuery(document).scroll(function() {

        scroll_pos = $(this).scrollTop();

        if (window.location.href.endsWith('/ru/') && scroll_pos < 550 && jQuery(document).width() > 800) {
            transparentNavigation();

        } else if (window.location.href.endsWith('/en/') && scroll_pos < 550 && jQuery(document).width() > 800) {
            transparentNavigation();

        } else if (scroll_pos > 550 || jQuery(document).width() <= 800 || window.location.pathname != '/' || !window.location.href.endsWith('/')) {
            opaqueNavigation();

        } else {
            transparentNavigation();
        }

    });


    function opaqueNavigation() {

        jQuery(".header-container-fixed").css('background-color', '#ffffff');
        jQuery(".header-navigation-top li a").css('color', '#00518B');
        jQuery(".extra-info .short-number a").css('color', '#00518B');
        jQuery(".social-icon svg").css('fill', '#00518B');
        jQuery(".social-icon svg path").css('fill', '#00518B');
        jQuery(".short-number-icon svg path").css('fill', '#00518B');
        jQuery(".weather-container span").css('color', '#00518B');
        jQuery(".header-navigation-top .menu-item-has-children").addClass('change-color');

        jQuery(".icon-menu svg path").css("fill", "#00518B");
        jQuery(".header-logo svg path").css("fill", "#00518B");
        jQuery(".header-logo svg").css("height", "90px");

        jQuery(".shadow-header").css("display", "none");

        jQuery(".menu-main-mobile i").css('color', '#00518B');

        jQuery(".header-container-fixed").css("border-bottom", "3px solid #f5f5f5");
    }

    function transparentNavigation() {

        jQuery(".header-container-fixed").css('background-color', 'transparent');
        jQuery(".header-navigation-top li a").css('color', '#ffffff');
        jQuery(".extra-info .short-number a").css('color', '#ffffff');
        jQuery(".social-icon svg").css('fill', '#ffffff');
        jQuery(".social-icon svg path").css('fill', '#ffffff');
        jQuery(".short-number-icon svg path").css('fill', '#ffffff');
        jQuery(".weather-container span").css('color', '#ffffff');
        jQuery(".header-navigation-top .menu-item-has-children").removeClass('change-color');

        jQuery(".icon-menu svg path").css("fill", "#ffffff");
        jQuery(".header-logo svg path").css("fill", "#ffffff");
        jQuery(".header-logo svg").css("height", "auto");

        jQuery(".shadow-header").css("display", "block");

        jQuery(".menu-main-mobile i").css('color', '#ffffff');

        jQuery(".header-container-fixed").css("border-bottom", "3px solid transparent");
    }

    // Clock script for schedule's
    updateClock();
    function updateClock() {
        var d = new Date(),
            h = (d.getHours()<10?'0':'') + d.getHours(),
            m = (d.getMinutes()<10?'0':'') + d.getMinutes();
        s = (d.getSeconds()<10?'0':'') + d.getSeconds();

        $(".clock-string").text( h + ':' + m + ":" + s);
    }
    setInterval(function() { updateClock()  }, 1000);

});