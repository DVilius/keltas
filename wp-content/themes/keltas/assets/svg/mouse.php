<svg width="30" height="48" viewBox="0 0 30 48" fill="none" xmlns="http://www.w3.org/2000/svg">
    <rect x="1.5" y="1.5" width="27" height="45" rx="13.5" stroke="white" stroke-width="3"/>
    <rect x="12" y="12" width="6" height="10" rx="3" fill="white"/>
</svg>