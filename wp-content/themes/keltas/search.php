<?php
/**
 * The template for displaying search results pages
 */

get_header(); ?>

<div class="container search-container-block">
	<div class="col-lg">
		<?php if ( have_posts() ) : ?>
			<h4 class="mid-title"><?php printf( __( 'Paieškos rezultatai: %s', 'keltas-theme' ), '<span>' . get_search_query() . '</span>' ); ?></h4>
		<?php else : ?>
			<h4 class="mid-title"><?php _e( 'Nieko nerasta', 'keltas-theme' ); ?></h4>
		<?php endif; ?>
	</div>

	<div class="col-lg pb-5">
		<?php

		if( have_posts() ):
			while( have_posts() ) : the_post();
				echo '<div class="col-xl-12">';
					get_template_part( 'template-parts/page/content-excerpt' );
				echo '</div>';
			endwhile;
			wp_reset_query();

		else :
			echo '<p>'. _e( 'Nieko nerasta atitinkančio raktažodį, bandykite dar kartą.', 'keltas-theme' ) .'</p>';

			 $unique_id = esc_attr( uniqid( 'search-form-' ) ); ?>

			<form role="search" method="get" class="search-form search-page-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
				<input type="search" id="<?php echo $unique_id; ?>" class="search-field" placeholder="<?php echo esc_attr_x( 'Ieškoti svetainėje', 'placeholder', 'keltas-theme' ); ?>"
					   value="<?php echo get_search_query(); ?>" name="s" />
				<button type="submit" class="search-submit"><?php get_template_part('assets/svg/search'); ?></button>
			</form>

			<?php
		endif;
		?>
	</div>

</div>

<?php get_footer();
